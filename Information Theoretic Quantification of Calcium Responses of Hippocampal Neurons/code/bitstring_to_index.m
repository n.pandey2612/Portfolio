function y=bitstring_to_index(x)
N=length(x);
%if(53 <= N )
 %  error('Sorry, this is too big for me to handle.')
%end;

S=1;
for i=1:N;
   S=2*S+x(i);
end
y=S-1;