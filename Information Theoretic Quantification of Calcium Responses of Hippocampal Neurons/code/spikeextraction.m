function[yproc]=spikeextraction(x,a,b,c)
%kernel defining
if(c==1)
%k=GAUSS(2000,3);
k=kernel(3000,a,b);
%deconvolution filter
else
   k=GAUSS(2000,3);
end
[y]=PSEUDOINVFILT(x,k);
%posttriggerestimation
[yproc]=POSTPROC(y,x);