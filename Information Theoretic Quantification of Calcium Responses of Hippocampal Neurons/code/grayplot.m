figure;
%baclofen%
%lz distance
 subplot(4,2,1)
 %without drug
 colormap('gray');   % set colormap
imagesc(ldb);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(a)');
 subplot(4,2,2)
 %with drug
 colormap('gray');   % set colormap
imagesc(ldd2);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(b)');
 
%correlation distance 
 subplot(4,2,3)
 %without drug
 colormap('gray');   % set colormap
imagesc(cdb);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(c)');
 subplot(4,2,4)
 %with drug
 colormap('gray');   % set colormap
imagesc(cdd2);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(d)');
%dhpg%
%lz distance
subplot(4,2,5)
%without drug
 colormap('gray');   % set colormap
imagesc(ldbg);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(e)');
 subplot(4,2,6)
 %with drug
 colormap('gray');   % set colormap
imagesc(ldd2g);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(f)');
 
%correlation distance 
 subplot(4,2,7)
 %without drug
 colormap('gray');   % set colormap
imagesc(cdbg);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(g)');
 subplot(4,2,8)
 %with drug
 colormap('gray');   % set colormap
imagesc(cdd2g);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(h)');