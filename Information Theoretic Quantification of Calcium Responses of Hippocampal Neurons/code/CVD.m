%CVD(A~, B~ ):
%require A~ and B~ have the same length
%~a = real part of A~
%~b = imaginary part of A~
%~c = real part of B~
%~d = imaginary part of B~
%C~ = new vector with same length as A~ (store real vals)
%~I = new vector with same length as A~ (store imag. vals)
%for i = 1 to length(A~):
%C~ [i], ~I[i] = CSD(~a[i], ~b[i], ~c[i], ~d[i])
%return C~ , ~I
function[y1,y2]=CVD(A, B)
%require A~ and B~ have the same length
a = real(A);
b = imag(A);
c = real(B);
d = imag(B);
%C= new vector with same length as A~ (store real vals)
%~I = new vector with same length as A~ (store imag. vals)
for i = 1 : length(A)
[C(i),I(i)] = CSD(a(i), b(i), c(i), d(i));
y1=C;
y2=I;
end






