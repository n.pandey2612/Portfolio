%for i=1:6
close all;
clc
clear
y1=num2str(2);
x1=strcat('(',y1,')');
%pathname1=strcat(pwd,'\','gyronormdatabase','\',x1,'.','csv');
pathname1=strcat(pwd,'\','a','\',x1,'.','xlsx');
z1=xlsread(pathname1);
%end
length=size(z1,2);
lenn=size(z1,1);
for i=3:length
    for j=3:length
        i
        j
    
    x1b=z1(1:240,i);
    x2b=z1(1:240,j);
    
   x1d2=z1(241:lenn,i);
   x2d2=z1(241:lenn,j);
    
    x1bm=movingavgfilter(x1b);
    x2bm=movingavgfilter(x2b);
   
    x1d2m=movingavgfilter(x1d2);
    x2d2m=movingavgfilter(x2d2);
    
    x1bs=spikeextraction(x1bm,0.1,0.3,1);
    x2bs=spikeextraction(x2bm,0.1,0.3,1);
    
    x1d2s=spikeextraction(x1d2m,0.1,0.3,1);
    x2d2s=spikeextraction(x2d2m,0.1,0.3,1);
    
    ldb(i-2,j-2)=lzdistance(x1bs,x2bs);
    cdb(i-2,j-2)=distcorr(x1b,x2b);
    
    ldd2(i-2,j-2)=lzdistance(x1d2s,x2d2s);
   cdd2(i-2,j-2)=distcorr(x1d2,x2d2);
    end
end

 
 figure;
 subplot(4,2,1)
 colormap('gray');   % set colormap
imagesc(ldb);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(a)');
 subplot(4,2,2)
 colormap('gray');   % set colormap
imagesc(ldd2);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(b)');
 
 
 subplot(4,2,3)
 colormap('gray');   % set colormap
imagesc(cdb);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(c)');
 subplot(4,2,4)
 colormap('gray');   % set colormap
imagesc(cdd2);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(d)');
 
%saveas(gcf,'baclo.fig');
%dhpg
%for i=1:6

y1g=num2str(8);
x1g=strcat('(',y1g,')');
%pathname1=strcat(pwd,'\','gyronormdatabase','\',x1,'.','csv');
pathname1g=strcat(pwd,'\','d','\',x1g,'.','xls');
z1g=xlsread(pathname1g);
%end
lengthg=size(z1g,2);
lenng=size(z1g,1);
for i=3:lengthg
    for j=3:lengthg
        i
        j
    
    x1bg=z1g(1:50,i);
    x2bg=z1g(1:50,j);
    
   x1d2g=z1g(51:lenng,i);
   x2d2g=z1g(51:lenng,j);
    
    x1bmg=movingavgfilter(x1bg);
    x2bmg=movingavgfilter(x2bg);
   
    x1d2mg=movingavgfilter(x1d2g);
    x2d2mg=movingavgfilter(x2d2g);
    
    x1bsg=spikeextraction(x1bmg,0.1,0.3,1);
    x2bsg=spikeextraction(x2bmg,0.1,0.3,1);
    
    x1d2sg=spikeextraction(x1d2mg,0.1,0.3,1);
    x2d2sg=spikeextraction(x2d2mg,0.1,0.3,1);
    
    ldbg(i-2,j-2)=lzdistance(x1bsg,x2bsg);
    cdbg(i-2,j-2)=distcorr(x1bg,x2bg);
    
    ldd2g(i-2,j-2)=lzdistance(x1d2sg,x2d2sg);
   cdd2g(i-2,j-2)=distcorr(x1d2g,x2d2g);
    end
end

 
 %figure;
 subplot(4,2,5)
 colormap('gray');   % set colormap
imagesc(ldbg);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(e)');
 subplot(4,2,6)
 colormap('gray');   % set colormap
imagesc(ldd2g);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(f)');
 
 
 subplot(4,2,7)
 colormap('gray');   % set colormap
imagesc(cdbg);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(g)');
 subplot(4,2,8)
 colormap('gray');   % set colormap
imagesc(cdd2g);        % draw image and scale colormap to values range
colorbar;
xlabel('Neuron number');
ylabel('Neuron number');
title('(h)');
 


 
    

 
    