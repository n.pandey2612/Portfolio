
X1 = poissrnd(2*(ones(80,150)));
X2 = poissrnd(3*(ones(20,150)));
X = [X1;X2];
scramble = randperm(size(X,1));
X = X(scramble,:);