%POSTPROC( ~ f, ~s):
%~p = new vector with same length as ~ f
%os = 7 (offset indexes for calculating the Ca deriv)
%for i = 1 to length( ~ f)-1
%~p[i] = ~ f[i+1] - ~ f[i] (differentiate)
%~p[i] = ~p[i]*(~s[i+os]-~s[i+os-1]) (mult by Ca deriv)
%~p[i] = ~p[i]*~p[i] (square it)
%~p = TRIGGER(~p, MEAN(~p))
%return ~
function[y]=POSTPROC(f,s)
%p = new vector with same length as ~ f
%os = 7 ;%(offset indexes for calculating the Ca deriv)
for i = 2 : length(f)-1
p(i) = f(i+1)-f(i); % (differentiate)
%p(i) = p(i)*(s(i+os)-s(i+os-1)); % (mult by Ca deriv)
p(i) = p(i)*(s(i)-s(i-1));
p(i) = p(i)*p(i); %(square it)

end
p = TRIGGER(p, mean(p));
y=p;