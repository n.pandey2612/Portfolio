function[y]=lzdistance(x1,x2)
[y1,len1]=lzcode(x1);
[y2,len2]=lzcode(x2);
y1s=cellstr(y1);
y2s=cellstr(y2);
yy12=setdiff(y1s,y2s);
yy21=setdiff(y2s,y1s);
k1=lzcomplexity(y1s,len1);
k12=lzcomplexity(yy12,len1);
k2=lzcomplexity(y2s,len2);
k21=lzcomplexity(yy21,len2);
d12=(k1-k12)/k1;
d21=(k2-k21)/k2;
d=1-min(d12,d21);
if isnan(d)
    d=0;
end
y=d;

