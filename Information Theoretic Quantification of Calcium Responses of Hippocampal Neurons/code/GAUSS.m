%GAUSS(w, d):
%~x = linspace(-1,1,w)
%~g = new vector of length w
%gs = 0.0
%sd2 = 1.5
%1/d�2
%for i = 1 to w (generate gaussian)
%~g[i] = exp(-~x[i]*~x[i]*sd2)
%gs = gs + ~g[i]
%for i = 1 to w (normalize)
%~g[i] = ~g[i] / gs
%return ~g
function[y]=GAUSS(w, d)
x = linspace(-1,1,w);
%g = new vector of length w
gs = 0.0;
sd2 = 1.5/(1/d^2);
for i = 1: w %(generate gaussian)
g(i) = exp(-x(i)*x(i)*sd2);
gs = gs + g(i);
end
for i = 1 : w %(normalize)
g(i) = g(i) / gs;
y=g;
end
