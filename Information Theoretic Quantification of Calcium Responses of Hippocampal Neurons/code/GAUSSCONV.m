%GAUSSCONV(~s, w, d):
%~k = GAUSS(w, d)
%~c = new vector of same length as ~s
%for i = 1 to length(~s):
%~c[i] = 0
%for j = 1 to length(~k)
%~c[i] = ~c[i] + ~k[j]*~s[REFLIND(i-w/2+j, 0, length(s)-1)]
%return ~c
function[y]=GAUSSCONV(s, w, d)
k = GAUSS(w, d);
%~c = new vector of same length as ~s
for i = 1 : length(s)
c(i) = 0;
for j = 1 : length(k)
c(i) = c(i) + k(j)*s(REFLIND(i-w/2+j, 0, length(s)-1));
y=c;
end
end