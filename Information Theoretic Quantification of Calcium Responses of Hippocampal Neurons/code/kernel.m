function[y]=kernel(w,a,b)
%b>a
x = linspace(-1,1,w);
%g = new vector of length w
gs = 0.0;
%sd2 = 1.5/(1/d^2);
for i = 1: w 
g(i) = exp(-a*x(i))-exp(-b*x(i));
gs = gs + g(i);
end
for i = 1 : w %(normalize)
g(i) = g(i) / gs;
y=g;
end