%PSEUDOR(K~ ):
%? = some small number, like 1e-4 (but not too small!!)
%for i = 1 to length(K~ ):
%if K~ [i] < ?: (if this element of K is too small,
%K~ [i] = ? make it not too small)
%return K~
function[y]=PSEUDOR(K)
a = 1e-4;
for i = 1 : length(K)
if K(i) < a    
K(i) = a;
end
end
y=K;
