figure;
%baclofen%
%lz dist
 subplot(4,2,1)
 h1=dendrogram(Zb);
 xlabel('Neuron number');
ylabel('Distance');
title('(a) Without drug');
 subplot(4,2,2)
 h2=dendrogram(Zd2);
 xlabel('Neuron number');
ylabel('Distance');
title('(b) With drug');
 set(h1,'LineWidth',2);
 set(h2,'LineWidth',2);
 subplot(4,2,3)
 %correlation dist
 h3=dendrogram(Zcs);
 xlabel('Neuron number');
ylabel('Distance');
title('(c) Without drug');
 subplot(4,2,4)
 h4=dendrogram(Zdc2);
 xlabel('Neuron number');
ylabel('Distance');
title('(d) With drug');
 set(h3,'LineWidth',2);
 set(h4,'LineWidth',2);
 %dhpg%
 %lz-distance
 subplot(4,2,5)
 h1g=dendrogram(Zbg);
 xlabel('Neuron number');
ylabel('Distance');
title('(e) Without drug');
 subplot(4,2,6)
 h2g=dendrogram(Zd2g);
 xlabel('Neuron number');
ylabel('Distance');
title('(f) With drug');
 set(h1g,'LineWidth',2);
 set(h2g,'LineWidth',2);
 %correlation dist
 
 subplot(4,2,7)
 h3g=dendrogram(Zcsg);
 xlabel('Neuron number');
ylabel('Distance');
title('(g) Without drug');
 subplot(4,2,8)
 h4g=dendrogram(Zdc2g);
 xlabel('Neuron number');
ylabel('Distance');
title('(h) With drug');
 set(h3g,'LineWidth',2);
 set(h4g,'LineWidth',2);
 