%for i=1:6
close all;
clc
clear
y1=num2str(2);%here 2 is the file in the folder
x1=strcat('(',y1,')');
%pathname1=strcat(pwd,'\','gyronormdatabase','\',x1,'.','csv');
pathname1=strcat(pwd,'\','a','\',x1,'.','xlsx');%here a is the folder name
z1=xlsread(pathname1);
%end
length=size(z1,2);
lenn=size(z1,1);
for i=3:length
    for j=3:length
        i
        j
    
    x1b=z1(1:240,i);
    x2b=z1(1:240,j);
    
   x1d2=z1(241:lenn,i);
   x2d2=z1(241:lenn,j);
    
    x1bm=movingavgfilter(x1b);
    x2bm=movingavgfilter(x2b);
   
    x1d2m=movingavgfilter(x1d2);
    x2d2m=movingavgfilter(x2d2);
    
    x1bs=spikeextraction(x1bm,0.1,0.3,1);
    x2bs=spikeextraction(x2bm,0.1,0.3,1);
    
    x1d2s=spikeextraction(x1d2m,0.1,0.3,1);
    x2d2s=spikeextraction(x2d2m,0.1,0.3,1);
    
    ldb(i-2,j-2)=lzdistance(x1bs,x2bs);
    cdb(i-2,j-2)=distcorr(x1b,x2b);
    
    ldd2(i-2,j-2)=lzdistance(x1d2s,x2d2s);
   cdd2(i-2,j-2)=distcorr(x1d2,x2d2);
    end
end

 ldbs=squareform(ldb);
 Zb = linkage(ldbs);
 ldd2s=squareform(ldd2);
 Zd2 = linkage(ldd2s);
 
 cdcs=squareform(cdb);
 Zcs = linkage(cdcs);
 cdd2s=squareform(cdd2);
 Zdc2 = linkage(cdd2s);
 figure;
 subplot(4,2,1)
 h1=dendrogram(Zb);
 xlabel('Neuron number');
ylabel('Distance');
title('(a) Without drug');
 subplot(4,2,2)
 h2=dendrogram(Zd2);
 xlabel('Neuron number');
ylabel('Distance');
title('(b) With drug');
 set(h1,'LineWidth',2);
 set(h2,'LineWidth',2);
 
 
 subplot(4,2,3)
 h3=dendrogram(Zcs);
 xlabel('Neuron number');
ylabel('Distance');
title('(c) Without drug');
 subplot(4,2,4)
 h4=dendrogram(Zdc2);
 xlabel('Neuron number');
ylabel('Distance');
title('(d) With drug');
 set(h3,'LineWidth',2);
 set(h4,'LineWidth',2);
 
%saveas(gcf,'baclo.fig');
%dhpg
%for i=1:6

y1g=num2str(8);
x1g=strcat('(',y1g,')');
%pathname1=strcat(pwd,'\','gyronormdatabase','\',x1,'.','csv');
pathname1g=strcat(pwd,'\','d','\',x1g,'.','xls');
z1g=xlsread(pathname1g);
%end
lengthg=size(z1g,2);
lenng=size(z1g,1);
for i=3:lengthg
    for j=3:lengthg
        i
        j
    
    x1bg=z1g(1:50,i);
    x2bg=z1g(1:50,j);
    
   x1d2g=z1g(51:lenng,i);
   x2d2g=z1g(51:lenng,j);
    
    x1bmg=movingavgfilter(x1bg);
    x2bmg=movingavgfilter(x2bg);
   
    x1d2mg=movingavgfilter(x1d2g);
    x2d2mg=movingavgfilter(x2d2g);
    
    x1bsg=spikeextraction(x1bmg,0.1,0.3,1);
    x2bsg=spikeextraction(x2bmg,0.1,0.3,1);
    
    x1d2sg=spikeextraction(x1d2mg,0.1,0.3,1);
    x2d2sg=spikeextraction(x2d2mg,0.1,0.3,1);
    
    ldbg(i-2,j-2)=lzdistance(x1bsg,x2bsg);
    cdbg(i-2,j-2)=distcorr(x1bg,x2bg);
    
    ldd2g(i-2,j-2)=lzdistance(x1d2sg,x2d2sg);
   cdd2g(i-2,j-2)=distcorr(x1d2g,x2d2g);
    end
end

 ldbsg=squareform(ldbg);
 Zbg = linkage(ldbsg);
 ldd2sg=squareform(ldd2g);
 Zd2g = linkage(ldd2sg);
 
 cdcsg=squareform(cdbg);
 Zcsg = linkage(cdcsg);
 cdd2sg=squareform(cdd2g);
 Zdc2g = linkage(cdd2sg);
 %figure;
 subplot(4,2,5)
 h1g=dendrogram(Zbg);
 xlabel('Neuron number');
ylabel('Distance');
title('(e) Without drug');
 subplot(4,2,6)
 h2g=dendrogram(Zd2g);
 xlabel('Neuron number');
ylabel('Distance');
title('(f) With drug');
 set(h1g,'LineWidth',2);
 set(h2g,'LineWidth',2);
 
 
 subplot(4,2,7)
 h3g=dendrogram(Zcsg);
 xlabel('Neuron number');
ylabel('Distance');
title('(g) Without drug');
 subplot(4,2,8)
 h4g=dendrogram(Zdc2g);
 xlabel('Neuron number');
ylabel('Distance');
title('(h) With drug');
 set(h3g,'LineWidth',2);
 set(h4g,'LineWidth',2);
 


 
    

 
    