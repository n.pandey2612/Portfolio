%REFLIND(i, a, b):
%if i < a
%return REFLIND(2*a-i, a, b)
%else if i >= b
%return REFLIND(2*b-i, a, b)
%else
%return i
function[y]=REFLIND(i, a, b)
if i < a
y= REFLIND(2*a-i, a, b);

elseif i >= b
y= REFLIND(2*b-i, a, b);

else
y= i;
end
