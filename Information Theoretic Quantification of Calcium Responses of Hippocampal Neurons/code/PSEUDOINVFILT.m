%PSEUDOINVFILT(~s,
%~k):
%S~ = FFT(~s) (forward fourier transform the signal)
%K~ = FFT(~k) (forward fourier transform the kernel)
%R~ = PSEUDOR(K~ ) (perform the correction from Eq. 4)
%P~ = CVD(S~, R~) (divide S by R as in Eq. 5)
%~f = IFT(P~ ) (perform the inverse fourier transform in Eq. 5)
%return ~f
function[y]=PSEUDOINVFILT(s,k)
S = fft(s); %(forward fourier transform the signal)
K = fft(k); %(forward fourier transform the kernel)
R = PSEUDOR(K); %(perform the correction from Eq. 4)
P = CVD(S, R); %(divide S by R as in Eq. 5)
f = ifft(P); %(perform the inverse fourier transform in Eq. 5)
y= f;