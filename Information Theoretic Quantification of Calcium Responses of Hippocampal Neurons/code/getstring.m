function[c]=getstring(y,y1)
ys=num2str(y);
y1s=num2str(y1);
ycell=cellstr(ys);
len=size(ycell,1);
ycat=[];
for i=1:len
ycat=strcat(ycat,',',ycell(i));
end
ycat=strcat(y1s,ycat);
c=char(ycat);
%yy2=char(y2cat)
%ychar=char(yy1,yy2)
