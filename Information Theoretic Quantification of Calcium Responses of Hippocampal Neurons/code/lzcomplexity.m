function[y]= lzcomplexity(x,n)
%[x1,n]=lzcode(x);
c=size(x,1);
y=(c*log(c))/n;
