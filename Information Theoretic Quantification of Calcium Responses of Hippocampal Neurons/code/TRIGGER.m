%TRIGGER(~s, th):
%~p = new vector of same length as ~s
%for i = 2 to length(~s)
%if ~s[i]>=th AND ~s[i-1]<th
%~p[i] = 1
%else
%~p[i] = 0
%return ~
function[y]=TRIGGER(s, th)
%~p = new vector of same length as ~s
for i = 2 : length(s)
if (s(i)>=th && s(i-1)<th)
p(i) = 1;
else
p(i) = 0;
end
end
y=p;