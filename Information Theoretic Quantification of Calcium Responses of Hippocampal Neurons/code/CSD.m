%CSD(a, b, c, d): (A = a + bi, B = c + di, returns A/B)
%denom = c*c + d*d
%e = (a*c - b*d) / denom
%f = (a*d + b*c) / denom
%return e, f
function [y1,y2]=CSD(a, b, c, d) %(A = a + bi, B = c + di, returns A/B)
denom = c*c + d*d;
e = (a*c - b*d) / denom;
f = (a*d + b*c) / denom;
y1=e;
y2=f;