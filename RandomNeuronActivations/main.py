# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 11:24:15 2017

@author: NehaPandey
"""

from scipy.ndimage import imread
import glob
import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize

# Tensorflow
import theano as th

# KERAS
from keras import backend as K
from keras.applications.vgg16 import VGG16

def get_representation(model, layer, input_batch):
    get_representation = K.function([model.layers[0].input,K.learning_phase()], [model.layers[layer].output,])
    representation = get_representation([input_batch, 0])
    return representation

model = VGG16(weights='imagenet', include_top=True)
print('Model loaded.')

#model.summary()
img_rows = 224
img_cols = 224

K.set_image_dim_ordering('tf')

#reading dataset
image_list = []
for filename in glob.glob('imagenet/*.JPEG'):
    im=imread(filename)
    im_resized=resize(im,(img_rows,img_cols,3))
    image_list.append(im_resized)

image_list=np.array(image_list)
#Data Preprocessing

if K.image_dim_ordering() == 'th':
    X_train = image_list.reshape(image_list.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    X_train = image_list.reshape(image_list.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

X_train = X_train.astype('float32')
X_train /= 255


#getting layer index
config=model.get_config()
for j,l in enumerate(config['layers']):
    if(l['config']['name']=='fc1'):
        layer_index=j

#getting activations
batch_size=20
no_of_batches=int(len(X_train)/batch_size)
rep=np.zeros((1,4096))
for i in range(no_of_batches):
    rep_i=get_representation(model, layer_index, X_train[20*i:20*(i+1)])
    rep=np.vstack((rep,rep_i[0]))

rep=rep[1:,:]
print(rep.shape)

random_neuron_indices=np.random.randint(low=0,high=4095,size=10)
for neuron in random_neuron_indices:
    args=np.argsort(rep[:,neuron])
    args=args[-5:]
    args=np.flip(args,0)
    print(args)
    fig=plt.figure()
    for i,arg in enumerate(args):
        ax=fig.add_subplot(2,3,i+1)
        plt.imshow(image_list[arg])

    fig.savefig('Activations for '+str(neuron))















