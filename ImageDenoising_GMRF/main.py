# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 09:15:11 2017

@author: Neha Pandey
"""
import numpy as np
import skimage.data
import scipy.sparse
from sklearn import preprocessing
import matplotlib.pyplot as plt
import pylab
import skimage.transform

def norm_image(img):
    norm=img.copy()
    for i in range(img.shape[2]):
        min_max_scaler = preprocessing.MinMaxScaler()
        norm[:, :, i] = min_max_scaler.fit_transform(img[: ,:, i])
    return norm

def q_matrix_gaussian_filter(img):
    imgShape = img.shape[0:2]
    nPixel = imgShape[0] * imgShape[1]
    qMat = scipy.sparse.lil_matrix( (nPixel,nPixel), dtype='float32')

    def vi(x0, x1):
        #return x0*imgShape[1] + x1
        return x1*imgShape[0] + x0


    for x0 in range(imgShape[0]):
        for x1 in range(imgShape[1]):

            vi0 = vi(x0,x1)

            # diagonal value
            qMat[vi0, vi0] = 4.0

            # right neighbor
            if x0 + 1 < imgShape[0]:
                vi1 = vi(x0 + 1,x1)
                qMat[vi0, vi1] = -1.0
                qMat[vi1, vi0] = -1.0

            # lower neighbor
            if x1 + 1 < imgShape[1]:
                vi1 = vi(x0, x1 + 1)
                qMat[vi0, vi1] = -1.0
                qMat[vi1, vi0] = -1.0

    return qMat
    
def optimize(noisy_img, Q, sigma):
    #solve AX=B, where A=(I + (sigma^2)*Q) and B=noisy_img
    A=scipy.sparse.identity(Q.shape[0]) + sigma**2 * Q
    B=noisy_img
    X = scipy.sparse.linalg.spsolve(A, B)
    return X
    
#loading image
img_i = skimage.data.astronaut().astype('float32')
#resizing image
img=skimage.transform.resize(img_i, (100, 100, img_i.shape[2]), order=1, preserve_range=True)
#Normalize image between 0 to 1
norm=norm_image(img)
error=[] 
for s in [0.1, 0.2, 0.5]:
    error_sig=[]
    for sigma in [0.1, 1, 2, 5, 10]:
        noise=np.random.normal(0, s, norm.shape)
        #Add gaussian noise
        noisy_img=np.clip(norm+noise, 0, 1)
        #Generate Q matrix
        Q=q_matrix_gaussian_filter(noisy_img)
        #Optimize
        optimized_img=noisy_img.copy()
        for i in range(noisy_img.shape[2]):
            optimized_img[:, :, i] = optimize(noisy_img[:,:,i].ravel(), Q, sigma).reshape(img[:, :, i].shape)
        #Normalize optimized image
        result=norm_image(optimized_img)
        #error matrix
        true=norm.ravel()
        res=result.ravel()
        error_sig.append(np.sum((true-res)**2))
        #Display and save result
        figure = plt.figure()
        
        ax1=figure.add_subplot(1,3,1)
        pylab.imshow(norm)
        ax1.set_title("Input Image")
        
        ax2=figure.add_subplot(1,3,2)
        pylab.imshow(noisy_img)
        ax2.set_title("Noisy Image")
        
        ax3=figure.add_subplot(1,3,3)
        pylab.imshow(result)
        ax3.set_title("Result Image")
        a=str(s)
        b=str(sigma)
        figure.suptitle('Results for s='+a+' & sigma='+b)
        figure.savefig('Results for s='+a.replace('.', ',')+' & sigma='+b.replace('.', ','))
    error.append(error_sig)
  
#Plotting error
sigma=[0.1, 1, 2, 5, 10]
fig1=plt.figure()
ax1=fig1.add_subplot(1,1,1)
ax1.plot(sigma, error[0],label='s=0.1')
ax1.plot(sigma, error[1],label='s=0.2')
ax1.plot(sigma, error[2],label='s=0.5')
ax1.set_xlabel('Sigma')
ax1.set_ylabel('Error')
ax1.legend(loc='best', shadow=True)
ax1.grid()
fig1.suptitle('Error for different noise levels, s and sigmas')
fig1.savefig('error')      
        
        
    


        
        
    


