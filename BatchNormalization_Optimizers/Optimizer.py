# -*- coding: utf-8 -*-
"""
Created on Sun Jan  1 15:55:53 2017

@author: NehaPandey
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle as pkl
import os

# Tensorflow
import theano as th

# KERAS
from keras import backend as K
from keras.datasets import cifar10
from keras.models import Sequential
from keras.utils import np_utils
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras import optimizers

img_rows = 32
img_cols = 32
nb_classes = 10
batch_size = 64
nb_epoch = 10

K.set_image_dim_ordering('th')
K.image_dim_ordering()

# loading CIFAR-10 dataset

(X_train, y_train), (X_test, y_test) = cifar10.load_data()

if K.image_dim_ordering() == 'th':
    X_train = X_train.reshape(X_train.shape[0], 3, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 3)
    X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

is_optimizer_sgd= False


#Model

model = Sequential()
model.add(Convolution2D(32, 3, 3, border_mode='same', name='conv1_1', input_shape=input_shape))
model.add(Activation('relu'))
model.add(Convolution2D(32, 3, 3, border_mode='valid', name='conv1_2'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Convolution2D(64, 3, 3, border_mode='same', name='conv1_3'))
model.add(Activation('relu'))
model.add(Convolution2D(64, 3, 3, border_mode='valid', name='conv1_4'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes, name='score'))
model.add(Activation('softmax'))

lrs=[0.01]
momentums=[0.9]


if is_optimizer_sgd:
    for lr in lrs:
        for momentum in momentums:
            optimizer = optimizers.SGD(lr=lr, momentum=momentum)
            filename = 'sgd'+ ' lr=' + str(lr).replace(".", ",") + ' momentum=' + str(momentum).replace(".", ",")
            model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            weights_path = '{}.h5'.format(filename)
            history_path = '{}.pkl'.format(filename)
            if not os.path.exists(weights_path):
                # run training
                history = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=2,
                                    validation_data=(X_test, Y_test))
                # save model
                model.save_weights(weights_path)
                # save history
                with open(history_path, 'wb') as outfile:
                    pkl.dump(history.history, outfile)
                history = history.history
            else:
                # load pre-trained weights
                model.load_weights(weights_path)
                # load history
                with open(history_path, 'r') as infile:
                    history = pkl.load(infile)

            fig=plt.figure()
            fig.subplots_adjust(hspace=0.5)
            ax1 = fig.add_subplot(2, 1, 1)
            ax2 = fig.add_subplot(2, 1, 2)
            ax1.plot(np.arange(1, nb_epoch + 1), history['loss'], linestyle='--', label='train')
            ax1.plot(np.arange(1, nb_epoch + 1), history['val_loss'], label='test')
            ax2.plot(np.arange(1, nb_epoch + 1), history['acc'], linestyle='--', label='train')
            ax2.plot(np.arange(1, nb_epoch + 1), history['val_acc'], label='test')
            ax1.set_xlabel("#Epochs")
            ax1.set_ylabel("Loss")
            ax2.set_xlabel("#Epochs")
            ax2.set_ylabel("Accuracy")

            ax1.legend(loc='best', shadow=True)
            ax2.legend(loc='best', shadow=True)

            ax1.grid()
            ax2.grid()

            fig.savefig(filename)
    score = model.evaluate(X_test, Y_test, verbose=0)
    print('Test score SGD:', score[0])
    print('Test accuracy SGD:', score[1])


else:
    for lr in lrs:
        optimizer = optimizers.Adagrad(lr=lr,decay=1e-5)
        filename = 'adagrad'+ ' lr=' + str(lr).replace(".", ",")+'decay=1e-5'
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        weights_path = '{}.h5'.format(filename)
        history_path = '{}.pkl'.format(filename)
        if not os.path.exists(weights_path):
            # run training
            history = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=2,
                                validation_data=(X_test, Y_test))
            # save model
            model.save_weights(weights_path)
            # save history
            with open(history_path, 'wb') as outfile:
                pkl.dump(history.history, outfile)
            history = history.history
        else:
            # load pre-trained weights
            model.load_weights(weights_path)
            # load history
            with open(history_path, 'r') as infile:
                history = pkl.load(infile)

        fig = plt.figure()
        fig.subplots_adjust(hspace=0.5)
        ax1 = fig.add_subplot(2, 1, 1)
        ax2 = fig.add_subplot(2, 1, 2)
        ax1.plot(np.arange(1, nb_epoch + 1), history['loss'], linestyle='--', label='train')
        ax1.plot(np.arange(1, nb_epoch + 1), history['val_loss'], label='test')
        ax2.plot(np.arange(1, nb_epoch + 1), history['acc'], linestyle='--', label='train')
        ax2.plot(np.arange(1, nb_epoch + 1), history['val_acc'], label='test')
        ax1.set_xlabel("#Epochs")
        ax1.set_ylabel("Loss")
        ax2.set_xlabel("#Epochs")
        ax2.set_ylabel("Accuracy")

        ax1.legend(loc='best', shadow=True)
        ax2.legend(loc='best', shadow=True)

        ax1.grid()
        ax2.grid()

        fig.savefig(filename)

    score = model.evaluate(X_test, Y_test, verbose=0)
    print('Test score Adagrad:', score[0])
    print('Test accuracy Adagrad:', score[1])



filename1='sgd'+ ' lr=' + str(0.01).replace(".", ",") + ' momentum=' + str(0.9).replace(".", ",")+'.pkl'
filename2='adagrad'+ ' lr=' + str(0.01).replace(".", ",")+'decay=1e-5'+'.pkl'
fig3=plt.figure()
fig3.subplots_adjust(hspace=0.5)
ax3 = fig3.add_subplot(2, 1, 1)
ax4 = fig3.add_subplot(2, 1, 2)

with open(filename1, 'r') as infile:
    history = pkl.load(infile)
ax3.plot(np.arange(1, nb_epoch+1), history['loss'], linestyle='--', label='SGD_train')
ax3.plot(np.arange(1, nb_epoch+1), history['val_loss'], label='SGD_test')
ax4.plot(np.arange(1, nb_epoch+1), history['acc'], linestyle='--', label='SGD_train')
ax4.plot(np.arange(1, nb_epoch+1), history['val_acc'], label='SGD_test')

with open(filename2, 'r') as infile:
    history = pkl.load(infile)
ax3.plot(np.arange(1, nb_epoch+1), history['loss'], linestyle='--', label='ADAGRAD_train')
ax3.plot(np.arange(1, nb_epoch+1), history['val_loss'], label='ADAGRAD_test')
ax4.plot(np.arange(1, nb_epoch+1), history['acc'], linestyle='--', label='ADAGRAD_train')
ax4.plot(np.arange(1, nb_epoch+1), history['val_acc'], label='ADAGRAD_test')

ax3.set_xlabel("#Epochs")
ax3.set_ylabel("Loss")
ax4.set_xlabel("#Epochs")
ax4.set_ylabel("Accuracy")

ax3.legend(loc='best', shadow=True)
ax4.legend(loc='best', shadow=True)

ax3.grid()
ax4.grid()

fig3.savefig('fig3')