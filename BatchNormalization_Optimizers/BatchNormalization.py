# -*- coding: utf-8 -*-
"""
Created on Sun Jan  1 15:54:43 2017

@author: NehaPandey
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle as pkl
import os

# Tensorflow
import theano as th

# KERAS
from keras import backend as K
from keras.datasets import cifar10
from keras.models import Sequential
from keras.utils import np_utils
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import BatchNormalization

img_rows = 32
img_cols = 32
nb_classes = 10
batch_size = 128
nb_epoch = 10

K.set_image_dim_ordering('th')
K.image_dim_ordering()

# loading CIFAR-10 dataset

(X_train, y_train), (X_test, y_test) = cifar10.load_data()

if K.image_dim_ordering() == 'th':
    X_train = X_train.reshape(X_train.shape[0], 3, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 3)
    X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

use_batch_norm= False

#Model
model = Sequential()
model.add(Convolution2D(32, 3, 3, border_mode='same', name='conv1_1', input_shape=input_shape))
if use_batch_norm:
    model.add(BatchNormalization(axis=1))

model.add(Activation('relu'))
model.add(Convolution2D(32, 3, 3, border_mode='valid', name='conv1_2'))
if use_batch_norm:
    model.add(BatchNormalization(axis=1))

model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Convolution2D(64, 3, 3, border_mode='same', name='conv1_3'))
if use_batch_norm:
    model.add(BatchNormalization(axis=1))

model.add(Activation('relu'))
model.add(Convolution2D(64, 3, 3, border_mode='valid', name='conv1_4'))
if use_batch_norm:
    model.add(BatchNormalization(axis=1))

model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes, name='score'))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])


#Training model
if use_batch_norm:
    filename='with_batch_norm'
else:
    filename='without_batch_norm'

weights_path = '{}.h5'.format(filename)
history_path = '{}.pkl'.format(filename)
if not os.path.exists(weights_path):
    # run training
    history = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=2,
                        validation_data=(X_test, Y_test))
    # save model
    model.save_weights(weights_path)
    # save history
    with open(history_path, 'wb') as outfile:
        pkl.dump(history.history, outfile)
    history = history.history
else:
    # load pre-trained weights
    model.load_weights(weights_path)
    # load history
    with open(history_path, 'r') as infile:
        history = pkl.load(infile)

fig=plt.figure()
fig.subplots_adjust(hspace=0.5)
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)
with open('with_batch_norm.pkl', 'r') as infile:
    history = pkl.load(infile)
ax1.plot(np.arange(1, nb_epoch+1), history['loss'], linestyle='--', label='without_batch_norm_train')
ax1.plot(np.arange(1, nb_epoch+1), history['val_loss'], label='without_batch_norm_test')
ax2.plot(np.arange(1, nb_epoch+1), history['acc'], linestyle='--', label='without_batch_norm_train')
ax2.plot(np.arange(1, nb_epoch+1), history['val_acc'], label='without_batch_norm_test')

with open('without_batch_norm.pkl', 'r') as infile:
    history = pkl.load(infile)
ax1.plot(np.arange(1, nb_epoch+1), history['loss'], linestyle='--', label='with_batch_norm_train')
ax1.plot(np.arange(1, nb_epoch+1), history['val_loss'], label='with_batch_norm_test')
ax2.plot(np.arange(1, nb_epoch+1), history['acc'], linestyle='--', label='with_batch_norm_train')
ax2.plot(np.arange(1, nb_epoch+1), history['val_acc'], label='with_batch_norm_test')

ax1.set_xlabel("#Epochs")
ax1.set_ylabel("Loss")
ax2.set_xlabel("#Epochs")
ax2.set_ylabel("Accuracy")

ax1.legend(loc='best', shadow=True)
ax2.legend(loc='best', shadow=True)

ax1.grid()
ax2.grid()

fig.savefig('fig1')

score = model.evaluate(X_test, Y_test, verbose=0)
print('Test score without batch norm:', score[0])
print('Test accuracy without batch norm:', score[1])
