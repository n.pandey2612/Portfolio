# -*- coding: utf-8 -*-
"""
Created on Wed May 31 12:34:19 2017

@author: Neha Pandey
"""

import networkx as nx
import random
import numpy as np

beta=0.01
unaries_0=[random.uniform(0,1) for _ in range(20)]
unaries_1=[random.uniform(0,1) for _ in range(20)]

def argmin_path_dijkstra(var, beta, unaries_0, unaries_1): 
    G=nx.Graph()
    G.clear()
    G.add_node(0) #Adding source
    G.add_node(2*var+1) #Adding target
    for i in range(1, var):
        G.add_node(i) #Adding 0 state of ith node
        G.add_node(var+i) #Adding 1 state of ith node
        
        G.add_edge(i, i+1, weight=0+unaries_0[i]) #Edge from i in 0 state to i+1 in 0 state
        G.add_edge(var+i, i+1, weight=beta+unaries_0[i]) #Edge from i in 1 state to i+1 in 0 state
        
        G.add_edge(i, var+i+1, weight=beta+unaries_1[i]) #Edge from i in 0 state to i+1 in 1 state
        G.add_edge(var+i, var+i+1, weight=0+unaries_1[i]) #Edge from i in 1 state to i+1 in 1 state
    
    #Connecting source to the first two nodes    
    G.add_edge(0, 1, weight=0+unaries_0[0])
    G.add_edge(0, var+1, weight=0+unaries_1[0])
    #Connecting target to the last two nodes 
    G.add_edge(var, 2*var+1, weight=0)
    G.add_edge(2*var, 2*var+1, weight=0) 
    #Finding short path using Dijkstra Algorithm
    short_path=np.array(nx.dijkstra_path(G, source=0, target= 2*var+1))
    #Getting argmin in form of binary states of the variables
    for i in range(np.size(short_path)):
        if(short_path[i]>var):
            short_path[i]=1
        else:
            short_path[i]=0
    #Clipping source and target to get final argmin
    argmin=short_path[1:var+1]
    return argmin

#problem 1
arg=argmin_path_dijkstra(20, beta, unaries_0, unaries_1)
print(arg)
    
#problem 2
def split_unaries(unaries):
    u_H=[]
    u_V=[]
    for u in unaries:
        u_h=random.uniform(0,u)
        u_H.append(u_h)
        u_v=u-u_h
        u_V.append(u_v)
    return u_H, u_V
    
def min_path_grid(num_rows, num_columns, beta, unaries_0, unaries_1):
    if(num_rows!=num_columns):
        raise NameError('Not a square grid')
    E_h=[]
    E_v=[]
    for i in range(num_rows):
        unaries_0_h, unaries_0_v=split_unaries(unaries_0)
        unaries_1_h, unaries_1_v=split_unaries(unaries_1)
        E_h.append(argmin_path_dijkstra(num_columns, beta, unaries_0_h, unaries_1_h))
        E_v.append(argmin_path_dijkstra(num_rows, beta, unaries_0_v, unaries_1_v))
    np.vstack(E_h)
    np.hstack(E_v)
    return np.array(E_h), np.array(E_v)

E_h, E_v=min_path_grid(20, 20, beta, unaries_0, unaries_1)
        
    
    
    
    