%QUATERNION_INTERFACE Quaternion MATLAB class wrapper to an underlying C++ class
classdef Quaternion_interface < handle
    properties (SetAccess = private, Hidden = true)
        objectHandle;
     % Handle to the underlying C++ class instance
    end
    methods
        %% Constructor - Create a new C++ class instance 
        function this = Quaternion_interface(varargin)
            this.objectHandle = Quaternion_mex('new', varargin{:});
        end
        
        %% Destructor - Destroy the C++ class instance
        function delete(this)
            Quaternion_mex('delete', this.objectHandle);
        end

        %% Train - an Quaternion class method call
        function varargout = train(this, varargin)
            [varargout{1:nargout}] = Quaternion_mex('train', this.objectHandle, varargin{:});
        end

        %% Test - another Quaternion class method call
        function varargout = test(this, varargin)
            [varargout{1:nargout}] = Quaternion_mex('test', this.objectHandle, varargin{:});
        end
    end
end
