#ifndef UNIT_QUATERNION_H
#define UNIT_QUATERNION_H

#include<math.h>
#include<quat.h>

class Unit_Quaternion: public Quaternion
{
    public:
        Unit_Quaternion(double real1, double img_i1, double img_j1, double img_k1): Quaternion(real1, img_i1, img_j1, img_k1)
        {
            Normalize();
        }
}


#endif
