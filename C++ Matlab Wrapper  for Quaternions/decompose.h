#ifndef DECOMPOSE_H
#define DECOMPOSE_H

#include<math.h>
#include<quat.h>
#include<unit_quat.h>

Quaternion decompose(char sign, Unit_Quaternion unit_1, Quaternion q_in, Unit_Quaternion unit_2)
{
    Quaternion q_out;
    if(sign == '+')
    {
        q_out =  (0.5*(q_in + ((unit_1)*q_in*(unit_2))));
    }
    else
    {
        q_out =  (0.5*(q_in - ((unit_1)*q_in*(unit_2))));      
    }
}

Quaternion decompose(char sign, Unit_Quaternion unit_1, Unit_Quaternion unit_2, Quaternion q_in, Unit_Quaternion unit_3)
{
    Quaternion q_out;
    if(sign == '+')
    {
        q_out =  (0.5*(q_in + (unit_1*unit_2*q_in*(unit_3))));
    }
    else
    {
        q_out =  (0.5*(q_in - (unit_1*unit_2*q_in*(unit_3))));
    }
}

Quaternion decompose(char sign, Unit_Quaternion unit_1, Quaternion q_in, Unit_Quaternion unit_2, Unit_Quaternion unit_3)
{
    Quaternion q_out;
    if(sign == '+')
    {
        q_out =  (0.5*(q_in + ((unit_1)*q_in*(unit_2)*unit_3)));
    }
    else
    {
        q_out =  (0.5*(q_in - ((unit_1)*q_in*(unit_2)*unit_3)));      
    }
}

Quaternion decompose(char sign, Unit_Quaternion unit_1, Unit_Quaternion unit_2, Quaternion q_in, Unit_Quaternion unit_3, Unit_Quaternion unit_4)
{
    Quaternion q_out;
    if(sign == '+')
    {
        q_out =  (0.5*(q_in + ((unit_1)*(unit_2)*q_in*(unit_3)*unit_4)));
    }
    else
    {
        q_out =  (0.5*(q_in + ((unit_1)*(unit_2)*q_in*(unit_3)*unit_4)));     
    }
}




#endif
