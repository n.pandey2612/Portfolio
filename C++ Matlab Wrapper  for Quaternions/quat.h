#ifndef QUATERNION_H
#define QUATERNION_H
#include<mex.h>
#include<math.h>
using namespace std;

class Qauternion
{
protected:
   double real, img_i, img_j, img_k;
public:
   Qauternion();
   Qauternion(double a);
   Quaternion(double real1, double img_i1, double img_j1, double img_k1);
   void Conjugate();
   double Norm();
   void operator+=(const Quaternion& rhs);
   void operator-=(const Quaternion& rhs);
   void operator*=(const Quaternion& rhs);
   void operator/=(const Quaternion& rhs);
   void operator=(const Quaternion& rhs);
   void inCrossProd(const Quaternion& rhs);
   void normalize();
   
}
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	
    // Get the command string
    char cmd[64];
	if (nrhs < 1 || mxGetString(prhs[0], cmd, sizeof(cmd)))
		mexErrMsgTxt("First input should be a command string less than 64 characters long.");
        
    // New
    if (!strcmp("new", cmd)) {
        // Check parameters
        if (nlhs != 1)
            mexErrMsgTxt("New: One output expected.");
        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<Quaternion>(new Quaternion);
        return;
    }
    
    // Check there is a second input, which should be the class instance handle
    if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");
    
    // Delete
    if (!strcmp("delete", cmd)) {
        // Destroy the C++ object
        destroyObject<Quaternion>(prhs[1]);
        // Warn if other commands were ignored
        if (nlhs != 0 || nrhs != 2)
            mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
        return;
    }
    
    // Get the class instance pointer from the second input
    Quaternion* Quaternion_instance = convertMat2Ptr<Quaternion>(prhs[1]);
    
    // Call the various class methods
    // Train    
    if (!strcmp("train", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Train: Unexpected arguments.");
        // Call the method
        Quaternion_instance->train();
        return;
    }
    // Test    
    if (!strcmp("test", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Test: Unexpected arguments.");
        // Call the method
        Quaternion_instance->test();
        return;
    }
    
    // Got here, so command not recognized
    mexErrMsgTxt("Command not recognized.");
}
Qauternion::Quaternion()
{
  real = 0; img_i = 0; img_j = 0, img_k = 0;
}
Quaternion::Qauternion(double a)
{
  real = a; img_i = 0; img_j = 0, img_k = 0;
}
Qauternion::Quaternion(double real1, double img_i1, double img_j1, double img_k1)
{
  real = real1; img_i = img_i1; img_j = img_j1; img_k = img_k1;
}
void Quaternion::Conjugate()
{
  real = real; img_i = -img_i; img_j = -img_j; img_k = -img_k;
}
double Quaternion::Norm()
{
   return math.sqrt(real**2 + img_i**2 + img_j**2 + img_k**2);
}
void Qauternion::operator+=(const Quaternion& rhs)
{
    real += rhs.real;
    img_i += rhs.img_i;
    img_j += rhs.img_j;
    img_k += rhs.img_k;
}
void Quaternion::operator-=(const Quaternion& rhs)
{
    real -= rhs.real;
    img_i -= rhs.img_i;
    img_j -= rhs.img_j;
    img_k -= rhs.img_k;
}
void Quaternion::operator*=(const Quaternion& rhs)
{
    a = real*rhs.real - img_i*rhs.img_i - img_j*rhs.img_j - img_k*rhs.img_k;
    b = real*rhs.img_i + img_i*rhs.real + img_j*rhs.img_k - img_k*rhs.img_j;
    c = real*rhs.img_j - img_i*rhs.img_k + img_j*rhs.real + img_k*rhs.img_i;
    d = real*rhs.img_k + img_i*rhs.img_j - img_j*rhs.img_i + img_k*rhs.real;
    real = a;
    img_i = b;
    img_j = c;
    img_k = d;
}
void Quaternion::operator*=(const double& lambda)
{
    real = lambda*real;
    img_i = lambda*img_i;
    img_j = lambda*img_j;
    img_k = lambda*img_k;
}
void Qauternion::normalize()
{ 
    double norm = Quaternion::Norm(); 
    real = real/norm; 
    img_i = img_i/norm; 
    img_j = img_j/norm; 
    img_k = img_k/norm;
}
Quaternion operator+(Qauternion q1, Quaternion q2)
{
    Qauternion temp;
    temp = q1;
    temp += q2;
    return temp;
}
Quaternion operator+(Qauternion q1, double rhs)
{
    Qauternion temp;
    q2 = Quaternion(rhs)
    temp = q1;
    temp += q2;
    return temp;
}
Quaternion operator+(double lhs, Quaternion q2)
{
    q1 = Quaternion(lhs);
    Qauternion temp;
    temp = q1;
    temp += q2;
    return temp;
}
Quaternion operator-(Qauternion q1, Quaternion q2)
{
    Qauternion temp;
    temp = q1;
    temp -= q2;
    return temp;
}
Quaternion operator-(double lhs, Quaternion q2)
{
    q1 = Qauternion(lhs);
    Qauternion temp;
    temp = q1;
    temp -= q2;
    return temp;
}
Quaternion operator-(Qauternion q1, double rhs)
{
    q2 = Quaternion(rhs);
    Qauternion temp;
    temp = q1;
    temp -= q2;
    return temp;
}
Quaternion operator*(Qauternion q1, Quaternion q2)
{
    Qauternion temp;
    temp = q1;
    temp *= q2;
    return temp;
}
Quaternion operator*(double lhs, Quaternion q2)
{
    Qauternion temp;
    temp = q2;
    temp *= lhs;
    return temp;
}
Quaternion operator*(Qauternion q1, double rhs)
{
    Qauternion temp;
    temp = q1;
    temp *= rhs;
    return temp;
}
#endif
