#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 11:09:56 2018

@author: npandey
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
#from statsmodels.tsa.ar_model import AR
import seaborn as sns
from scipy.signal import periodogram
from scipy.stats import kurtosis

def runningMeanFast(x, N):
    return np.convolve(x, np.ones((N,))/N, 'valid')[ : -1]

def runningSumFast(x, N):
    return np.convolve(x, np.ones((N,)), 'valid')[ : -1]

def prepareData(rv_data):
    month_hist_first_sample = rv_data[ : 22]
    week_hist_first_sample = month_hist_first_sample[-5 : ]    
    rv_d = rv_data[22 : -1]
    hist_w = np.concatenate((week_hist_first_sample, rv_d))
    hist_m = np.concatenate((month_hist_first_sample, rv_d))
    rv_d_future = rv_data[23 : ]
    
    #Calculating weekly and monthly RV
    rv_w = np.expand_dims(runningMeanFast(np.squeeze(hist_w), 5), axis = 1)
    rv_m = np.expand_dims(runningMeanFast(np.squeeze(hist_m), 22), axis = 1)    
    
    #Get X and y for HAR model
    X = np.concatenate((rv_d, rv_w, rv_m), axis = 1)
    y = rv_d_future
    return X, y, rv_w, rv_m, week_hist_first_sample, month_hist_first_sample

def R_SQUARE(y_true, y_pred):
    SSE = np.sum(np.square(y_true - y_pred))
    SST = np.sum(np.square(y_true - np.mean(y_true)))
    return 1 - (SSE/SST)
    
def RMSE(y_true, y_pred):
    return np.sqrt(np.mean(np.square(y_true - y_pred)))
 
def MAE(y_true, y_pred):
    return np.mean(np.absolute(y_true - y_pred))

def QLIKE(y_true, y_pred):
    return np.mean(np.divide(y_pred, y_true) - np.log(np.divide(y_pred, y_true)) - 1) 

def update_hist(x, y):
    x = np.delete(x, 0)
    x = np.insert(x, x.size, y)
    return x

def future_h_step(x, y, h):
    x_h_step = x[:len(x)-h]
    y_h_step = y[:len(y)-h]
    h_step = []
    for i in range(len(y_h_step)):
        h_step_i = y_test[i:i+h]
        h_step.append(h_step_i)
    h_step = np.array(h_step)
    return x_h_step, h_step
   
class HAR:
    def __init__(self):
        self.data = 0
        
    def fit(self, X, y):
        X = sm.add_constant(X)
        self.model = sm.OLS(y, X).fit(cov_type='HAC', cov_kwds={'maxlags':1})
        return self.model
    
    def summary(self):
        print(self.model.summary())
        
    def forecast(self, rv_d, week_hist, month_hist, H):
        preds = []
        day_curr = rv_d
        hist_buffer_week = week_hist
        hist_buffer_month = month_hist
        for h in range(H):
            x = np.array([1, day_curr, np.mean(hist_buffer_week), np.mean(hist_buffer_month)])
            day_next = self.model.predict(x)
            preds.append(day_next)
            hist_buffer_week = update_hist(hist_buffer_week, day_curr)
            hist_buffer_month = update_hist(hist_buffer_month, day_curr)
            day_curr = day_next
        return np.array(preds)
    
    
data = pd.read_csv('oxfordmanrealizedvolatilityindices.csv')
#data = pd.read_csv('neha_msft_wmt.csv')
data_spx = data.loc[data['Symbol'] == '.SPX']['rv5'] 
#data_wmt = data['wmt']
rv5 = np.expand_dims(10.00 * data_spx.values, axis = 1) #RV for 5 min subsampling
nb_train = 2000
#nb_test = 1000
buffer = 23


#Train HAR model
rv5_train = rv5[ : nb_train + buffer]
X_train, y_train, rv_w_is, rv_m_is, week_hist_first_sample, month_hist_first_sample = prepareData(rv5_train)

model_HAR = HAR()
res_HAR = model_HAR.fit(X_train, y_train) 
print(model_HAR.summary())

#1 Day-predictions HAR model

#in-sample 1 day predictions
#y_pred_is = np.expand_dims(model_HAR.predict(X_train), axis = 1)
y_pred_is = []
week_buff = week_hist_first_sample
month_buff = month_hist_first_sample
for i in range(len(X_train)):
    #print(i)
    x_i = X_train[i][0]
    pred_i = model_HAR.forecast(x_i, week_buff, month_buff, H = 1)
    y_pred_is.append(pred_i)
    week_buff = update_hist(week_buff, x_i)
    month_buff = update_hist(month_buff, x_i)
y_pred_is = np.squeeze(np.array(y_pred_is), axis = 2)
residuals_is = y_train - y_pred_is
rv_w_pred_is = runningMeanFast(np.squeeze(y_pred_is), 5)
rv_m_pred_is = runningMeanFast(np.squeeze(y_pred_is), 22)

#out-of-sample 1 day predictions
rv5_test = rv5[nb_train : ]
X_test, y_test, rv_w_os, rv_m_os, week_hist_first_sample, month_hist_first_sample = prepareData(rv5_test)
y_pred_os = []
week_buff = week_hist_first_sample
month_buff = month_hist_first_sample
for i in range(len(X_test)):
    x_i = X_test[i][0]
    pred_i = model_HAR.forecast(X_test[i][0], week_buff, month_buff, H = 1)
    y_pred_os.append(pred_i)
    week_buff = update_hist(week_buff, x_i)
    month_buff = update_hist(month_buff, x_i)
y_pred_os = np.squeeze(np.array(y_pred_os), axis = 2)
residuals_os = y_test - y_pred_os
rv_w_pred_os = runningMeanFast(np.squeeze(y_pred_os), 5)
rv_m_pred_os = runningMeanFast(np.squeeze(y_pred_os), 22)

#out-of-sample 1 week predictions
X_test_1week, y_test_1week = future_h_step(X_test, y_test, h = 5)
y_pred_os_1week = []
week_buff = week_hist_first_sample
month_buff = month_hist_first_sample
for i in range(len(X_test_1week)):
    #print(i)
    x_i = X_test_1week[i][0]
    pred_i = model_HAR.forecast(x_i, week_buff, month_buff, H = 5)
    y_pred_os_1week.append(pred_i)
    week_buff = update_hist(week_buff, x_i)
    month_buff = update_hist(month_buff, x_i)
y_pred_os_1week = np.array(y_pred_os_1week)

#out-of-sample 2 week predictions
X_test_2week, y_test_2week = future_h_step(X_test, y_test, h = 10)
y_pred_os_2week = []
week_buff = week_hist_first_sample
month_buff = month_hist_first_sample
for i in range(len(X_test_2week)):
    #print(i)
    x_i = X_test_2week[i][0]
    pred_i = model_HAR.forecast(x_i, week_buff, month_buff, H = 10)
    y_pred_os_2week.append(pred_i)
    week_buff = update_hist(week_buff, x_i)
    month_buff = update_hist(month_buff, x_i)
y_pred_os_2week = np.array(y_pred_os_2week)

#getting errors
errors_os = {}
errors_is = {}
errors_os_1week = {}
errors_os_2week = {}

errors_is["R2"] = R_SQUARE(y_train, y_pred_is)
errors_is["RMSE"] = RMSE(y_train, y_pred_is)
errors_is["MAE"] = MAE(y_train, y_pred_is) 
errors_is["QLIKE"] = QLIKE(y_train, y_pred_is)
print('Errors for in-sample 1 day predictions: ')
print(errors_is)

errors_os["R2"] = R_SQUARE(y_test, y_pred_os)
errors_os["RMSE"] = RMSE(y_test, y_pred_os)
errors_os["MAE"] = MAE(y_test, y_pred_os) 
errors_os["QLIKE"] = QLIKE(y_test, y_pred_os)
print('Errors for out-of-sample 1 day predictions: ')
print(errors_os)

errors_os_1week["R2"] = R_SQUARE(y_test_1week, y_pred_os_1week)
errors_os_1week["RMSE"] = RMSE(y_test_1week, y_pred_os_1week)
errors_os_1week["MAE"] = MAE(y_test_1week, y_pred_os_1week) 
errors_os_1week["QLIKE"] = QLIKE(y_test_1week, y_pred_os_1week)
print('Errors for out-of-sample 1 week predictions: ')
print(errors_os_1week)

errors_os_2week["R2"] = R_SQUARE(y_test_2week, y_pred_os_2week)
errors_os_2week["RMSE"] = RMSE(y_test_2week, y_pred_os_2week)
errors_os_2week["MAE"] = MAE(y_test_2week, y_pred_os_2week) 
errors_os_2week["QLIKE"] = QLIKE(y_test_2week, y_pred_os_2week)
print('Errors for out-of-sample 2 weeks predictions: ')
print(errors_os_2week)


#getting kutosis
kurtosis_rv_os = {}
kurtosis_rv_is = {}

kurtosis_rv_is['actual daily'] = kurtosis(np.squeeze(y_train))
kurtosis_rv_is['actual weekly'] = kurtosis(np.squeeze(rv_w_is))
kurtosis_rv_is['actual monthly'] = kurtosis(np.squeeze(rv_m_is))
kurtosis_rv_is['HAR daily'] = kurtosis(np.squeeze(y_pred_is))
kurtosis_rv_is['HAR weekly'] = kurtosis(np.squeeze(rv_w_pred_is))
kurtosis_rv_is['HAR monthly'] = kurtosis(np.squeeze(rv_m_pred_is))
print('Kurtosis values for in_sample 1 day predictions: ')
print(kurtosis_rv_is)

kurtosis_rv_os['actual daily'] = kurtosis(np.squeeze(y_test))
kurtosis_rv_os['actual weekly'] = kurtosis(np.squeeze(rv_w_os))
kurtosis_rv_os['actual monthly'] = kurtosis(np.squeeze(rv_m_os))
kurtosis_rv_os['HAR daily'] = kurtosis(np.squeeze(y_pred_os))
kurtosis_rv_os['HAR weekly'] = kurtosis(np.squeeze(rv_w_pred_os))
kurtosis_rv_os['HAR monthly'] = kurtosis(np.squeeze(rv_m_pred_os))
print('Kurtosis values for out_of_sample 1 day predictions: ')
print(kurtosis_rv_os)



#Plot actual and forecast RVs and residuals
fig1 = plt.figure(figsize = (10, 10))
fig1.subplots_adjust(hspace = 0.7)
fig1.subplots_adjust(hspace = 0.7)
ax11 = fig1.add_subplot(2, 2, 1)
plt.plot(y_train, 'r', label = 'Actual') 
plt.plot(y_pred_is, 'b', label = 'Forecast')
#plt.xlabel('Days')
plt.ylabel('Returns')
ax11.set_title("In Sample")
plt.grid()
plt.legend()

ax12 = fig1.add_subplot(2, 2, 2)
plt.plot(residuals_is)
ax12.set_title("Residuals")
ax12.grid()

ax13 = fig1.add_subplot(2, 2, 3)
plt.plot(y_test, 'r', label = 'Actual') 
plt.plot(y_pred_os, 'b', label = 'Forecast')
#plt.xlabel('Days')
plt.ylabel('Returns')
ax13.set_title("Out of Sample")
plt.grid()
plt.legend()

ax14 = fig1.add_subplot(2, 2, 4)
plt.plot(residuals_os)
ax14.set_title("Residuals")
ax14.grid()

fig1.savefig('dailyReturns.png')

#Autocorrelation plots
fig2 = plt.figure(figsize = (10, 5))
#fig2.subplots_adjust(hspace = 0.5)
ax21 = fig2.add_subplot(1, 2, 1)
ax21.set_xlim(left = 0, right = 100)
ax21.acorr(np.squeeze(y_train), usevlines = False, maxlags = 100, linestyle = 'solid', marker = None, label = 'Actual')
ax21.acorr(np.squeeze(y_pred_is), usevlines = False, maxlags = 100, linestyle = 'dotted', marker = None, label = 'Forecast')
ax21.grid()
plt.legend()
plt.xlabel('k-values')
plt.ylabel('sacf values')
ax21.set_title("In Sample")

ax22 = fig2.add_subplot(1, 2, 2)
ax22.set_xlim(left = 0, right = 100)
ax22.acorr(np.squeeze(y_test), usevlines = False, maxlags = 100, linestyle = 'solid', marker = None, label = 'Actual')
ax22.acorr(np.squeeze(y_pred_os), usevlines = False, maxlags = 100, linestyle = 'dotted', marker = None, label = 'Forecast')
ax22.grid()
plt.legend()
plt.xlabel('k-values')
plt.ylabel('sacf values')
ax22.set_title("Out of Sample")

fig2.savefig('autocorrelation.png')
fig2.suptitle('Autocorrelation of RVs')

#Distribution plot
fig3 = plt.figure(figsize = (15, 5))
ax31 = fig3.add_subplot(1, 3, 1)
sns.kdeplot(np.squeeze(y_train), label = 'Actual')
sns.kdeplot(np.squeeze(y_pred_is), label = 'Forecast')
plt.legend()
plt.grid()
ax31.set_title('Daily')
ax32 = fig3.add_subplot(1, 3, 2)
sns.kdeplot(np.squeeze(rv_w_is), label = 'Actual')
sns.kdeplot(np.squeeze(rv_w_pred_is), label = 'Forecast')
plt.legend()
plt.grid()
ax32.set_title('Weekly')
ax33 = fig3.add_subplot(1, 3, 3)
sns.kdeplot(np.squeeze(rv_m_is), label = 'Actual')
sns.kdeplot(np.squeeze(rv_m_pred_is), label = 'Forecast')
plt.legend()
plt.grid()
ax33.set_title('Monthly')
fig3.savefig('distribution.png')
fig3.suptitle('Distribution of RVs_in')

fig33 = plt.figure(figsize = (15, 5))
ax331 = fig33.add_subplot(1, 3, 1)
sns.kdeplot(np.squeeze(y_test), label = 'Actual')
sns.kdeplot(np.squeeze(y_pred_os), label = 'Forecast')
plt.legend()
plt.grid()
ax331.set_title('Daily')
ax332 = fig33.add_subplot(1, 3, 2)
sns.kdeplot(np.squeeze(rv_w_os), label = 'Actual')
sns.kdeplot(np.squeeze(rv_w_pred_os), label = 'Forecast')
plt.legend()
plt.grid()
ax332.set_title('Weekly')
ax333 = fig33.add_subplot(1, 3, 3)
sns.kdeplot(np.squeeze(rv_m_os), label = 'Actual')
sns.kdeplot(np.squeeze(rv_m_pred_os), label = 'Forecast')
plt.legend()
plt.grid()
ax333.set_title('Monthly')
fig33.savefig('distribution.png')
fig33.suptitle('Distribution of RVs_out')

#Periodograms
fig4 = plt.figure(figsize = (10, 10))
fig4.subplots_adjust(hspace = 0.5)

ax41 = fig4.add_subplot(2, 2, 1)
f, Pxx_den = periodogram(np.squeeze(y_train))
plt.loglog(f, Pxx_den)
ax41.set_title("Actual Returns in-sample")
ax41.grid()
ax42 = fig4.add_subplot(2, 2, 2)
f, Pxx_den = periodogram(np.squeeze(y_pred_is))
plt.loglog(f, Pxx_den)
plt.xlabel('log cycles')
ax42.set_title("Forecast in-sample")
ax42.grid()

ax43 = fig4.add_subplot(2, 2, 3)
f, Pxx_den = periodogram(np.squeeze(y_test))
plt.loglog(f, Pxx_den)
ax43.set_title("Actual Returns out-sample")
ax43.grid()
ax44 = fig4.add_subplot(2, 2, 4)
f, Pxx_den = periodogram(np.squeeze(y_pred_os))
plt.loglog(f, Pxx_den)
plt.xlabel('log cycles')
ax44.set_title("Forecast out-sample")
ax44.grid()


fig4.savefig('periodogram.png')
fig4.suptitle('log-log Periodogram')








