#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 15:46:52 2018

@author: npandey
"""

import numpy as np
import pandas as pd
import statsmodels.api as sm
from collections import deque

def R_SQUARE(y_true, y_pred):
    SSE = np.sum(np.square(y_true - y_pred))
    SST = np.sum(np.square(y_true - np.mean(y_true)))
    return 1 - (SSE/SST)
    
def RMSE(y_true, y_pred):
    return np.sqrt(np.mean(np.square(y_true - y_pred)))
 
def MAE(y_true, y_pred):
    return np.mean(np.absolute(y_true - y_pred))

def QLIKE(y_true, y_pred):
    return np.mean(np.divide(y_pred, y_true) - np.log(np.divide(y_pred, y_true)) - 1) 


def future_h_step(x, y, h):
    x_h_step = x[:len(x)-h]
    y_h_step = y[:len(y)-h]
    h_step = []
    for i in range(len(y_h_step)):
        h_step_i = y_test[i:i+h]
        h_step.append(h_step_i)
    h_step = np.array(h_step)
    return x_h_step, h_step
   
class AR:
    def __init__(self, h):
        self.data = 0
        self.h = h
    
    def preprocess(self, data, nb_train):
        X_train = []
        X_test = []
        """
        rv5_test = rv5[nb_train : ]
        X_test = rv5_test[ : len(rv5_test) - h]
        y_test = rv5_test[h : ]
        """
        for i in range(self.h):
            X_train.append(data[i : nb_train + i])
            X_test.append(data[nb_train + i: len(data) - nb_train - self.h + i])
        #return X_train, X_test
        X_train = np.concatenate(X_train, axis = 1)
        X_train = sm.add_constant(X_train)
        X_test = np.concatenate(X_test, axis = 1)
        X_test = sm.add_constant(X_test)
        
        y_train = data[h : nb_train + h]
        y_test = data[nb_train + h : len(data) - nb_train]
        return X_train, y_train, X_test, y_test        
        
    def fit(self, X, y):
        self.model = sm.OLS(y, X).fit(cov_type='HAC', cov_kwds={'maxlags':1})
        return self.model, X, y
    
    def summary(self):
        print(self.model.summary())
        
    def forecast(self, x_in, H):
        preds = []
        x_curr = x_in
        for h in range(H):
            x_next = self.model.predict(x_curr)
            preds.append(x_next)
            buffer = list(x_curr)
            buffer.insert(1, x_next)
            buffer.pop()
            x_curr = np.array(buffer)
        return np.array(preds)
    
    
#data = pd.read_csv('oxfordmanrealizedvolatilityindices.csv')
data = pd.read_csv('neha_msft_wmt.csv')
#data_spx = data.loc[data['Symbol'] == '.SPX']['rv5'] 
data_wmt = data['wmt']
rv5 = np.expand_dims(10.00 * data_wmt.values, axis = 1) #RV for 5 min subsampling
nb_train = 2000
h = 22 #order of the AR model


#Train AR model
#X_train = rv5[ : nb_train]
#y_train = rv5[h : nb_train + h]
model_AR = AR(h)
X_train, y_train, X_test, y_test = model_AR.preprocess(rv5, nb_train)


res_AR, X_train, y_train = model_AR.fit(X_train, y_train) 
print(model_AR.summary())

#1 Day-predictions HAR model

#in-sample 1 day predictions
#y_pred_is = np.expand_dims(model_HAR.predict(X_train), axis = 1)
y_pred_is = []
for i in range(len(X_train)):
    #print(i)
    x_i = X_train[i, :]
    pred_i = model_AR.forecast(x_i, H = 1)
    y_pred_is.append(pred_i)

y_pred_is = np.squeeze(np.array(y_pred_is), axis = 2)
residuals_is = y_train - y_pred_is


#out-of-sample 1 day predictions

y_pred_os = []
for i in range(len(X_test)):
    x_i = X_test[i, :]
    pred_i = model_AR.forecast(X_test[i], H = 1)
    y_pred_os.append(pred_i)
y_pred_os = np.squeeze(np.array(y_pred_os), axis = 2)
residuals_os = y_test - y_pred_os


#out-of-sample 1 week predictions
X_test_1week, y_test_1week = future_h_step(X_test, y_test, h = 5)
y_pred_os_1week = []

for i in range(len(X_test_1week)):
    #print(i)
    x_i = X_test_1week[i, :]
    pred_i = model_AR.forecast(x_i, H = 5)
    y_pred_os_1week.append(pred_i)
y_pred_os_1week = np.array(y_pred_os_1week)

#out-of-sample 2 week predictions
X_test_2week, y_test_2week = future_h_step(X_test, y_test, h = 10)
y_pred_os_2week = []
for i in range(len(X_test_2week)):
    #print(i)
    x_i = X_test_2week[i, :]
    pred_i = model_AR.forecast(x_i, H = 10)
    y_pred_os_2week.append(pred_i)
y_pred_os_2week = np.array(y_pred_os_2week)

#getting errors
errors_os = {}
errors_is = {}
errors_os_1week = {}
errors_os_2week = {}

errors_is["R2"] = R_SQUARE(y_train, y_pred_is)
errors_is["RMSE"] = RMSE(y_train, y_pred_is)
errors_is["MAE"] = MAE(y_train, y_pred_is) 
errors_is["QLIKE"] = QLIKE(y_train, y_pred_is)
print('Errors for in-sample 1 day predictions: ')
print(errors_is)

errors_os["R2"] = R_SQUARE(y_test, y_pred_os)
errors_os["RMSE"] = RMSE(y_test, y_pred_os)
errors_os["MAE"] = MAE(y_test, y_pred_os) 
errors_os["QLIKE"] = QLIKE(y_test, y_pred_os)
print('Errors for out-of-sample 1 day predictions: ')
print(errors_os)

errors_os_1week["R2"] = R_SQUARE(y_test_1week, y_pred_os_1week)
errors_os_1week["RMSE"] = RMSE(y_test_1week, y_pred_os_1week)
errors_os_1week["MAE"] = MAE(y_test_1week, y_pred_os_1week) 
errors_os_1week["QLIKE"] = QLIKE(y_test_1week, y_pred_os_1week)
print('Errors for out-of-sample 1 week predictions: ')
print(errors_os_1week)

errors_os_2week["R2"] = R_SQUARE(y_test_2week, y_pred_os_2week)
errors_os_2week["RMSE"] = RMSE(y_test_2week, y_pred_os_2week)
errors_os_2week["MAE"] = MAE(y_test_2week, y_pred_os_2week) 
errors_os_2week["QLIKE"] = QLIKE(y_test_2week, y_pred_os_2week)
print('Errors for out-of-sample 2 weeks predictions: ')
print(errors_os_2week)
