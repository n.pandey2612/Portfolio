# -*- coding: utf-8 -*-


from __future__ import print_function
import numpy as np

np.random.seed(1337)  # for reproducibility

import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras import backend as K

batch_sizes = [128]
nb_classes = 10
nb_epoch = 2

# input image dimensions
img_rows, img_cols = 28, 28
# number of convolutional filters to use
nb_filters = 32
# size of pooling area for max pooling
pool_size = (2, 2)
# convolution kernel size
kernel_size = (3, 3)

# the data, shuffled and split between train and test sets
(X_train, y_train), (X_test, y_test) = mnist.load_data()

if K.image_dim_ordering() == 'th':
    X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
    X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 1)
    X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

model = Sequential()

model.add(Convolution2D(nb_filters, kernel_size[0], kernel_size[1],
                        border_mode='valid',
                        input_shape=input_shape))
model.add(Activation('relu'))
model.add(Convolution2D(nb_filters, kernel_size[0], kernel_size[1]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=pool_size))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])


def divide_in_batches(X, y, batch_size):
    n = int(X.shape[0] / batch_size)
    X_batch = np.vsplit(X[0:n * batch_size], n)
    Y_batch = np.vsplit(y[0:n * batch_size], n)
    return X_batch, Y_batch


def train_batch_wise(X_train, Y_train, X_test, Y_test, batch_size, model, iteration):
    X_train_batch, Y_train_batch = divide_in_batches(X_train, Y_train, batch_size)
    loss_train_epoch = []
    accuracy_train_epoch = []
    loss_test_epoch = []
    accuracy_test_epoch = []
    for X, y in zip(X_train_batch, Y_train_batch):
        loss_train_iteration, accuracy_train_iteration = model.train_on_batch(X, y, accuracy=True)
        loss_train_epoch.append(loss_train_iteration)
        accuracy_train_epoch.append(accuracy_train_iteration)

        if (iteration % 100 == 0):
            loss_test_iteration, accuracy_test_iteration = model.test_on_batch(X_test, Y_test, accuracy=True)
            loss_test_epoch.append(loss_test_iteration)
            accuracy_test_epoch.append(accuracy_test_iteration)

        iteration = iteration + 1
        if(iteration==2000):
            break
        print('iteration:' + str(iteration))
        print('loss:' + str(loss_train_iteration))
        print('accuracy' + str(accuracy_train_iteration))

    return loss_train_epoch, accuracy_train_epoch, loss_test_epoch, accuracy_test_epoch, iteration

fig1 = plt.figure()
fig2 = plt.figure()
fig1.subplots_adjust(hspace=0.5)
fig2.subplots_adjust(hspace=0.5)
ax1 = fig1.add_subplot(2, 1, 1)
ax2 = fig1.add_subplot(2, 1, 2)
ax3 = fig2.add_subplot(2, 1, 1)
ax4 = fig2.add_subplot(2, 1, 2)
for batch_size in batch_sizes:
    iteration = 0
    loss_train = []
    accuracy_train = []
    loss_test = []
    accuracy_test = []
    while iteration < 2000:
        print('batch_size='+str(batch_size))
        loss_train_epoch, accuracy_train_epoch, loss_test_epoch, accuracy_test_epoch, iteration = train_batch_wise(
            X_train, Y_train, X_test, Y_test, batch_size, model, iteration)
        loss_train.extend(loss_train_epoch)
        accuracy_train.extend(accuracy_train_epoch)
        loss_test.extend(loss_test_epoch)
        accuracy_test.extend(accuracy_test_epoch)

    ax1.plot(loss_train,label=str(batch_size))
    ax2.plot(accuracy_train, label=str(batch_size))
    ax3.plot(loss_test, label=str(batch_size) )
    ax4.plot(accuracy_test, label=str(batch_size))

    ax1.set_xlabel('#iterations')
    ax1.set_ylabel('Train_loss')
    ax2.set_xlabel('#iterations')
    ax2.set_ylabel('Train_accuracy')
    ax3.set_xlabel('#iterations(per 100)')
    ax3.set_ylabel('Test_loss')
    ax4.set_xlabel('#iterations(per 100)')
    ax4.set_ylabel('Test_accuracy')


    ax1.grid()
    ax2.grid()
    ax3.grid()
    ax4.grid()

fig1.savefig('train'+str(batch_size))
fig2.savefig('test'+str(batch_size))













