
import matplotlib.pyplot as plt
import numpy as np
import cv2

from sklearn import svm, metrics
from scipy import ndimage
from skimage.transform import resize
from sklearn.preprocessing import normalize


# function to load images amd labels of the original MNIST dataset
from mnist import load_mnist


def part_a():

    # name of the image to load
    filename = 'red-fox.jpg'

    # load the image
    img1 = ndimage.imread(filename)
    # transfer the image into numpy ndarray 
    #img1 = np.ndarray(img1)
    # get image dimensions
    dims = np.shape(img1)
    # get image size
    s = np.size(img1)

    print ('Loaded: {}'.format(filename))
    print ('Image shape: {}'.format(dims))
    print ('Image size: {}'.format(s))

    # crop a random (h x w) patch from the image
    h = 227
    w = 227
    img1_cut = img1[0:h,0:w,:]

    # resize initial image to the size (h x w x c), c - number of channels
    img1_resize = resize(img1,(h,w))

    # convert the original image into the gray scale
    img1_gray = cv2.imread(filename,0)

    
    img1_dx = cv2.Sobel(img1_gray,cv2.CV_64F,1,0,ksize=3)
    

    # apply an arbitrary affine transformation (rotation, translation) to the gray scale image
    img1_rot = ndimage.interpolation.rotate(img1_gray,90)
    img1_transf = ndimage.interpolation.shift(img1_rot,90)

    # show all resuls (6 images in a single figure)
    fig = plt.figure()
    a=fig.add_subplot(2,3,1)
    imgplot = plt.imshow(img1)
    a.set_title('Original Image')
    
    a=fig.add_subplot(2,3,2)
    imgplot = plt.imshow(img1_cut)
    a.set_title('Cropped Image')
    
    a=fig.add_subplot(2,3,3)
    imgplot = plt.imshow(img1_resize)
    a.set_title('Resized Image')
    
    a=fig.add_subplot(2,3,4)
    imgplot = plt.imshow(img1_gray,cmap='gray')
    a.set_title('Gray Image')
    
    a=fig.add_subplot(2,3,5)
    imgplot = plt.imshow(img1_dx,cmap='gray')
    a.set_title('Sobel')
    
    a=fig.add_subplot(2,3,6)
    imgplot = plt.imshow(img1_transf,cmap='gray')
    a.set_title('Affine')
    
    fig.subplots_adjust(wspace=0.2, hspace=0.5)
    
    plt.savefig('result')
    plt.show()
    



def part_b():

    # load MNIST data set (only selected digits, i.e. 3,8,9 )
    digits = np.array([3,8,9])
    
    trainset, trainlabels = load_mnist("training",digits)
    testset, testlabels = load_mnist("testing",digits)

 	# reshape arrays with the labels into 1d arrays
    trainlabels = trainlabels.reshape(-1,1)
    testlabels = testlabels.reshape(-1,1)

    print ('trainset shape: ', trainset.shape)
    print ('testset shape: ', testset.shape)
    print ('trainlables shape: ', trainlabels.shape)
    #print (trainset.shape[0])
    # visualize three random sample from the dataset:
    fig = plt.figure()
    a=fig.add_subplot(1,3,1)
    imgplot = plt.imshow(trainset[50,:,:],cmap='gray')
    a.set_title('Sample1')
    
    a=fig.add_subplot(1,3,2)
    imgplot = plt.imshow(trainset[290,:,:],cmap='gray')
    a.set_title('Sample2')
    
    a=fig.add_subplot(1,3,3)
    imgplot = plt.imshow(trainset[390,:,:],cmap='gray')
    a.set_title('Sample3')
    
    fig.subplots_adjust(wspace=0.2, hspace=0.5)
    
    plt.savefig('random_sample')
    plt.show()
    
    

    # consider image pixels as features, for that reshape the images in 1d vectors
    trainset_vec = trainset.reshape(trainset.shape[0],trainset.shape[1]*trainset.shape[2])
    print ('new trainset shape: ', trainset_vec.shape)
    testset_vec = testset.reshape(testset.shape[0],testset.shape[1]*testset.shape[2]) 
    print ('new testset shape: ', testset_vec.shape)

    # normalize data to [-1, 1]-range
    trainset_vec = normalize(trainset_vec)
    testset_vec = normalize(testset_vec)

    # create a SVM classifier
    classifier = svm.SVC()

    # train classifier check (see the svm.SVC definition)
    classifier.fit(trainset_vec,trainlabels)

    # test classifier on the validation set (see the svm.SVC definition)
    testlabels_pred = classifier.predict(testset_vec)

    # classification accuracy on the validation set
    Acc = metrics.accuracy_score(testlabels,testlabels_pred)
    print ("Accuracy on the validation set: %0.2f" % (100*Acc))

    # confusion matrix ( HINT see metrics from sklearn)
    confmatrix = metrics.confusion_matrix(testlabels,testlabels_pred)
    print ('Confusion matrix:\n ', confmatrix)



def main():
  part_a()
  part_b()


if __name__ == '__main__':
    main()
