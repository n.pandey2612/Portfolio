% function []=preprocessing()
%E:\8th semester\Machine Learning\jaffeimages\1
identity = {'KA','KL','KM','KR','MK','NA','NM','TM','UY','YM'};
test_folder = 'E:\machine learning\course\Machine Learning\jaffeimages\6';
test_folder_name =  dir(fullfile(test_folder, '*.TIFF'));
[index1 index2] = size(test_folder_name);
size1=256;
mkdir(test_folder ,'cropped1');
mkdir(test_folder ,'cropped2');
mkdir(test_folder ,'cropped3');
neutral_folder = 'E:\machine learning\course\Machine Learning\jaffeimages\10\n';
neutral_folder_name = dir(fullfile(neutral_folder));


%neutral = imread(fullfile(test_folder,'n','KA.NE1.26.TIFF'));
%imshow(neutral);
for ni = 3:5
    neutral = imread(fullfile(neutral_folder,neutral_folder_name(ni).name));
    
    for i =1:length(test_folder_name)
    
    test_image = imread(fullfile(test_folder,test_folder_name(i).name));
    
    diff =  test_image - neutral;
    
    I = imcrop(diff,[0,size1/3,size1,size1*5/9]);
    I_o = imcrop(test_image,[0,size1/3,size1,size1*5/9]);
    S_I=size(I);
    
    eye_region = imcrop(I,[0,0,size1,72]);
    eye_region_o = imcrop(test_image,[0,0,size1,72]);
    S_eye =size(eye_region);
    
    x_size = size1;
    y_size = 72;

    [Leye_xstart,Leye_xend,Reye_xstart,Reye_xend,Index(i)] = x_coord_eye(eye_region,x_size,y_size);
     
    face_cropped = imcrop(I,[Leye_xstart,0,Reye_xend-Leye_xstart,S_I(1)]);
    face_cropped_o = imcrop(I_o,[Leye_xstart,0,Reye_xend-Leye_xstart,S_I(1)]);
    % imwrite(face_cropped,fullfile(test_folder,'cropped',test_folder_name(i).name));
    
    eye_cropped = imcrop(eye_region,[Leye_xstart,0,Reye_xend-Leye_xstart,y_size]);
    eye_cropped_o = imcrop(eye_region_o,[Leye_xstart,0,Reye_xend-Leye_xstart,y_size]);
    [y_s,y_e]= y_coord_eye(eye_cropped,Reye_xend-Leye_xstart,72);
    
    eye_cropped = imcrop(eye_cropped,[0,y_s,Reye_xend-Leye_xstart,y_e-y_s]);
    eye_cropped_o = imcrop(eye_cropped_o,[0,y_s,Reye_xend-Leye_xstart,y_e-y_s]);
    s=size(face_cropped);
    face_cropped_o = imcrop(face_cropped_o,[0,y_s,s(2),s(1)-y_s]);
    face_cropped = imcrop(face_cropped,[0,y_s,s(2),s(1)-y_s]);
    
    %imwrite(face_cropped,fullfile(test_folder,'cropped',test_folder_name(i).name));
    
    mouth_region = imcrop(face_cropped,[Leye_xend,y_e,Reye_xstart-Leye_xend,s(1)-y_s-y_e]);
    mouth_region_o = imcrop(face_cropped_o,[Leye_xend,y_e,Reye_xstart-Leye_xend,s(1)-y_s-y_e]);

    %[m_s,m_e]=mouth_coord(mouth_region);
    %q=s(2)-m_e;
    s=size(face_cropped);
    
    face_cropped = imcrop(face_cropped,[0,0,s(2),s(1)-20]);
    face_cropped_o = imcrop(face_cropped_o,[0,0,s(2),s(1)-20]);
%    figure; imshow(face_cropped_o);
%    face_cropped_o = histeq(face_cropped_o);
    imwrite(face_cropped_o,fullfile(test_folder,strcat('cropped',int2str(ni-2)),test_folder_name(i).name));
    %figure;imshow(mouth_region);
     
%    eye_cropped = imcrop(eye_region,[Leye_xstart,0,Reye_xend-Leye_xstart,72]);
%     
%     [y_s,y_e]= y_coord_eye(eye_cropped,Reye_xend-Leye_xstart,72);
%     
%     eye_cropped = imcrop(eye_cropped,[0,y_s,Reye_xend-Leye_xstart,y_e-y_s]);
% 
%    face_cropped = imcrop(face_cropped,[0,y_s,Reye_xend-Leye_xstart,size1*5/9-y_s]);
%      
%      figure;imshow(mouth_cropped);
%      figure;imshow(face_cropped);
    

end
end
% end