
num = 10;

[lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU] = getlbp(num);

index = 1:2048;
index = index';
m_2048 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,index);


load 'I_index_1.mat'
boosted_index = I_index;
unique_index = unique(boosted_index);
non_rep = nonrep(boosted_index);
m_nonrep_1 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,non_rep);
m_unique_1 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,unique_index);

load 'I_index_5.mat'
boosted_index = I_index;
unique_index = unique(boosted_index);
non_rep = nonrep(boosted_index);
m_nonrep_5 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,non_rep);
m_unique_5 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,unique_index);

load 'I_index_10.mat'
boosted_index = I_index;
unique_index = unique(boosted_index);
non_rep = nonrep(boosted_index);
m_nonrep_10 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,non_rep);
m_unique_10 = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,unique_index);

