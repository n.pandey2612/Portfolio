function [start,ending] = mouth_coord(mouth_region)
    mouth_region = mouth_region';
   figure;imshow(mouth_region);
   
    level = graythresh(mouth_region);
    BW = im2bw(mouth_region,level );
    s=size(mouth_region);
    
     DL = mean(BW);
    t=mean(DL);
    figure;
    plot(1:s(2),t*ones(1,s(2)));hold on;
    plot(DL);
    xlabel(['t=',num2str(t)]) ;
    start=45;
    ending=70;
end