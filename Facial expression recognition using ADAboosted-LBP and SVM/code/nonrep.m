%nonrepeating
function [nonrepeating] = nonrep(a)
[n, bin] = histc(a, unique(a));
multiple = find(n > 1);
index = find(ismember(bin, multiple));
nonrepeating = setdiff(a, unique(a(index)));