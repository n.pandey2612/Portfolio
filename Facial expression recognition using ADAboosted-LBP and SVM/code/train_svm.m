function [svmstruct] = train_svm(num)

addpath 'C:\Windows.old\Users\Vicky\Downloads\libsvm-3.20\libsvm-3.20\matlab';
%num = 5;
an=1;
di=1;
fe=1;
ha=1;
sa=1;
su=1;
for index = 1:num
    
folder = 'E:\machine learning\course\Machine Learning\jaffeimages';
test_folder = fullfile(folder,int2str(index),'cropped1');
test_folder_name =  dir(fullfile(test_folder, '*.TIFF'));


A = {'AN','DI','FE','HA','SA','SU'};

for i =1:length(test_folder_name)
    if(strfind(test_folder_name(i).name,char(A(1))))
        lbp_AN(an,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        an=an+1;
    end
    if(strfind(test_folder_name(i).name,char(A(2))))
        lbp_DI(di,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        di=di+1;
    end
    if(strfind(test_folder_name(i).name,char(A(3))))
        lbp_FE(fe,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        fe=fe+1;
    end
    if(strfind(test_folder_name(i).name,char(A(4))))
        lbp_HA(ha,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        ha=ha+1;
    end
    if(strfind(test_folder_name(i).name,char(A(5))))
        lbp_SA(sa,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        sa=sa+1;
    end
    if(strfind(test_folder_name(i).name,char(A(6))))
        lbp_SU(su,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        su=su+1;
    end
    
end
end

lbp_features=[lbp_AN;lbp_DI;lbp_FE;lbp_HA;lbp_SA;lbp_SU];
% lbp_features = lbp_features (:,I_unique);
[s_an,s] = size(lbp_AN);
[s_di,s] = size(lbp_DI);
[s_fe,s] = size(lbp_FE);
[s_ha,s] = size(lbp_HA);
[s_sa,s] = size(lbp_SA);
[s_su,s] = size(lbp_SU);

g = [ones(1,s_an),2*ones(1,s_di),3*ones(1,s_fe),4*ones(1,s_ha),5*ones(1,s_sa),6*ones(1,s_su)];

g_an = [ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_di = [-1*ones(1,s_an),ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_fe = [-1*ones(1,s_an),-1*ones(1,s_di),ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_ha = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_sa = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),ones(1,s_sa),-1*ones(1,s_su)];
g_su = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),ones(1,s_su)];

svmstruct = svmtrain(g',lbp_features,'-s 1 -t 1 -d 4')

% svm_an = svmtrain(lbp_features,g_an)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);
% svm_di = svmtrain(lbp_features,g_di)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);
% svm_fe = svmtrain(lbp_features,g_fe)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);
% svm_ha = svmtrain(lbp_features,g_ha)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);
% svm_sa = svmtrain(lbp_features,g_sa)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);
% svm_su = svmtrain(lbp_features,g_su)%,'Kernel_Function', 'polynomial', 'Polyorder', 3);



%[r_an,r_di,r_fe,r_ha,r_sa,r_su] = testing(svm_an,svm_di,svm_fe,svm_ha,svm_sa,svm_su,num)
end