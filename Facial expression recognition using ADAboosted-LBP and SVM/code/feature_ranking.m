function [index] = feature_ranking(num,itt)
%num = 10;
an=1;
di=1;
fe=1;
ha=1;
sa=1;
su=1;
for index = 1:num
    
folder = 'E:\machine learning\course\Machine Learning\jaffeimages';
test_folder = fullfile(folder,int2str(index),'cropped1')
test_folder_name =  dir(fullfile(test_folder, '*.TIFF'));


A = {'AN','DI','FE','HA','SA','SU'};

for i =1:length(test_folder_name)
    if(strfind(test_folder_name(i).name,char(A(1))))
        lbp_AN(an,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        an=an+1;
    end
    if(strfind(test_folder_name(i).name,char(A(2))))
        lbp_DI(di,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        di=di+1;
    end
    if(strfind(test_folder_name(i).name,char(A(3))))
        lbp_FE(fe,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        fe=fe+1;
    end
    if(strfind(test_folder_name(i).name,char(A(4))))
        lbp_HA(ha,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        ha=ha+1;
    end
    if(strfind(test_folder_name(i).name,char(A(5))))
        lbp_SA(sa,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        sa=sa+1;
    end
    if(strfind(test_folder_name(i).name,char(A(6))))
        lbp_SU(su,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        su=su+1;
    end
    
end
end

lbp_features=[lbp_AN;lbp_DI;lbp_FE;lbp_HA;lbp_SA;lbp_SU];

[s_an,s] = size(lbp_AN);
[s_di,s] = size(lbp_DI);
[s_fe,s] = size(lbp_FE);
[s_ha,s] = size(lbp_HA);
[s_sa,s] = size(lbp_SA);
[s_su,s] = size(lbp_SU);

lbp_AN_N = [lbp_DI;lbp_FE;lbp_HA;lbp_SA;lbp_SU];
[s_AN_N,s] = size(lbp_AN_N);
lbp_features_AN = [lbp_AN; lbp_AN_N(randperm(s_AN_N,s_an),:)];

lbp_DI_N = [lbp_AN;lbp_FE;lbp_HA;lbp_SA;lbp_SU];
[s_DI_N,s] = size(lbp_DI_N);
lbp_features_DI = [lbp_DI; lbp_DI_N(randperm(s_DI_N,s_di),:)];

lbp_FE_N = [lbp_DI;lbp_AN;lbp_HA;lbp_SA;lbp_SU];
[s_FE_N,s] = size(lbp_FE_N);
lbp_features_FE = [lbp_FE; lbp_FE_N(randperm(s_FE_N,s_fe),:)];

lbp_HA_N = [lbp_DI;lbp_FE;lbp_AN;lbp_SA;lbp_SU];
[s_HA_N,s] = size(lbp_HA_N);
lbp_features_HA = [lbp_HA; lbp_HA_N(randperm(s_HA_N,s_ha),:)];

lbp_SA_N = [lbp_DI;lbp_FE;lbp_HA;lbp_AN;lbp_SU];
[s_SA_N,s] = size(lbp_SA_N);
lbp_features_SA = [lbp_SA; lbp_SA_N(randperm(s_SA_N,s_sa),:)];

lbp_SU_N = [lbp_DI;lbp_FE;lbp_HA;lbp_SA;lbp_AN];
[s_SU_N,s] = size(lbp_SU_N);
lbp_features_SU = [lbp_SU; lbp_SU_N(randperm(s_SU_N,s_su),:)];


g_an = [ones(1,s_an),-1*ones(1,s_an)];
g_di = [ones(1,s_di),-1*ones(1,s_di)];
g_fe = [ones(1,s_fe),-1*ones(1,s_fe)];
g_ha = [ones(1,s_ha),-1*ones(1,s_ha)];
g_sa = [ones(1,s_sa),-1*ones(1,s_sa)];
g_su = [ones(1,s_su),-1*ones(1,s_su)];


[index_di] = boosted_lbp(lbp_features_DI,g_di,itt)
[index_an] = boosted_lbp(lbp_features_AN,g_an,itt)
[index_fe] = boosted_lbp(lbp_features_FE,g_fe,itt)
[index_ha] = boosted_lbp(lbp_features_HA,g_ha,itt)
[index_sa] = boosted_lbp(lbp_features_SA,g_sa,itt)
[index_su] = boosted_lbp(lbp_features_SU,g_su,itt)

index = [index_di;index_an;index_fe;index_ha;index_sa;index_su]
I_unique = unique(index);

lbp_boosted_features = lbp_features(I_unique);
% 
% 
% [classestimate,model_di] = adaboost('train',lbp_features_DI,g_di)
% [classestimate,model_an] = adaboost('train',lbp_features_AN,g_an)
% [classestimate,model_fe] = adaboost('train',lbp_features_FE,g_fe)
% [classestimate,model_ha] = adaboost('train',lbp_features_HA,g_ha)
% [classestimate,model_sa] = adaboost('train',lbp_features_SA,g_sa)
% [classestimate,model_su] = adaboost('train',lbp_features_SU,g_su)


% g_an = [ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
% g_di = [-1*ones(1,s_an),ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
% g_fe = [-1*ones(1,s_an),-1*ones(1,s_di),ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
% g_ha = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
% g_sa = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),ones(1,s_sa),-1*ones(1,s_su)];
% g_su = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),ones(1,s_su)];
end