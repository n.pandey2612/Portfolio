function [lbp] = lbp123(pathname)
    I = imread(pathname);
     I= imresize(I,[64 64]);
%     I=histeq(I);
%       I1=im2double(I1(:,:));
%      m = mean2(I1);
%      s = std2(I1);
%     
%       I = (I1 - m)/s;
%     
    
    folder = 'E:\machine learning\course\Machine Learning';
    s=size(I);
    %I = imread('autumn.tif');
%     rectangle = [int32([0 0 s(2)/2 s(1)*2/5]);                    %left eye
%                     int32([s(2)/2 0 s(2)/2+1 s(1)*2/5]);             %right eye
%                     int32([s(2)/4 s(1)*2/5 s(2)/2+1 s(1)*2/5]);         %nose 
%                     int32([s(2)*1/8 s(1)*4/5 s(2)*3/4 s(1)*1/5]);]       %mouth 
   
    rectangle = [int32([0 0 s(2)*3/16 s(1)*3/8]);                    %left eye
                 int32([s(2)*3/16 0 s(2)*3/16 s(1)*3/8]);
                    int32([s(2)*5/8 0 s(2)*3/16+1 s(1)*3/8]);             %right eye
                    int32([s(2)*13/16 0 s(2)*3/16+1 s(1)*3/8]);
                    int32([s(2)/4 s(1)*3/8 s(2)/4+1 s(1)*3/8]);         %nose 
                    int32([s(2)/2 s(1)*3/8 s(2)/4+1 s(1)*3/8]); 
                    int32([s(2)*1/8 s(1)*3/4 s(2)*3/8 s(1)*1/4]);
                    int32([s(2)*1/2 s(1)*3/4 s(2)*3/8 s(1)*1/4]);];       %mouth
lbp=[];
for i=1:8
    
    I1=imcrop(I,rectangle(i,:));
    imwrite(I1,fullfile(folder,'temp','123.jpg'));
    lpb_ = r_lbp_score(fullfile(folder,'temp','123.jpg'));
    lbp = [lbp,lpb_];
end
    
end