function [start,ending] = y_coord_eye(eye_cropped1,x_size,y_size)

    eye_cropped = eye_cropped1';
    level = graythresh(eye_cropped);
    BW = im2bw(eye_cropped,level );
    
    ending = 0;
  %  imshow(BW);
    DL = mean(BW);
    t=mean(DL);
    %figure;plot(DL);
    %xlabel(['t=',num2str(t)]) 
    s=0;e=0;
    for i=1:y_size
        if((DL(i)>t)&&(s==0))
            start = i;
            s=1;
            break;
        end
    end
    for i=start+30:y_size
        if((DL(i)<2*t/3)&&(s==1)&&(e==0))
            ending = i;
            e=1;
        end
    end
    
    

end