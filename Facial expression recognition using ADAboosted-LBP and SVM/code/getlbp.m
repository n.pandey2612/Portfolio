function [lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU] = getlbp(num)
an=1;
di=1;
fe=1;
ha=1;
sa=1;
su=1;
for index = 1:num
    
folder = 'E:\machine learning\course\Machine Learning\jaffeimages';
test_folder = fullfile(folder,int2str(index),'cropped1');
test_folder_name =  dir(fullfile(test_folder, '*.TIFF'));


A = {'AN','DI','FE','HA','SA','SU'};

for i =1:length(test_folder_name)
    if(strfind(test_folder_name(i).name,char(A(1))))
        lbp_AN(an,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        an=an+1;
    end
    if(strfind(test_folder_name(i).name,char(A(2))))
        lbp_DI(di,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        di=di+1;
    end
    if(strfind(test_folder_name(i).name,char(A(3))))
        lbp_FE(fe,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        fe=fe+1;
    end
    if(strfind(test_folder_name(i).name,char(A(4))))
        lbp_HA(ha,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        ha=ha+1;
    end
    if(strfind(test_folder_name(i).name,char(A(5))))
        lbp_SA(sa,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        sa=sa+1;
    end
    if(strfind(test_folder_name(i).name,char(A(6))))
        lbp_SU(su,:) = lbp123(fullfile(test_folder,test_folder_name(i).name));
        su=su+1;
    end
    
end
end
end