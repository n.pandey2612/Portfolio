clear;
clc;
folder = 'E:\machine learning\course\Machine Learning\jaffeimages\1';
test_folder = fullfile(folder,'cropped1');
test_folder_name =  dir(fullfile(test_folder, '*.TIFF'));
mkdir(folder ,'final');

for i =3:length(test_folder_name)

    I = imread(fullfile(test_folder,test_folder_name(i).name));
    s=size(I);
    %I = imread('autumn.tif');
%     rectangle = [int32([0 0 s(2)/4 s(1)*2/5]);                    %left eye
%                  int32([s(2)/4 0 s(2)/4 s(1)*2/5]);
%                     int32([s(2)/2 0 s(2)/4+1 s(1)*2/5]);             %right eye
%                     int32([s(2)*3/4 0 s(2)/4+1 s(1)*2/5]);
%                     int32([s(2)/4 s(1)*2/5 s(2)/4+1 s(1)*2/5]);         %nose 
%                     int32([s(2)/2 s(1)*2/5 s(2)/4+1 s(1)*2/5]); 
%                     int32([s(2)*1/8 s(1)*4/5 s(2)*3/8 s(1)*1/5]);
%                     int32([s(2)*1/2 s(1)*4/5 s(2)*3/8 s(1)*1/5]);]       %mouth 

    rectangle = [int32([0 0 s(2)*3/16 s(1)*3/8]);                    %left eye
                 int32([s(2)*3/16 0 s(2)*3/16 s(1)*3/8]);
                    int32([s(2)*5/8 0 s(2)*3/16+1 s(1)*3/8]);             %right eye
                    int32([s(2)*13/16 0 s(2)*3/16+1 s(1)*3/8]);
                    int32([s(2)/4 s(1)*3/8 s(2)/4+1 s(1)*3/8]);         %nose 
                    int32([s(2)/2 s(1)*3/8 s(2)/4+1 s(1)*3/8]); 
                    int32([s(2)*1/8 s(1)*3/4 s(2)*3/8 s(1)*1/4]);
                    int32([s(2)*1/2 s(1)*3/4 s(2)*3/8 s(1)*1/4]);]       %mouth 


   
    %rectangle = int32([s(2)/2 0 s(2)/2+1 s(1)*2/5]);             %right eye
    %rectangle = int32([s(2)/4 s(1)*2/5 s(2)/2+1 s(1)*2/5]);      %nose eye
    %rectangle = int32([s(2)*1/8 s(1)*4/5 s(2)*3/4 s(1)*1/5]);      %mouth eye
    J=I;
    for ii = 1:8
    shapeInserter = vision.ShapeInserter('Shape','Rectangles','BorderColor','Custom','CustomBorderColor',255);
    J = step(shapeInserter, J, rectangle(ii,:));
    imshow(J);shg;
    imwrite(J,fullfile(folder,'final',test_folder_name(i).name));
    end

end