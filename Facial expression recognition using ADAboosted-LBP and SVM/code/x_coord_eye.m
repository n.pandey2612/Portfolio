function [Leye_xstart,Leye_xend,Reye_xstart,Reye_xend,index] = x_coord_eye(eye_region,x_size,y_size)

    level = graythresh(eye_region);
    BW = im2bw(eye_region,level );
  %  imshow(BW);
    
    DL = mean(BW);
    t = mean(DL);
%     figure;
%     plot(1:x_size,t*ones(1,x_size));hold on;
%     plot(DL);xlabel(['t=',num2str(t)])
        index=0;
    for ii = x_size/2 -10 : x_size/2 +10
        if(DL(ii:ii+5)==0)
        index = ii+5;
        break;
        else
          index =x_size/2;
        end
    end
    
    DL_left = DL(1:index);
    DL_right = DL(index:x_size);
    
    rs=0;
    re=0;
    ls=0;
    le=0;
    %figure;plot(DL);
    eye_gap = 30;
    for ii = 10:index-1
    
        if((DL_left(index-ii)>t*3/4)&&(le==0))
            Leye_xend = index  - ii;
            le=1;
        end
    end  
    for ii = index-Leye_xend + eye_gap: index-1
        if((le==1) && (DL_left(index  - ii) < t) && (ls==0))
        %    if(DL_left(index  - ii -5 :index  - ii )    ==0)
                Leye_xstart = index - ii;
                ls=1;
         %   end
        end
    end
    
    for ii = index+10 : x_size
    
        if( (DL_right(ii-index)>t*3/4) && (rs==0))
            Reye_xstart = ii;
            rs=1;
        end
    end
    for ii = Reye_xstart+eye_gap : x_size
        if((rs==1) && (DL_right(ii-index)<=t) && (re==0))
            %if((DL_right(ii-index:ii-index+5)==0))
             Reye_xend = ii;
             re=1;
            %end
        end
    end
%     
%     
%     for ii = 1 : x_size-index-1
%         
%         if( (DL_right(ii)~=0) && (rs==0))
%             Reye_xstart = x_size/2+ii;
%             rs=1;
%         end
%         
%          if( (DL_left(index  - ii)~=0) && (le==0))
%             Leye_xend = index  - ii;
%             le=1;
%          end
%          
%          if ((rs==1) && (DL_right(ii)==0) && (re==0))
%             if((DL_right(ii:ii+5)==0))
%              Reye_xend = x_size/2+ii;
%              re=1;
%             end
%          end
%          
%          if ((le==1) && (DL_left(index  - ii)==0) && (ls==0))
%             if((DL_left(index  - ii-1)==0)&&(DL_left(index  - ii-2)==0))
%              Leye_xstart = index - ii;
%              ls=1;
%             end
%          end
%     end
end