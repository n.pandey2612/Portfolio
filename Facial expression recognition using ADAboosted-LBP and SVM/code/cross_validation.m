function [m] = cross_validation(lbp_AN,lbp_DI,lbp_FE,lbp_HA,lbp_SA,lbp_SU,index)
addpath 'E:\machine learning\course\codes\libsvm-3.20\libsvm-3.20\matlab';


lbp_features=[lbp_AN;lbp_DI;lbp_FE;lbp_HA;lbp_SA;lbp_SU]; 
lbp_features = lbp_features(:,index);

[s_an,s] = size(lbp_AN);
[s_di,s] = size(lbp_DI);
[s_fe,s] = size(lbp_FE);
[s_ha,s] = size(lbp_HA);
[s_sa,s] = size(lbp_SA);
[s_su,s] = size(lbp_SU);

g = [ones(1,s_an),2*ones(1,s_di),3*ones(1,s_fe),4*ones(1,s_ha),5*ones(1,s_sa),6*ones(1,s_su)];         
g_an = [ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_di = [-1*ones(1,s_an),ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_fe = [-1*ones(1,s_an),-1*ones(1,s_di),ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_ha = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),ones(1,s_ha),-1*ones(1,s_sa),-1*ones(1,s_su)];
g_sa = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),ones(1,s_sa),-1*ones(1,s_su)];
g_su = [-1*ones(1,s_an),-1*ones(1,s_di),-1*ones(1,s_fe),-1*ones(1,s_ha),-1*ones(1,s_sa),ones(1,s_su)];

str = '-s 0 -t 1 -d 3 -v 10';

svm_an = svmtrain(g_an',lbp_features,str);
svm_di = svmtrain(g_di',lbp_features,str);
svm_fe = svmtrain(g_fe',lbp_features,str);
svm_ha = svmtrain(g_ha',lbp_features,str);
svm_sa = svmtrain(g_sa',lbp_features,str);
svm_su = svmtrain(g_su',lbp_features,str);

m = mean([svm_an,svm_di,svm_fe,svm_ha,svm_sa,svm_su])

end