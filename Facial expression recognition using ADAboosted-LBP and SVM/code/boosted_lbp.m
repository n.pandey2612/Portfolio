function [index] = boosted_lbp(lbp_f,dataclass,itt)

num_of_classifiers = itt;

%dataclass(1:15)=-1; dataclass(16:30)=1;
D=ones(length(dataclass),1)/length(dataclass);
err=[];
tic;
j=1;
index =zeros(20,1);
for j = 1 : 20
    for i = 1 : 2048
        if(ismember(i,index)==0)
        [classestimate,model]=adaboost('train',lbp_f(:,i),dataclass,num_of_classifiers);
        err(i)=0;
        for ii = 1:length(dataclass)
            err(i) = err(i) + abs(D(ii)*(classestimate(ii)-dataclass(ii)));
            epow(ii) = 1 -  abs(classestimate(ii) - dataclass(ii))/2; 
            
        end
        else
            err(i)=1000;
            epow(ii)=0;
        end
    end
    [m(j),index(j)]=min(err);
    alpha = err(index(j))/(1-err(index(j)));

    for ii = 1 : length(dataclass)
        D(ii) = D(ii)*(alpha^epow(ii));
    end
    j
    
end
toc;
end