#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 14:11:45 2018

@author: npandey
"""

#import torch
from torch.utils.data.dataset import Dataset
from PIL import Image
import numpy as np
import h5py
import glob
import os


def makeDataset(mode, path2raw, path2sparse, path2dense):
    dataset = []
    #loading raw dataset
    raw = []
    for file in sorted(glob.glob(os.getcwd() + path2raw + '/*.tif')):
        im = Image.open(file)
        im = np.array(im)
        im = (im - np.mean(im))/np.std(im)
        im = np.expand_dims(im, axis = 0)
        raw.append(im)
     
    #loading dense labels
    dense_labels = []
    for file in sorted(glob.glob(os.getcwd() + path2dense + '/*.png')):
        im = Image.open(file)
        im = np.array(im, dtype = np.uint8)
        im1 = im.copy()
        im[im1 == 0] = 1
        im[im1 != 0] = 0
        
        dense_labels.append(im)
        
    #loading sparse labels
    sparse_labels = []
    for file in sorted(glob.glob(os.getcwd() + path2sparse + '/*.h5')):
        f = h5py.File(file, 'r')
        sparse_labels.append(np.squeeze(np.array(f['exported_data']), axis = 2))
        
    #forming dataset
    for i in range(len(raw)):
        dataset.append((raw[i], sparse_labels[i], dense_labels[i]))
    
    ind_s = 13
    #assert mode in ['train', 'val', 'test']
    if mode=='train':
        return dataset[:ind_s]
    elif mode=='val':
        return dataset[ind_s:]
    else:
        return dataset
            
  
class IlastikData(Dataset):
    def __init__(self, mode, path2raw, path2sparse, path2dense, co_tf = None):
        self.mode = mode
        self.path2raw = path2raw
        self.path2sparse = path2sparse
        self.path2dense = path2dense
        self.items = makeDataset(mode, path2raw, path2sparse, path2dense)
        self.co_tf = co_tf
        #self.post_tf = post_tf
        
    def __len__(self):
        return len(self.items)

    def __getitem__(self, idx):
        raw, sparse, dense = self.items[idx]

        if self.co_tf is not None:
            raw, sparse, dense = self.co_tf(raw, sparse, dense)
            
        return (raw, sparse, dense)
            
    