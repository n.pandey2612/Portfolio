#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 16:20:51 2018

@author: neha
"""

import torch
import torch.nn as nn

class crfLoss(nn.Module):
    def __init__(self):
        super(crfLoss, self).__init__()
        
    def forward(self, S_0, S_1, G_0, G_1):
        crf_loss = torch.sum(torch.mul(G_0, S_1)) + torch.sum(torch.mul(G_1, S_0))
        return crf_loss
        
    def backward(self, S_0, S_1, G_0, G_1):
        grad_0 = -torch.sum(2*G_0)
        grad_1 = -torch.sum(2*G_1)
        return grad_0, grad_1, None, None
        
        
        
