#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 14:37:54 2018

@author: npandey
"""

import torch
#import torch.optim as optim
import torch.nn as nn
import numpy as np
from load_models import load_model
from dunet2D import DUNet2D
import os
from torch.autograd import Variable
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
#import argparse
#from torchvision import transforms
import pickle
from IlastikData import IlastikData
from inferno.io.transform import Compose
from inferno.io.transform.generic import AsTorchBatch
from inferno.io.transform.image import RandomRotate, ElasticTransform, RandomFlip
from pydensecrf.utils import create_pairwise_bilateral
from crfLoss import crfLoss

#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
batch_size = 1
num_epochs = 300
lr = 1e-2
use_gpu = True

path2raw = r'/datasets/drosophila/stack1/raw'
path2dense = r'/datasets/drosophila/stack1/mitochondria'
path2sparse = r'/datasets/drosophila/stack1/sparse_labels'


#joint transforms for data augmentation
co_tf = Compose(ElasticTransform(alpha=1000, sigma=30),
                RandomRotate(),
                RandomFlip(),
                AsTorchBatch(2)                
                )

#preparing training and validation data
train_data = IlastikData('train', path2raw, path2sparse, path2dense, co_tf)
train_loader = DataLoader(train_data, batch_size = batch_size, shuffle = True)

val_data = IlastikData('val', path2raw, path2sparse, path2dense, co_tf)
val_loader = DataLoader(val_data, batch_size = batch_size, shuffle = False)
criterion_crf = crfLoss()

def run_epoch(model, data_loader, criterion, mode):
    fin_preds = []
    acc_epoch = []
    loss_epoch = []
    if mode == 'Train':
        model.train()
    else:
        model.eval()
    for i, (images, labels , dense) in enumerate(data_loader):
        images = Variable(images, requires_grad = False)
        dense = Variable(dense, requires_grad = False)
        if use_gpu == True:
            images = images.cuda().float()
            labels = labels.cuda()
            dense = dense.cuda()
        if mode == 'Train':
            optimizer.zero_grad()
        output = model(images)   
        #masking the unlabeled values 
        mask = labels.gt(0)
        output_masked = torch.masked_select(output, mask)
        labels_masked = torch.masked_select(labels, mask)
        labels_masked -= 1
        
        tot = list(labels_masked.size())[0]
        nb_1 = labels_masked.sum().item()
        nb_0 = tot - nb_1
        weight = labels_masked.clone()
        weight[labels_masked == 0] = tot/nb_0
        weight[labels_masked == 1] = tot/nb_1
        criterion.weight = weight.float()
        loss_bce = criterion(output_masked, labels_masked.float())
        
        #Calculate fast crf loss
        #out_np = output.data.cpu().numpy()
        
        y_0 = 1 - output
        S_0 = y_0.view(y_0.size(0), -1)
        S_1 = output.view(output.size(0), -1)    
        G_0 = fastGaussian(S_0.data.cpu().numpy())
        G_1 = fastGaussian(S_1.data.cpu().numpy())
        
        G_0 = Variable(torch.from_numpy(G_0), requires_grad = False).cuda()
        G_1 = Variable(torch.from_numpy(G_1), requires_grad = False).cuda()
        loss_crf = criterion_crf(S_0, S_1, G_0, G_1)
        
        """
        out_np = output.data.cpu().numpy()
        y_0 = 1 - out_np
        S_0 = y_0.flatten()
        S_1 = out_np.flatten()
        G_0 = fastGaussian(S_0)
        G_1 = fastGaussian(S_1)
        S_0 = Variable(torch.from_numpy(S_0), requires_grad = True).cuda()
        S_1 = Variable(torch.from_numpy(S_1), requires_grad = True).cuda()
        G_0 = Variable(torch.from_numpy(G_0), requires_grad = False).cuda()
        G_1 = Variable(torch.from_numpy(G_1), requires_grad = False).cuda()
        loss_crf = criterion_crf(S_0, S_1, G_0, G_1)
        
        """
        
        #Total loss
        loss = loss_crf + loss_bce
        loss_epoch.append(loss)
        
        #get on batch predictions
        predicted = output.clone()
        predicted[output >= 0.5] = 1
        predicted[output < 0.5] = 0
        acc = ((predicted == dense.float().data).sum()).float()/(1024*1024)
        acc_epoch.append(acc)
        
        fin_preds.append(predicted)
        
        if mode == 'Train':
            loss.backward()
            optimizer.step()
    
    
    loss_epoch_mean = np.mean(np.array([x.item() for x in loss_epoch]))    
    acc_epoch_mean = np.mean(np.array([x.item() for x in acc_epoch]))
    
    return loss_epoch_mean, acc_epoch_mean, fin_preds
    
    
        

#training
path2model = 'finetune_mito_pcc.torch'
if os.path.isfile(path2model) == False:
    model = DUNet2D(1, 1)
    model = load_model(model, 'best_model_dunet2D.torch', use_gpu = use_gpu)
    #freezing the encoder of DUNet2D
    if use_gpu == True:
        for param in model.module.encoders.parameters():
            param.requires_grad = False
        for param in model.module.decoders.parameters():
            param.requires_grad = True
        optimizer = torch.optim.Adam(model.module.decoders.parameters(), lr = lr)   
    
    else:
        for param in model.encoders.parameters():
            param.requires_grad = False
        for param in model.decoders.parameters():
                param.requires_grad = True
        optimizer = torch.optim.Adam(model.decoders.parameters(), lr = lr)
    print("Start training")
    criterion = nn.BCELoss()
    loss_all_train = []
    acc_all_train = []
    loss_all_val = []
    acc_all_val = []
    for epoch in range(num_epochs):
        acc_epoch_train = []
        loss_epoch_train = []
        print('\n\nEpoch {}'.format(epoch))
    
        #training mode
        loss_epoch_mean_train, acc_epoch_mean_train, _ = run_epoch(model, train_loader, criterion, 'Train')
        loss_all_train.append(loss_epoch_mean_train)
        acc_all_train.append(acc_epoch_mean_train)
        print('Mean train loss over epoch = {0:.2f}'.format(loss_epoch_mean_train))    
        print('Mean train acc over epoch = {0:.2f}%'.format(acc_epoch_mean_train*100))
        print("   ")
        
        #validation mode
        with torch.no_grad():
            loss_epoch_mean_val, acc_epoch_mean_val, _ = run_epoch(model, val_loader, criterion, 'Validation')
        loss_all_val.append(loss_epoch_mean_val)
        acc_all_val.append(acc_epoch_mean_val)
        print('Mean validation loss over epoch = {0:.2f}'.format(loss_epoch_mean_val))    
        print('Mean validation acc over epoch = {0:.2f}%'.format(acc_epoch_mean_val*100))

        
    #Saving model after training
    torch.save(model, path2model)
    #Saving loss, accuracy and label predictions as pkl files
    if os.path.isfile('loss_train.pkl')==False & os.path.isfile('acc_train.pkl')==False:
        with open('loss_train.pkl', 'wb') as f:
            pickle.dump(loss_all_train, f)
            
        with open('acc_train.pkl', 'wb') as f:
            pickle.dump(acc_all_train, f)
            
      
            
    
    if os.path.isfile('loss_val.pkl')==False & os.path.isfile('acc_val.pkl')==False:
        with open('loss_val.pkl', 'wb') as f:
            pickle.dump(loss_all_val, f)
            
        with open('acc_val.pkl', 'wb') as f:
            pickle.dump(acc_all_val, f)
            
            
        
    with open('loss_train.pkl', 'rb') as f:
        loss_all_train = pickle.load(f)
    with open('acc_train.pkl', 'rb') as f:
        acc_all_train = pickle.load(f)
    with open('loss_val.pkl', 'rb') as f:
        loss_all_val = pickle.load(f)
    with open('acc_val.pkl', 'rb') as f:
        acc_all_val = pickle.load(f)   
    #Plotting results
    fig1 = plt.figure(figsize = (10, 5))
    #fig1.subplots_adjust(hspace = 0.7)
    ax11 = fig1.add_subplot(1, 2, 1)
    plt.plot(loss_all_train, 'r', label = 'Train') 
    plt.plot(loss_all_val, 'b', label = 'Test')
    plt.xlabel('Epochs')
    plt.ylabel('Cross Entropy Loss')
    plt.grid()
    plt.legend()
    ax12 = fig1.add_subplot(1, 2, 2)
    plt.plot(acc_all_train, 'r', label = 'Train') 
    plt.plot(acc_all_val, 'b', label = 'Test')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.grid()
    plt.legend()
    fig1.savefig('Results.png')


    #plotting predictions
    test_data = IlastikData('test', path2raw, path2sparse, path2dense)
    test_loader = DataLoader(test_data, batch_size = batch_size, shuffle = False)
    with torch.no_grad():
        loss_mean_overall, acc_mean_overall, test_preds = run_epoch(model, test_loader, criterion, 'Test')
    
    print(' ')
    print('Mean loss overall= {0:.2f}'.format(loss_mean_overall))    
    print('Mean acc overall = {0:.2f}%'.format(acc_mean_overall*100))
    
    for i in range(len(test_preds)):
        fig2 = plt.figure(figsize = (10, 5))
        #fig1.subplots_adjust(hspace = 0.7)
        ax21 = fig2.add_subplot(1, 2, 1)
        plt.imshow(test_data.__getitem__(i)[2], cmap = 'binary')
        ax21.set_title('Ground Truth')
        ax22 = fig2.add_subplot(1, 2, 2)
        plt.imshow(np.squeeze(np.squeeze(test_preds[i].data.cpu().numpy())), cmap = 'binary')
        ax22.set_title('Sparse')
        fig2.savefig('train_'+ str(i)+'.png')
        plt.close('all')
    









