import torch
#import torch.optim as optim
import torch.nn as nn
#import numpy as np
#import h5py
from collections import OrderedDict
"""
from dunet2D.dunet2D import DUNet2D
from dunet3D.dunet3D import DUNet3D
"""
#from dunet2D import DUNet2D
#from dunet3D import DUNet3D

def load_model(model, path_to_saved_model, use_gpu = False):
    saved_checkpoint = torch.load(
        path_to_saved_model,
        map_location=lambda storage, loc: storage
    )
    # Model was trained using nn.DataParallel so we have to crop the 'module.' prefix
    state_dict = OrderedDict()
    for k, v in saved_checkpoint['state_dict'].items():
        name = k[7:]  # remove 'module'.
        state_dict[name] = v

    if use_gpu:
        model = nn.DataParallel(model, device_ids=[0])
        model.load_state_dict(saved_checkpoint['state_dict'])
        model.cuda()
    else:
        model.load_state_dict(state_dict)

    return model

"""
def main():
    model = DUNet3D(1, 1)
    #model = load_model(model, 'dunet3D/dunet3D_22-01-18.torch')
    #torch.save(model.state_dict(), 'dunet3D/dunet3D_weights.pytorch')
    #torch.load('dunet3D/dunet3D_weights.pytorch')
    #torch.load('best_model_dunet3D.torch')
    model = load_model(model, 'best_model_dunet3D.torch')
    


if __name__ == '__main__':
    main()
    
"""