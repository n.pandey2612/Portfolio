import torch
import numpy as np
import torch.nn as nn
from inferno.extensions.layers.convolutional import ConvActivation, Conv3D, ConvELU3D
from functools import reduce


class AtrousBlock3D(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, num_branches=3):
        super(AtrousBlock3D, self).__init__()
        self.branches = nn.ModuleList([Conv3D(in_channels=in_channels,
                                              out_channels=out_channels,
                                              kernel_size=kernel_size,
                                              dilation=(2 ** branch_num))
                                       for branch_num in range(num_branches)])
        self.activation = nn.ELU()

    def forward(self, input):
        # Forward through all branches
        preactivation = reduce(lambda x, y: x + y,
                               [branch(input) for branch in self.branches])
        output = self.activation(preactivation)
        return output


class AnisotropicUpsample(nn.Module):
    def __init__(self, scale_factor):
        super(AnisotropicUpsample, self).__init__()
        self.upsampler = nn.UpsamplingNearest2d(scale_factor=scale_factor)

    def forward(self, input):
        # input is 3D of shape NCDHW
        N, C, D, H, W = input.size()
        # Fold C and D axes in one
        folded = input.view(N, C * D, H, W)
        # Upsample
        upsampled = self.upsampler(folded)
        # Unfold out the C and D axes
        unfolded = upsampled.view(N, C, D,
                                  self.upsampler.scale_factor * H,
                                  self.upsampler.scale_factor * W)
        # Done
        return unfolded


class AnisotropicPool(nn.MaxPool3d):
    def __init__(self, downscale_factor):
        ds = downscale_factor
        super(AnisotropicPool, self).__init__(kernel_size=(1, ds + 1, ds + 1),
                                              stride=(1, ds, ds),
                                              padding=(0, 1, 1))

class Xcoder(nn.Module):
    def __init__(self, previous_in_channels, out_channels, kernel_size, pre_output):
        """"
        Args:
            previous_in_channels (list)
            out_channels (int)
            pre_output (nn.Module)
        """
        super(Xcoder, self).__init__()
        assert out_channels % 2 == 0
        self.in_channels = sum(previous_in_channels)
        self.out_channels = out_channels
        self.conv1 = ConvELU3D(in_channels=self.in_channels,
                               out_channels=self.out_channels // 2,
                               kernel_size=kernel_size)
        self.conv2 = ConvELU3D(in_channels=self.in_channels + (self.out_channels // 2),
                               out_channels=self.out_channels // 2,
                               kernel_size=kernel_size)
        self.pre_output = pre_output

    def forward(self, input_):
        conv1_out = self.conv1(input_)
        conv2_inp = torch.cat((input_, conv1_out), 1)
        conv2_out = self.conv2(conv2_inp)
        conv_out = torch.cat((conv1_out, conv2_out), 1)
        if self.pre_output is not None:
            out = self.pre_output(conv_out)
        else:
            out = conv_out
        return out


class Encoder(Xcoder):
    def __init__(self, previous_in_channels, out_channels, kernel_size, sampling_scale):
        super(Encoder, self).__init__(previous_in_channels, out_channels, kernel_size,
                                      pre_output=AnisotropicPool(downscale_factor=sampling_scale))


class Decoder(Xcoder):
    def __init__(self, previous_in_channels, out_channels, kernel_size, sampling_scale):
        super(Decoder, self).__init__(previous_in_channels, out_channels, kernel_size,
                                      pre_output=AnisotropicUpsample(scale_factor=sampling_scale))


class Base(Xcoder):
    def __init__(self, previous_in_channels, out_channels, kernel_size):
        super(Base, self).__init__(previous_in_channels, out_channels, kernel_size,
                                   pre_output=None)


class Output(Conv3D):
    def __init__(self, previous_in_channels, out_channels, kernel_size):
        self.in_channels = sum(previous_in_channels)
        self.out_channels = out_channels
        super(Output, self).__init__(self.in_channels, self.out_channels, kernel_size)


class DUNetSkeleton(nn.Module):
    def __init__(self, encoders, poolers, base, upsamplers, decoders, output,
                 final_activation=None, return_hypercolumns=False):
        super(DUNetSkeleton, self).__init__()
        assert isinstance(encoders, list)
        assert isinstance(decoders, list)
        assert len(encoders) == len(decoders) == 3
        assert len(poolers) == 3
        assert len(upsamplers) == 3
        assert isinstance(base, nn.Module)
        self.encoders = nn.ModuleList(encoders)
        self.decoders = nn.ModuleList(decoders)
        self.poolx2 = poolers[0]
        self.poolx4 = poolers[1]
        self.poolx8 = poolers[2]
        self.upx2 = upsamplers[0]
        self.upx4 = upsamplers[1]
        self.upx8 = upsamplers[2]
        self.base = base
        self.output = output
        if isinstance(final_activation, str):
            self.final_activation = getattr(nn, final_activation)()
        elif isinstance(final_activation, nn.Module):
            self.final_activation = final_activation
        elif final_activation is None:
            self.final_activation = None
        else:
            raise NotImplementedError
        self.return_hypercolumns = return_hypercolumns

    def forward(self, input_):

        # Say input_ spatial size is 512, i.e. input_.ssize = 512
        # Pre-pool to save computation
        input_2ds = self.poolx2(input_)
        input_4ds = self.poolx4(input_)
        input_8ds = self.poolx8(input_)

        # e0.ssize = 256
        e0 = self.encoders[0](input_)
        e0_2ds = self.poolx2(e0)
        e0_4ds = self.poolx4(e0)
        e0_2us = self.upx2(e0)

        # e1.ssize = 128
        e1 = self.encoders[1](torch.cat((input_2ds, e0), 1))
        e1_2ds = self.poolx2(e1)
        e1_2us = self.upx2(e1)
        e1_4us = self.upx4(e1)

        # e2.ssize = 64
        e2 = self.encoders[2](torch.cat((input_4ds,
                                         e0_2ds,
                                         e1), 1))
        e2_2us = self.upx2(e2)
        e2_4us = self.upx4(e2)
        e2_8us = self.upx8(e2)

        # b.ssize = 64
        b = self.base(torch.cat((input_8ds,
                                 e0_4ds,
                                 e1_2ds,
                                 e2), 1))
        b_2us = self.upx2(b)
        b_4us = self.upx4(b)
        b_8us = self.upx8(b)

        # d2.ssize = 128
        d2 = self.decoders[0](torch.cat((input_8ds,
                                         e0_4ds,
                                         e1_2ds,
                                         e2,
                                         b), 1))
        d2_2us = self.upx2(d2)
        d2_4us = self.upx4(d2)

        # d1.ssize = 256
        d1 = self.decoders[1](torch.cat((input_4ds,
                                         e0_2ds,
                                         e1,
                                         e2_2us,
                                         b_2us,
                                         d2), 1))
        d1_2us = self.upx2(d1)

        # d0.ssize = 512
        d0 = self.decoders[2](torch.cat((input_2ds,
                                         e0,
                                         e1_2us,
                                         e2_4us,
                                         b_4us,
                                         d2_2us,
                                         d1), 1))

        # out.ssize = 512
        out = self.output(torch.cat((input_,
                                     e0_2us,
                                     e1_4us,
                                     e2_8us,
                                     b_8us,
                                     d2_4us,
                                     d1_2us,
                                     d0), 1))

        if self.final_activation is not None:
            out = self.final_activation(out)

        if not self.return_hypercolumns:
            return out
        else:
            out = torch.cat((input_,
                             e0_2us,
                             e1_4us,
                             e2_8us,
                             b_8us,
                             d2_4us,
                             d1_2us,
                             d0,
                             out), 1)
            return out

class DUNet3D(DUNetSkeleton):
    def __init__(self, in_channels, out_channels, N=16,
                 final_activation='auto', sampling_scale=3,
                 return_hypercolumns=False):
        #Set attributes
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.N = N
        self.sampling_scale = sampling_scale

        # Build encoders
        encoders = [
            Encoder(previous_in_channels=[in_channels],
                    out_channels=N,
                    kernel_size=3,
                    sampling_scale=self.sampling_scale),
            Encoder([in_channels, N], 2 * N, 3, self.sampling_scale),
            Encoder([in_channels, N, 2 * N], 4 * N, 3, self.sampling_scale)
        ]
        # different poolings:
        pooling_scales = [self.sampling_scale ** (i + 1) for i in range(len(encoders))]

        # Build poolers
        poolers = [AnisotropicPool(downscale_factor=scale) for scale in pooling_scales]

        # Build base
        base = Base([in_channels, N, 2 * N, 4 * N], 4 * N, 3)

        # Build upsamplers
        upsamplers = [AnisotropicUpsample(scale_factor=scale) for scale in pooling_scales]

        # Build decoders
        decoders = [
            Decoder([in_channels, N, 2 * N, 4 * N, 4 * N], 2 * N, 3, self.sampling_scale),
            Decoder([in_channels, N, 2 * N, 4 * N, 4 * N, 2 * N], N, 3, self.sampling_scale),
            Decoder([in_channels, N, 2 * N, 4 * N, 4 * N, 2 * N, N], N, 3, self.sampling_scale)
        ]
        output = Output([in_channels, N, 2 * N, 4 * N, 4 * N, 2 * N, N, N], out_channels, 3)
        # Parse final activation
        if final_activation == 'auto':
            final_activation = nn.Sigmoid()
        assert final_activation != 'Softmax2d'
        # dundundun
        super(DUNet3D, self).__init__(encoders=encoders,
                                      poolers=poolers,
                                      base=base,
                                      upsamplers=upsamplers,
                                      decoders=decoders,
                                      output=output,
                                      final_activation=final_activation,
                                      return_hypercolumns=return_hypercolumns)

if __name__=='__main__':
    model = DUNet3D(1, 1, sampling_scale=3, return_hypercolumns=True)
    from torch.autograd import Variable
    input_ = Variable(torch.rand(1, 1, 1, 270, 270))
    output = model(input_)
    print(output)

