import torch
import torch.nn as nn
from torch.autograd import Variable
from inferno.extensions.layers.convolutional import BNReLUConv3D, Conv3D

class AnisotropicPool(nn.MaxPool3d):
    def __init__(self, downscale_factor):
        ds = downscale_factor
        super(AnisotropicPool, self).__init__(kernel_size=(1, ds + 1, ds + 1),
                                              stride=(1, ds, ds),
                                              padding=(0, 1, 1))


class AnisotropicUpsample(nn.Module):
    def __init__(self, scale_factor):
        super(AnisotropicUpsample, self).__init__()
        self.upsampler = nn.UpsamplingNearest2d(scale_factor=scale_factor)

    def forward(self, input_):
        # input is 3D of shape NCDHW
        N, C, D, H, W = input_.size()
        # Fold C and D axes in one
        folded = input_.view(N, C * D, H, W)
        # Upsample
        upsampled = self.upsampler(folded)
        # Unfold out the C and D axes
        unfolded = upsampled.view(N, C, D,
                                  self.upsampler.scale_factor * H,
                                  self.upsampler.scale_factor * W)
        # Done
        return unfolded


class UNet3D(nn.Module):
    def __init__(self, in_channels, out_channels, final_activation='auto',
                 sampling_scale=3, return_hypercolumns=False):
        super(UNet3D, self).__init__()
        # Set attributes
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.sampling_scale = sampling_scale
        self.return_hypercolumns = return_hypercolumns

        # Build encoders with proper number of feature maps
        # (same as MALA paper)
        f0e = 12
        f1e = 60
        f2e = 300

        # Encoding path
        self.ec0 = BNReLUConv3D(in_channels=in_channels, out_channels=f0e, kernel_size=3)
        self.ec1 = BNReLUConv3D(in_channels=f0e, out_channels=f0e, kernel_size=3)
        self.pool0 = AnisotropicPool(downscale_factor=self.sampling_scale)

        self.ec2 = BNReLUConv3D(f0e, f1e, 3)
        self.ec3 = BNReLUConv3D(f1e, f1e, 3)
        self.pool1 = AnisotropicPool(self.sampling_scale)

        self.ec4 = BNReLUConv3D(f1e, f2e, 3)
        self.ec5 = BNReLUConv3D(f2e, f2e, 3)
        self.pool2 = AnisotropicPool(self.sampling_scale)

        # Build base
        # number of base output feature maps
        f0b = 1500
        self.base0 = BNReLUConv3D(f2e, f0b, 3)
        self.base1 = BNReLUConv3D(f0b, f0b, 3)

        # Build decoders (same number of feature maps as MALA)
        f2d = 300
        f1d = 60
        f0d = 12

        # Decoding path
        self.dc5 = BNReLUConv3D(f0b + f2e, f2d, 3)
        self.dc4 = BNReLUConv3D(f2d, f2d, 3)
        self.up2 = AnisotropicUpsample(scale_factor=self.sampling_scale)

        self.dc3 = BNReLUConv3D(f2d + f1e, f1d, 3)
        self.dc2 = BNReLUConv3D(f1d, f1d, 3)
        self.up1 = AnisotropicUpsample(self.sampling_scale)

        self.dc1 = BNReLUConv3D(f1d + f0e, f0d, 3)
        self.dc0 = BNReLUConv3D(f0d, f0d, 3)
        self.up0 = AnisotropicUpsample(self.sampling_scale)

        # Build output
        self.output = Conv3D(in_channels=f0d, out_channels=out_channels, kernel_size=3)

        # Parse final activation
        if final_activation == 'auto':
            self.final_activation = nn.Sigmoid() if out_channels == 1 else nn.Softmax2d()

    def forward(self, input_):

        # all spatial sizes (ssize) for 512 input size
        # and donwscaling by a factor of 2
        # apply first decoder
        # e0.ssize = 256 (ssize / scale_factor)
        e0 = self.ec0(input_)
        syn0 = self.ec1(e0)
        e1 = self.pool0(syn0)

        # apply second encoder
        # e1.ssize = 128 (ssize / (scale_factor**2))
        e2 = self.ec2(e1)
        syn1 = self.ec3(e2)
        e3 = self.pool1(syn1)

        # apply third encoder
        # e2.ssize = 64 (ssize / (scale_factor**3))
        e4 = self.ec4(e3)
        syn2 = self.ec5(e4)
        e5 = self.pool2(syn2)

        # apply the base
        # b.ssize =
        b0 = self.base0(e5)
        b1 = self.base1(b0)

        # apply the third / lowest decoder with input from base
        # and encoder 2
        # d2.ssize = 128
        d5 = self.dc5(torch.cat((b1, e5), 1))
        syn3 = self.dc4(d5)
        d4 = self.up2(syn3)

        # apply the second decoder with input from the third decoder
        # and the second encoder
        # d1.ssize = 256
        d3 = self.dc3(torch.cat((d4, e3), 1))
        syn4 = self.dc2(d3)
        d2 = self.up1(syn4)

        # apply the first decoder with input from the second decoder
        # and the first encoder
        # d0.ssize = 512
        d1 = self.dc1(torch.cat((d2, e1), 1))
        syn5 = self.dc0(d1)
        d0 = self.up0(syn5)

        # out.ssize = 512
        out = self.output(d0)

        if self.final_activation is not None:
            out = self.final_activation(out)

        if not self.return_hypercolumns:
            return out
        else:
            out = torch.cat((input_,
                             e0,
                             syn0,
                             self.up0(e1),
                             self.up0(e2),
                             self.up0(syn1),
                             self.up0(self.up0(e3)),
                             self.up0(self.up0(e4)),
                             self.up0(self.up0(syn2)),
                             self.up0(self.up0(self.up0(e5))),
                             self.up0(self.up0(self.up0(b0))),
                             self.up0(self.up0(self.up0(b1))),
                             self.up0(self.up0(self.up0(d5))),
                             self.up0(self.up0(self.up0(syn3))),
                             self.up0(self.up0(d4)),
                             self.up0(self.up0(d3)),
                             self.up0(self.up0(syn4)),
                             self.up0(d2),
                             self.up0(d1),
                             self.up0(syn5),
                             d0,
                             out), 1)
            return out

if __name__=='__main__':
    model = UNet3D(1, 1, sampling_scale=3, return_hypercolumns=True)
    input_ = Variable(torch.rand(1, 1, 1, 108, 108))
    out = model(input_)
    print(out[0, 0] == input_)
