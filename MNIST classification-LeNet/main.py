'''

MNIST-digits classification using LeNet in keras

'''

import matplotlib.pyplot as plt
import numpy as np
from mnist import load_mnist

# Tensorflow
import theano as th

# KERAS
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import convolutional
from keras.utils import np_utils
from keras import optimizers
from keras import backend as K
from plot_function import plot_confusion_matrix, find_misclassfied,plot_misclassified
from sklearn.metrics import confusion_matrix

np.random.seed(1337)  # for reproducibility
batch_size = 128
nb_epoch = 1
digits = np.arange(10)
nb_classes = len(digits)

def main():

    # input image dimensions
    img_rows, img_cols = 28, 28
    # number of convolutional filters to use
    nb_filters = 32
    # size of pooling area for max pooling
    pool_size = (2, 2)
    # convolution kernel size
    kernel_size = (3, 3)
    # the data, shuffled and split between train and test sets
    X_train, y_train = load_mnist(dataset='training', digits=digits)
    X_test, y_test = load_mnist(dataset='testing', digits=digits)
    print(X_train.shape)
    if K.image_dim_ordering() == 'th':
        X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
        X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 1)
        X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    X_train /= 255
    X_test /= 255
    # convert class vectors to binary class matrices
    Y_train = np_utils.to_categorical(y_train, nb_classes)
    Y_test = np_utils.to_categorical(y_test, nb_classes)

    #X_train = X_train[0:1000]
    #Y_train = Y_train[0:1000]

    print('X_train shape:', X_train.shape)
    print(X_train.shape[0], 'train samples')
    print(X_test.shape[0], 'test samples')

    model = Sequential()
    model.add(convolutional.ZeroPadding2D(input_shape=input_shape,padding=(1,1)))
    model.add(Convolution2D(2*nb_filters, kernel_size[0], kernel_size[1],
                            border_mode='valid',subsample=(1, 1)))
    model.add(Activation('relu'))
    model.add(Convolution2D(nb_filters, kernel_size[0], kernel_size[1],subsample=(1, 1)))
    model.add(Activation('relu'))
    model.add(convolutional.ZeroPadding2D(padding=(1, 1)))
    model.add(Convolution2D(2*nb_filters, kernel_size[0], kernel_size[1], subsample=(1, 1)))
    model.add(Activation('relu'))
    model.add(Convolution2D(2 * nb_filters, kernel_size[0], kernel_size[1], subsample=(1, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=pool_size,strides=(2,2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))
    adam=optimizers.Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy',
                  optimizer=adam,
                  metrics=['accuracy'])

    
    M1 = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, validation_data=(X_test, Y_test))
    score1 = model.evaluate(X_test, Y_test, verbose=0)
    print('Test score for Deep network:', score1[0])
    print('Test accuracy for Deep network:', score1[1])
    
    #LeNet model
    model_1 = Sequential()

    model_1.add(Convolution2D(nb_filters, kernel_size[0], kernel_size[1],
                            border_mode='valid',
                            input_shape=input_shape))
    model_1.add(Activation('relu'))
    model_1.add(Convolution2D(nb_filters, kernel_size[0], kernel_size[1]))
    model_1.add(Activation('relu'))
    model_1.add(MaxPooling2D(pool_size=pool_size))
    model_1.add(Dropout(0.25))
    
    model_1.add(Flatten())
    model_1.add(Dense(128))
    model_1.add(Activation('relu'))
    model_1.add(Dropout(0.5))
    model_1.add(Dense(nb_classes))
    model_1.add(Activation('softmax'))
    
    model_1.compile(loss='categorical_crossentropy',
                  optimizer='adadelta',
                  metrics=['accuracy'])
    
    
    M2 = model_1.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, validation_data=(X_test, Y_test))
    score2 = model_1.evaluate(X_test, Y_test, verbose=0)
    print('Test score for LeNet:', score2[0])
    print('Test accuracy for LeNet:', score2[1])
    
    #training loss plot
    fig1=plt.figure()
    plt.plot(M1.history['loss'],'b', label='Deep Network')
    plt.plot(M2.history['loss'],'r', label='LeNet')
    plt.legend(loc='best', shadow=True)
    plt.xlabel('epochs')
    plt.ylabel('training loss')
    plt.suptitle('Training Loss vs Epoch curve')
    fig1.savefig('Training Loss vs Epoch curve',dpi=300)
    
    #training accuracy plot
    fig2=plt.figure()
    plt.plot(M1.history['acc'],'b', label='Deep Network')
    plt.plot(M2.history['acc'],'r', label='LeNet')
    plt.legend(loc='best', shadow=True)
    plt.xlabel('epochs')
    plt.ylabel('training accuracy')
    plt.suptitle('Training Accuracy vs Epoch curve')
    fig2.savefig('Training Accuracy vs Epoch curve')
    
    #test error plot
    fig3=plt.figure()
    plt.plot(M1.history['val_loss'],'b', label='Deep Network')
    plt.plot(M2.history['val_loss'],'r', label='LeNet')
    plt.legend(loc='best', shadow=True)
    plt.xlabel('epochs')
    plt.ylabel('test loss')
    plt.suptitle('Test Loss vs Epoch curve')
    fig3.savefig('Test Loss vs Epoch curve')
    
    
    #test accuracy plot
    fig4=plt.figure()
    plt.plot(M1.history['val_acc'],'b', label='Deep Network')
    plt.plot(M2.history['val_acc'],'r', label='LeNet')
    plt.legend(loc='best', shadow=True)
    plt.xlabel('epochs')
    plt.ylabel('test accuracy')
    plt.suptitle('Test Accuracy vs Epoch curve')
    fig4.savefig('Test Accuracy vs Epoch curve')
    
    y_pred=model.predict_classes(X_test,verbose=0)
    y_pred_proba=model.predict_proba(X_test, verbose=0)
    cnf_matrix = confusion_matrix(y_test, y_pred)
    fig5=plt.figure()
    np.set_printoptions(precision=2)
    class_names=str(digits)
    plot_confusion_matrix(cnf_matrix, classes=class_names,normalize=True,
    title='Normalized confusion matrix')
    fig5.savefig('Confusion_matrix.png', dpi=300)
    
    y_test=y_test.flatten()
    y_pred=y_pred.flatten()
    index_misclassified=find_misclassfied(y_test, y_pred)
    
    plot_misclassified(X_test, index_misclassified, y_pred,y_pred_proba, img_rows, img_cols)



    
    

    




if __name__ == '__main__':
    main()