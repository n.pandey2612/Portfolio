import itertools
import numpy as np
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.model_selection import train_test_split




def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm=round(cm,2)
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.

    plt.imshow(cm, interpolation='nearest', cmap=cmap,aspect='auto')
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, str(round(cm[i,j],2)),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def find_misclassfied(y_test, y_pred):
    index_misclassified=[]
    num_samples=len(y_test)
    for i in range(num_samples):
        if y_test[i]!=y_pred[i]:
            index_misclassified.append(i)

    return index_misclassified

def plot_misclassified(X_test, index_misclassified,y_pred,y_pred_proba,img_rows, img_cols):
    max_subplot_num=3
    const_num=100*max_subplot_num+10*max_subplot_num+1
    num_plots=int(len(index_misclassified)/4.0)+1
    counter=0
    for i in range(num_plots):
        f = plt.figure()
        plot_name='misclassified_samples_'+str(i)+'.png'
        for j in range(max_subplot_num**2):
            if counter<len(index_misclassified):
                subplot_num=int(const_num+j)
                #plt.subplot(subplot_num)
                ax = f.add_subplot(subplot_num)
                index=index_misclassified[counter]
                im = np.reshape(X_test[index], (img_rows, img_cols))
                plt.imshow(im,cmap=plt.cm.binary, interpolation='nearest')
                plt.axis('off')
                sof_proba=np.max(y_pred_proba[index])
                details1='Predicted Class='+str(y_pred[index])
                details2='Softmax prob='+str(round(sof_proba,2))
                plt.text(0.1,0.2,details1,transform=ax.transAxes)
                plt.text(0.1, 0.1, details2, transform=ax.transAxes)
                counter += 1
        f.savefig(plot_name, dpi=300)






