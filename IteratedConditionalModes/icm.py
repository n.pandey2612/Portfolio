# -*- coding: utf-8 -*-
"""
Created on Thu May  4 18:54:40 2017

@author: Neha Pandey

Please note that I have substituted unaries with image which is the result of predict_proba. Hence, a minor change in
the function 'iterated_conditonal_modes' of replacing argmin wth argmax to find labels.
"""

import numpy
import pickle as pkl
def iterated_conditonal_modes ( image , beta , labels = None ):
    shape = image . shape [0:2]
    n_labels = image . shape [2]
    if labels is None :
        labels = numpy . argmax ( image , axis =2)
    continue_search = True
    while ( continue_search ):
        continue_search = False
        for x0 in range (1, shape [0] -1):
            for x1 in range (1, shape [1] -1):
                current_label = labels [x0 , x1]
                min_energy = float ('inf' )
                best_label = None
                for l in range ( n_labels ):
                    # evaluate cost
                    energy = 0.0
                    # unary terms
                    energy += (-numpy.log(0.01 + image[x0][x1][l]))
                    # pairwise terms
                    for x, y in [(x0, x1-1), (x0, x1+1), (x0-1, x1), (x0+1, x1)]:
                        if(labels[x, y]==l):
                            energy += 0*beta
                        else:
                            energy += 1*beta
                    if energy < min_energy :
                        min_energy = energy
                        best_label = l
                if best_label != current_label :
                    labels [x0 , x1] = best_label
                    continue_search = True
    return labels
if __name__ == "__main__":
    import matplotlib . pyplot as plt
    n_labels = 2
    images = pkl.load(open('probability_images.pkl', 'rb'))
    for j, image in enumerate(images):
        fig=plt.figure()
        fig.subplots_adjust(hspace=.5)
        
        ax2=fig.add_subplot(1, 3, 1)
        plt.imshow (image[:, :, 1], cmap='gray')
        ax2.set_title("Predicted Image")
        
        ax2=fig.add_subplot(1, 3, 2)
        labels = iterated_conditonal_modes (image, beta = 0.01)
        plt.imshow (labels, cmap='gray')
        ax2.set_title("beta=0.01")
        
        ax3=fig.add_subplot(1, 3, 3)
        labels = iterated_conditonal_modes (image, beta = 10)
        plt.imshow (labels, cmap='gray')
        ax3.set_title("beta=10")
        
        
        plt.savefig("icm "+str(j+1))
