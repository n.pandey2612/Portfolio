# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 05:59:42 2017

@author: Neha Pandey
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve
from matplotlib import colors

#Build laplacian
def build_laplacian_1D(n):
    #Degree Matrix
    D=np.zeros((n, n));
    for i in range(n):
        if(i==0 or i==n-1):
            D[i][i]=1;
        else:
            D[i][i]=2;
    #Adjacency Matrix
    Ad=np.zeros((n, n));
    for i in range(n-1):
        Ad[i][i+1]=1;
        Ad[i+1][i]=1;
    #Laplacian Matrix
    L=D-Ad;
    return L
        
        
def build_laplacian_2D(m, gamma):
    n=m*m;
    #Degree Matrix
    D=np.zeros((n, n));
    for i in range(m):
        for j in range(m):
            #corner pixels
            if((i+j)==0 or (i+j)==m-1 or i*j==(m-1)**2):
                D[m*i+j][m*i+j]=2;
            #border pixels excluding corner pixels
            elif(i==0 or j==0 or i==m-1 or j==m-1):
                D[m*i+j][m*i+j]=3;
            #rest of the pixels
            else:
                D[m*i+j][m*i+j]=4;
            
    #Adjacency Matrix
    Ad=np.zeros((n, n));
    for i in range(m):
        for j in range(m):
            if i-1>=0:
               neigh=(i-1)*m+j       
               Ad[m*i+j, neigh]=np.exp(-gamma*np.linalg.norm(img[i, j]-img[i-1,j])**2)
            if i+1<m:
               neigh=(i+1)*m+j 
               Ad[m*i+j, neigh]=np.exp(-gamma*np.linalg.norm(img[i, j]-img[i+1,j])**2)
            if j-1>=0:
               neigh=(i)*m+j-1
               Ad[m*i+j, neigh]=np.exp(-gamma*np.linalg.norm(img[i, j]-img[i,j-1])**2)
            if j+1<m:
               neigh=(i)*m+j+1
               Ad[m*i+j, neigh]=np.exp(-gamma*np.linalg.norm(img[i, j]-img[i,j+1])**2)
                   
    #Laplacian Matrix
    L= D-Ad;
    return L;

#Extract A and B matrices from Laplacian
def getA_B(L, seeds):
    all_pixels=list(range(np.size(L, 0)));
    seeded=seeds;
    unseeded=[element for i, element in enumerate(all_pixels) if i not in seeds];
    A=L[unseeded, :][:, unseeded];
    B=L[unseeded, :][:, seeded];
    return A, B;
    

#Linear solve 
def solve(A, B, f_c):
    #convert A to sparse matrix
    S = sp.csr_matrix(A);
    prob=spsolve(S,-B.dot(f_c).astype('float64'));
    return prob;

#Fast random walker 
def fast_random_walker(m, seeds, mode,  gamma=0):
    if(mode=='1D'):
        L=build_laplacian_1D(m);
    else:
        L=build_laplacian_2D(m, gamma);
    A, B=getA_B(L, seeds)
    prob=[];
    f_c=np.identity(np.size(seeds));
    for i in range(np.size(seeds)):
        prob_i=solve(A, B, f_c[i]);
        prob.append(prob_i);
    prob=np.array(prob);
    #insert probabilities for seeded pixels
    for i in range(np.size(seeds)):
        prob=np.insert(prob, seeds[i], f_c[:, i] , axis=1);
    prob=(np.abs(prob))/(np.sum(np.abs(prob), 0));
    return prob;

#build 2D seeded image of size m 
def build_2D_data(m, seed):
    #create image   
    data = np.ones((m,m))
    for i in range(0,m):
        for j in range(0,m):
            if(i<13):
                data[i,m-1-i]=1            
            if(18<i<m):
                data[i,m-1-i]=1
            data[i,j]=0
    img = data
    #convert seeds to linear form
    seeds=[m*seed[0][0]+seed[0][1], m*seed[1][0]+seed[1][1]];
    return img, seeds;

"""/////////////////////    #Problem 1- 1D Random Walker  //////////////////////////////////////////"""

seeds1D=[3, 8]
prob1D=fast_random_walker(12, seeds1D, mode='1D');
#plotting probabilities
fig1=plt.figure()
ax1=fig1.add_subplot(1,1,1)
ax1.plot(prob1D[0, :], '--bo', label='seed at (0,3)');
ax1.plot(prob1D[1, :], '--co', label='seed at (0,8)');
ax1.set_xlabel('position, x')
ax1.set_ylabel('probability')
ax1.legend(loc='best', shadow=True)
ax1.grid()
fig1.suptitle('Seed probabilities vs x')
fig1.savefig('pro_1D')   

"""/////////////////////    #Problem 2- 2D Random Walker  //////////////////////////////////////////"""
m=30;
gammas=[0.01, 0.1, 1, 10];
#generate seeds
Seed2D=[(12, int((m-1)/2)), (m-3, int((m-1)/2))];
#generate image
img, seed2D=build_2D_data(m, Seed2D);
fig2 = plt.figure(figsize=(6, 6));
count=0;
for gamma in gammas:
    #Call random walker
    prob2D=fast_random_walker(30, seed2D, mode='2D',  gamma=gamma);
    #get probability maps for both seeds 
    prob1=np.reshape(prob2D[0], (30, 30));
    ax1=fig2.add_subplot(4, 4, 4*count+1);
    ax1.set(xlabel='Seed1_prob');

    plt.imshow(prob1, cmap='hot', interpolation='nearest');
    prob2=np.reshape(prob2D[1], (30, 30));
    ax2=fig2.add_subplot(4, 4, 4*count+2);
    ax2.set(xlabel='Seed2_prob');
    plt.imshow(prob2, cmap='hot', interpolation='nearest');
    #get labelled image
    labels=np.zeros_like(img, dtype=int);
    for i in range(np.size(img, 0)):
        for j in range(np.size(img, 0)):
            if(prob1[i, j] > prob2[i, j]):
                labels[i, j]= 0;
            else:
                labels[i, j]= 1;
    ax3=fig2.add_subplot(4, 4, 4*count+3);
    ax3.set(xlabel='Input');
    plt.imshow(img, cmap='binary', interpolation='nearest');
    ax3.plot(Seed2D[0][1], Seed2D[0][0], marker='o', markersize=3, label='seed1', color="red");
    ax3.plot(Seed2D[1][1], Seed2D[1][0], marker='o', markersize=3, label='seed1', color="blue");
    ax4=fig2.add_subplot(4, 4, 4*count+4);
    ax4.set(xlabel='Output Labels');
    cmap = colors.ListedColormap(['red', 'blue'])
    bounds=[0,0.1, 1]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    # tell imshow about color map so that only set colors are used
    plt.imshow(labels, interpolation='nearest', origin='upper',
                    cmap=cmap, norm=norm)
    #plt.imshow(labels, cmap='binary', interpolation='nearest');
    count=count+1;

plt.tight_layout();
fig2.suptitle('RandomWalk2D for different gamma=0.01, 0.1, 1, 10', y=1.05);
fig2.savefig('RandomWalk2D_diff');  


    
    
        
    
    
     
   
      
      
      