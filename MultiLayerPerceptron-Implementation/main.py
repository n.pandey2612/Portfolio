# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 13:19:48 2016

@author: Neha Pandey
"""

import matplotlib.pyplot as plt
import numpy as np
from sklearn.utils import shuffle
from prepare_data_XOR import prepare_data_XOR
from get_CIFAR_data import get_CIFAR_data
from sklearn.metrics import accuracy_score

def unit_step(x):
    x[x>0]=1
    x[x<0]=0
    return x
    
        
def sigmoid(x):
    f=1/(1+np.exp(-x))
    return f
    
def predict(X,weight,bias):
    a=sigmoid(np.dot(X,weight[0])+bias[0])
    y_predicted=unit_step(np.dot(a,weight[1])+bias[1])
    return y_predicted
    
def draw_decision_boundary(data_set,data1,data2,weight,b,fig_name):
    h = .2  # step size in the mesh
    # create a mesh to plot in
    x_min, x_max = data_set[:, 0].min() - 1, data_set[:, 0].max() + 1
    y_min, y_max = data_set[:, 1].min() - 1, data_set[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),np.arange(y_min, y_max, h))
    z1= predict(np.c_[xx.ravel(), yy.ravel()],weight,b)
    z1 = z1.reshape(xx.shape)
    plt.contour(xx,yy,z1,levels=[0])
    plt.scatter(data1[:,0],data1[:,1], marker='+')
    plt.scatter(data2[:,0],data2[:,1], c= 'green', marker='o')
    plt.suptitle(fig_name)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.savefig(fig_name.replace('.',','))
    plt.show()

def learning_curve(epochs,loss,alpha,flag,accuracy,accuracy2,fig_title,h):
    plt.plot(np.arange(0,epochs), loss)
    plt.suptitle(fig_title)
    plt.xlabel("Epoch #")
    plt.ylabel("Loss")
    plt.annotate('#Hidden neurons={1}'.format(alpha,h), xy=(0.5, 0.90), xycoords='axes fraction')
    plt.annotate('learning rate={1}'.format(alpha,alpha), xy=(0.5, 0.85), xycoords='axes fraction')
    plt.annotate('training accuracy={1:0.2f}'.format(alpha,accuracy), xy=(0.5, 0.80), xycoords='axes fraction')
    if flag==True:
        plt.annotate('test accuracy={1:0.2f}'.format(alpha,accuracy2), xy=(0.5, 0.75), xycoords='axes fraction')
    
    plt.savefig(fig_title.replace('.',','))
    plt.show()        
    
class mlp:
    
    def __init__(self,layers):
        self.layers=layers
        self.weight=[]
        for i in range(1,len(layers)):
            w=np.random.randn(layers[i-1],layers[i])
            self.weight.append(w)
            
        self.bias=np.random.randn(len(self.layers)-1,1)
        
    def fit(self,X,y,epochs,learning_rate,batch_size,isShuffle=True):
        loss=[]
        for k in range(epochs):
            X, y = shuffle(X,y)
            n_batches=int(len(X)/batch_size)
            batches=np.vsplit(X,n_batches)
            batches_y=np.vsplit(y,n_batches)
            loss_epoch=[]
            for j in range(len(batches)):
                node=[]
                X_layer=batches[j]
                for i in range(1,len(self.layers)):
                    X_layer_n=sigmoid(np.dot(X_layer,self.weight[i-1])+self.bias[i-1])
                    node.append(X_layer_n)
                    X_layer=X_layer_n
                error=batches_y[j]-node[-1]

                delta_output=error*((1-node[-1]))*node[-1]
                self.weight[-1]=self.weight[-1]+learning_rate*np.dot(node[-2].T,delta_output)
                self.bias[-1]=self.bias[-1]+learning_rate*np.sum(delta_output)
            
                delta_hidden=(node[-2]*(1-node[-2]))*(np.dot(delta_output,self.weight[-1].T))
                self.weight[-2]=self.weight[-2]+learning_rate*np.dot(batches[j].T,delta_hidden)
                self.bias[-2]=self.bias[-2]+learning_rate*np.sum(delta_hidden)
            
                loss_batch=(np.sum(error**2))/(2*len(batches[j]))
                loss_epoch.append(loss_batch)
        
            loss.append(np.average(loss_epoch))
                
        y_predicted=predict(X,self.weight,self.bias)
        accuracy=accuracy_score(y,y_predicted)
        return y_predicted,loss,accuracy,self.weight,self.bias
        
if __name__ == '__main__':
    hn=[2,10,50,100]
    alpha_n=[1,0.1,0.01]
    for h in hn:
        for alpha in alpha_n:
            #working with gaussian XOR data
            
            N_samples=50
            sigma_n=[1,3,5]
            for sigma in sigma_n:
                X,y=prepare_data_XOR(N_samples, sigma)
                NNet=mlp([2,h,1])
                y_predicted,loss,accuracy,weight,bias=NNet.fit(X,y,epochs=1000,learning_rate=0.01,batch_size=50)
                data1=X[np.any(y==0, axis=1),:]
                data2=X[np.any(y==1, axis=1),:]
                alpha_s=str(alpha)
                name1='decision boundary for sigma={0}, #Hidden neurons={1}, alpha={2}'.format(sigma,h,alpha_s)
                name2='learning curve for sigma={0}, #Hidden neurons={1}, alpha={2}'.format(sigma,h,alpha_s)
                draw_decision_boundary(X,data1,data2,weight,bias,name1)
                learning_curve(epochs=1000,loss=loss,alpha=alpha_s,flag=False,accuracy=accuracy,accuracy2=0,fig_title=name2,h=h)
                
            
            """
            #working with CIFAR-10 dataset
            train_1,test,train_label_1,test_label=get_CIFAR_data()
            train=train_1[0:1550,:]
            train_label=train_label_1[0:1550,:]
            NNet_CIFAR=mlp([512,h,1])
            y_predicted_CIFAR,loss_CIFAR,accuracy_CIFAR,weight_CIFAR,bias_CIFAR=NNet_CIFAR.fit(train,train_label,epochs=1000,learning_rate=0.01,batch_size=50)
            
            
            y_predicted_test=predict(test,weight_CIFAR,bias_CIFAR)
            accuracy_test=accuracy_score(test_label,y_predicted_test)
            
            alpha_s=str(alpha)
            name3='learning curve for #Hidden neurons={0}, alpha={1}'.format(h,alpha_s)
            learning_curve(epochs=1000,loss=loss_CIFAR,alpha=alpha_s,flag=True,accuracy=accuracy_CIFAR,accuracy2=accuracy_test,fig_title=name3,h=h)            
            """       
                
        
        
        
    


    
    

        
    


