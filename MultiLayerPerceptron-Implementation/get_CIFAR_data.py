# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:43:33 2016

@author: dell pc
"""

import numpy as np
from skimage.feature import hog
from skimage import color
from sklearn.model_selection import train_test_split

def get_CIFAR_data():     
    def unpickle(file):
        import pickle
        fo = open(file, 'rb')
        dict = pickle.load(fo,encoding='latin1')
        fo.close()
        return dict
    
    batch=unpickle('data_batch_1')
    batch['data']=batch ['data'].reshape([-1 , 3 , 32 , 32])
    batch ['data'] = batch ['data'].transpose ( ( 0 , 2 , 3 , 1 ) )

    airplane_data=[]
    dogs_data=[]

    for i in range(0,10000):
        if(batch['labels'][i]==0):
            airplane_data.append(batch['data'][i])
        elif(batch['labels'][i]==5):
            dogs_data.append(batch['data'][i])
        else:
            pass


    def extract_hog_features(image_set):
        hog_features=[]
        for i in range(0,len(image_set)):
            gray_image=color.rgb2gray(image_set[i])
            fd= hog(gray_image, orientations=8, pixels_per_cell=(4, 4),cells_per_block=(1, 1))
            
            hog_features.append(fd)
        return hog_features
        
    hog_airplane_data=extract_hog_features(airplane_data)
    hog_airplane_data=np.array(hog_airplane_data)
    hog_dogs_data=extract_hog_features(dogs_data)
    hog_dogs_data=np.array(hog_dogs_data)
    label_airplane=np.full((1,len(hog_airplane_data)),-1)
    label_dogs=np.full((1,len(hog_dogs_data)),1)
    
    Airplane_train, Airplane_test, airplane_train, airplane_test = train_test_split(hog_airplane_data,np.transpose(label_airplane),test_size=0.2)
    Dogs_train, Dogs_test, dogs_train, dogs_test = train_test_split(hog_dogs_data,np.transpose(label_dogs),test_size=0.2)
    
    train=np.vstack((Airplane_train,Dogs_train))
    test=np.vstack((Airplane_test,Dogs_test))
    train_label=np.vstack((airplane_train,dogs_train))
    test_label=np.vstack((airplane_test,dogs_test))
    
    return train,test,train_label,test_label