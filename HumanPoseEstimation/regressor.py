# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 11:08:32 2017

@author: NehaPandey

"""

import matplotlib.pyplot as plt
import numpy as np
import pickle as pkl
import os
from PIL import Image
import glob
from skimage.transform import resize
from scipy.io import loadmat
from math import hypot


# Tensorflow
import theano as th

# KERAS
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation,Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator


img_rows = 220
img_cols = 220
batch_size = 32
nb_epoch = 15

# loading LEEDS dataset
image_list = []
for filename in glob.glob('images/*.jpg'):
    im=Image.open(filename)
    result=np.zeros([220,220,3])
    img = np.array(im)
    result[:img.shape[0], :img.shape[1], :img.shape[2]] = img
    image_list.append(result)

image_data=np.array(image_list)
joints=loadmat('joints.mat')
y=joints['joints']
y=y.T
y=y[:,:,0:2]
y=y.reshape(2000,28)

#Data Preprocessing

if K.image_dim_ordering() == 'th':
    X_train = image_data.reshape(image_data.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    X_train = image_data.reshape(image_data.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

X_train = X_train.astype('float32')
X_train /= 255

X_train_n=X_train[0:1500,:,:,:]
y_train=y[0:1500,:]
X_test=X_train[1500:,:,:,:]
y_test=y[1500:,:]

X_train=X_train_n


print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print('X_test shape:', X_test.shape)
print(X_test.shape[0], 'test samples')


#Model
model = Sequential()
model.add(Convolution2D(32, 3, 3, border_mode='valid', name='conv1_1', input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Convolution2D(64, 3, 3, border_mode='valid', name='conv1_2'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Convolution2D(128, 3, 3, border_mode='valid', name='conv1_3'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(28))
model.compile(loss='mean_squared_error', optimizer='adam')

#Data Augmentation

datagen = ImageDataGenerator(
    featurewise_center=False,
    featurewise_std_normalization=False,
    rotation_range=20,
    width_shift_range=0.0,
    height_shift_range=0.0,
    horizontal_flip=True)
datagen.fit(X_train)

filename='regressor'
weights_path = '{}.h5'.format(filename)
history_path = '{}.pkl'.format(filename)
if not os.path.exists(weights_path):
    # run training
    history = M=model.fit_generator(datagen.flow(X_train, y_train, batch_size=batch_size),
                    samples_per_epoch=len(X_train), nb_epoch=nb_epoch, validation_data=(X_test,y_test))
    # save model
    model.save_weights(weights_path)
    # save history
    with open(history_path, 'wb') as outfile:
        pkl.dump(history.history, outfile)
    history = history.history
else:
    # load pre-trained weights
    model.load_weights(weights_path)
    # load history
    with open(history_path, 'r') as infile:
        history = pkl.load(infile)


y_pred=model.predict(X_test)

#PDJ Calculation
s_all=np.arange(0.1,1,0.1)
pdj=[]
for s in s_all:

    scores_bin=[]
    for i in range(0,len(y_test)):
        ground=y_test[i,:]
        pred=y_pred[i,:]
        torso_dia=hypot(ground[4]-ground[18],ground[5]-ground[19])
        #print('torso='+str(torso_dia))
        j=0
        scores=[]
        while j<14:
            dis=hypot(ground[j]-pred[j],ground[j+1]-pred[j+1])
            #print('dist=' + str(dis))
            if dis<s*torso_dia:
                scores.append(1)
            else:
                scores.append(0)
            j=j+2
        scores_bin.append(scores)

    scores_bin=np.array(scores_bin)
    scores_fin=np.sum(scores_bin,axis=0)
    scores_fin1=scores_fin*0.002
    pdj.append(scores_fin1)

pdj=np.array(pdj)
np.savetxt('PDJ',pdj)

parts=['Right ankle',
'Right knee',
'Right hip',
'Left hip',
'Left knee',
'Left ankle',
'Right wrist',
'Right elbow',
'Right shoulder',
'Left shoulder',
'Left elbow',
'Left wrist',
'Neck',
'Head top',
]

for i in range(0,14):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    ax.plot(s_all,pdj[:,i])
    ax.set_xlabel('s')
    ax.set_ylabel('PDJ')
    ax.grid()
    fig.suptitle('PDJ vs s curve for '+parts[i])
    fig.savefig(parts[i])

fig1=plt.figure()
ax1=fig1.add_subplot(1,1,1)
ax1.plot(history['loss'],label='Training')
ax1.plot(history['val_loss'],label='Validation')
ax1.set_xlabel('#epochs')
ax1.set_ylabel('MSE')
ax1.legend(loc='best', shadow=True)
ax1.grid()
fig1.suptitle('MSE vs epochs curve')
fig1.savefig('loss')




























