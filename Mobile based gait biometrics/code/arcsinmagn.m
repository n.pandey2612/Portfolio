function [yarcsin,ymagn1]=arcsinmagn(x)
s=size(x,1);
for i=1:s
    m=x(i,1)^2+x(i,2)^2+x(i,3)^2;
ymagn(i)=sqrt(m);
for k=1:3
    yarcsin(i,k)=asin(x(i,k)/ymagn(i));
end
end
ymagn1=ymagn.';
