function [y]=walkextract(x)

    [maxtab,mintab]=peakdet(x(:,2),1);
    if (isempty(maxtab)==0)
    time=maxtab(:,1);
    len=size(time,1);
    if(len>20)
    threshold=abs(mean(maxtab(10:20,2)));
    else
      threshold=abs(mean(maxtab(:,2)));
    end
    c=0;
     for i=1:len
         if(1.2*threshold>abs(maxtab(i,2)) && abs(maxtab(i,2))>0.8*threshold)
             c=c+1;
             a(c,1)=maxtab(i,1);
         end
     end
    % len2=size(a,1);
     %error handling
     if(c~=0)
     len2=size(a,1);
     minn=a(1,1);
     %maxx=a(4,1);
      maxx=a(len2,1);   
  
   y=x(minn:maxx,:);
     
     else
         y=x;
     end
    else
        y=x;
    end