function [y]=autocorr(x)
for i=1:3
    y(:,i)=xcorr(x(:,i));
end