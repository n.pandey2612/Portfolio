function [y]=nanpos(x)
a=isnan(x);
for i=1:size(x,1)
    for j=1:4
        if(a(i,j)==1)
            y=[i,j];
        end
    end
end

        