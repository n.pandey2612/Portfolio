function [r]=inverse(x)
m=size(x,1);

for i=1:m
    for j=1:m
        for k=1:3
            r(i,j,k)=1/x(i,j,k);
        end
    end
end
