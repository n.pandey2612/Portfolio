function [a1,a2,a3,a4,a5]=wav(x)
[c,l]=wavedec(x,4,'db1');
[cd2,cd3,cd4,cd5]=detcoef(c,l,[1 2 3 4]);
n=l(1);
cd1=c(1:n);
a1=norm(cd1);
a2=norm(cd2);
a3=norm(cd3);
a4=norm(cd4);
a5=norm(cd5);

