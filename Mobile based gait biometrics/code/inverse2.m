function[r]=inverse2(x)
m=size(x,1);
for i=1:m
    for j=1:m
        %for k=1:3
            r(i,j)=1/x(i,j);
        %end
    end
end