function [EER]=EER_DET_conf(clients,imposteurs,pas0,a)
%clients=genuine1(x);
%imposteurs=imposter1(x);

%%%%% estimation of thresholds used to calculate FAR et FRR

% maximum of client scores
m0 = max (clients);

% size of client vector
num_clients = length (clients);

% minimum impostor scores
m1 = min (imposteurs);

% size of impostor vector
num_imposteurs = length (imposteurs);

% calculation of the step
pas1 = (m0 - m1)/pas0;
x = [m1:pas1:m0]';
%x=[0:pas1:50];

num = length (x);

%%%%%

%%%%% calculation of FAR and FRR

for i=1:num
    fr=0;
    fa=0;
    for j=1:num_clients
        if clients(j)<x(i)
            fr=fr+1;
        end
    end
    for k=1:num_imposteurs
        if imposteurs(k)>=x(i)
            fa=fa+1;
        end
    end
    FRR(i)=100*fr/num_clients;
    FAR(i)=100*fa/num_imposteurs;
end 

%%%%%

%%%%% calculation of EER value

%tmp1=find (FRR==FAR);
tmp1=find (FRR-FAR<=0);
tmps=length(tmp1);

if ((FAR(tmps)-FRR(tmps))<=(FRR(tmps+1)-FAR(tmps+1)))
    EER=(FAR(tmps)+FRR(tmps))/2;tmpEER=tmps;
else
    EER=(FRR(tmps+1)+FAR(tmps+1))/2;tmpEER=tmps+1;
end

%%%%%



%%%%% calculation of the confidence intervals
%[FARconfMIN  FRRconfMIN FARconfMAX FRRconfMAX]=ParamConfInter(FAR/100,FRR/100,num_imposteurs,num_clients);

% EER
%confInterEER=EER-100*(FARconfMIN(tmpEER)+FRRconfMIN(tmpEER))/2;

% Operating Point
%confInterOP=OP-100*FRRconfMIN(tmpOP);

%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% plotting of curves

% FAR vs FRR
%figure(1);
%plot (x,FRR,'r');
%hold on;plot (x,FAR,'b');
%xlabel ('Threshold');
%ylabel ('Error');
%title ('FAR vs FRR graph');

% interpolation for the plotting
equaX=x(tmps)*(FRR(tmps+1)-FAR(tmps+1))+x(tmps+1)*(FAR(tmps)-FRR(tmps));
equaY=FRR(tmps+1)-FAR(tmps+1)+FAR(tmps)-FRR(tmps);
threshold=equaX/equaY;
EERplot=threshold*(FAR(tmps)-FAR(tmps+1))/(x(tmps)-x(tmps+1))+(x(tmps)*FAR(tmps+1)-x(tmps+1)*FAR(tmps))/(x(tmps)-x(tmps+1));

% ROC curve
figure(2);
if a==1
plot (FAR,100-FRR,'r');
%hleg1 = legend('x');
end
if a==2
plot (FAR,100-FRR,'b');
%hleg2 = legend('y');
end
if a==3
plot (FAR,100-FRR,'c');
%hleg3 = legend('z');
end
if a==4
plot (FAR,100-FRR,'m');
%hleg4 = legend('statistical');
end
xlabel ('Impostor Attempts Accepted = FAR (%)');
ylabel ('Genuine Attempts Accepted = 1-FRR (%)');
title ('ROC curve');
hold on;
%scatter (EERplot,100-EERplot,'ok');
%hold on;scatter (FAR(tmpOP),100-FRR(tmpOP),'xk');
%AXIS([0 50 50 100]);

