y1=num2str(1);
x1=strcat('(',y1,')');
pathname1=strcat(pwd,'\','normalaccelerometerdatabase','\',x1,'.','csv');
pathname2=strcat(pwd,'\','normalgyroscopedatabase','\',x1,'.','csv');


z1=xlsread(pathname1);

[z1]=datapreprocess(z1);
z2=xlsread(pathname2);

[z2]=datapreprocess(z2);
for i=1:3
    figure;
    subplot(2,1,1);
    plot(z1(:,i));
    xlabel('time');
    ylabel('accelerometer reading');
    %title('original signal')
    subplot(2,1,2);
    plot(z2(:,i));
    xlabel('time');
    ylabel('gyroscope reading');
    %title('original signal')
    
end    