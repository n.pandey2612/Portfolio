function [y]=interpolation(x)
[x,zdtimenew]=timestamp(x);
len=size(zdtimenew,2);
timeend=zdtimenew(len);
for j=1:3
time=0:0.01:timeend;
yy= interp1(zdtimenew,x(:,j),time);
y(:,j)=yy.';
end


