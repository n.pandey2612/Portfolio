#include <iostream>
#include <conio.h>
#include <stack>
//#include <array>
using namespace std;

class cell
{
  private:
      int x,y;
  public:
      cell()
      {
        x=0;y=0;
      }
      cell(int i,int j)
      {
        x=i;y=j;
      }
      bool operator== (const cell& c) const
      {
        return x == c.x && y == c.y;
      }

  friend class maze;
};
class maze
{
  private:
      int rows, columns;
      int xl, yl, xr, yr, xu, yu, xd, yd;
      cell current, exit, entry, printc, cellf;
      stack<cell> s1;
      char m[20][20];

  public:
      void input()
      {
          cout<<"Enter the number of rows:";
          cin>>rows;
          cout<<"\nEnter the number of columns:";
          cin>>columns;
          //char m[rows][columns];
          cout<<"\nEnter the maze:\n";
          for(int r=0;r<rows;r++)
          {
              for(int c=0;c<columns;c++)
              {
                  cin>>m[r][c];
              }
          }
      }
      cell find(char ch)
      {
          for(int r=0;r<rows;r++)
          {
              for(int c=0;c<columns;c++)
              {
                  if (m[r][c]==ch)
                  {
                      cellf.x=r;
                      cellf.y=c;
                  }
              }
          }
          //cout<<cellf.x<<"\n";
          return cellf;
      }
      void exitmaze()
      {

      entry=find('m');
      exit=find('e');
      current.x=entry.x;
      current.y=entry.y;
      //cout<<"\n"<<current.x<<"\n"<<current.y<<"\n";


      while(!(current==exit))
      {
         if(!(current==entry))
          {
              m[current.x][current.y]='.';
          }

          xd=current.x-1;
          yd=current.y;
          cell down(xd,yd);
          if(m[xd][yd]=='0'||m[xd][yd]=='e')
          {
              //m[xd][yd]='.';
              s1.push(down);
          }
          xu=current.x+1;
          yu=current.y;
          cell up(xu,yu);
          if(m[xu][yu]=='0'||m[xu][yu]=='e')
          {
              //m[xu][yu]='.';
              s1.push(up);
          }

          xl=current.x;
          yl=current.y-1;
          cell left(xl,yl);
          if(m[xl][yl]=='0'||m[xl][yl]=='e')
          {
              //m[xl][yl]='.';
              s1.push(left);
          }
          xr=current.x;
          yr=current.y+1;
          cell right(xr,yr);
          if(m[xr][yr]=='0'||m[xr][yr]=='e')
          {
              //m[xr][yr]='.';
              s1.push(right);
          }

          current=s1.top();
          //m[current.x][current.y]='.';
          s1.pop();
      }
      //cout<<"\n"<<s1.size()<<"\n";


      for(int r=0;r<rows;r++)
          {
              for(int c=0;c<columns;c++)
              {
                  cout<<m[r][c]<<"\t";
              }
              cout<<"\n";
          }

      }
};
int main()
{
    maze maze1;
    maze1.input();
    maze1.exitmaze();
    getch();
    return 0;
   // getch();
}
