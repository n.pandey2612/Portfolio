#include "NumVector.cc"
#include<iostream>

int main()
{
NumVector u(0);
NumVector v(0);
NumVector w(10);
std::cout<<"size of u is 0"<<std::endl;
std::cout<<"size of v is 0"<<std::endl;
std::cout<<"size of w is 10"<<std::endl;
std::cout<<endl;
//Case 1
try
{
    std::cout<<"Input: u(0)*v(0)"<<std::endl;
    u*v;
}
catch(const illegal_access& e)
{
    std::cout<<e.what()<<std::endl;
}
catch(const length_mismatch& e)
{
    std::cout<<e.what()<<std::endl;
}
std::cout<<endl;
//Case 2
try
{
    std::cout<<"Input: v(0)*w(10)"<<std::endl;
    v*w;
}
catch(const illegal_access& e)
{
    std::cout<<e.what()<<std::endl;
}
catch(const length_mismatch& e)
{
    std::cout<<e.what()<<std::endl;
}
std::cout<<endl;
//Case 3
try
{
    std::cout<<"Input: w[-8]=9"<<std::endl;
    w[-8]=9;
}
catch(const illegal_access& e)
{
    std::cout<<e.what()<<std::endl;
}
catch(const length_mismatch& e)
{
    std::cout<<e.what()<<std::endl;
}
std::cout<<endl;
//Case 4
try
{
    std::cout<<"Input: w[18]=9"<<std::endl;
    w[18]=9;
}
catch(const illegal_access& e)
{
    std::cout<<e.what()<<std::endl;
}
catch(const length_mismatch& e)
{
    std::cout<<e.what()<<std::endl;
}

return 0;
}


