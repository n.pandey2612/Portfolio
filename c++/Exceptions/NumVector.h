#ifndef NUMVECTOR
#define NUMVECTOR
#include<vector>

using namespace std;

class NumVector: private vector<double>
{
public:
    NumVector();
    NumVector(int n);
    int Size() const;
    double& operator[](unsigned int i);
    const double& operator[](unsigned int i) const;
    double norm() const;
    double operator*(const NumVector& lhs) const;
};


#endif
