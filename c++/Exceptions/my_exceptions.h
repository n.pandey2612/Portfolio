#ifndef MY_EXCEPTIONS
#define MY_EXCEPTIONS
#include<iostream>
#include<exception>

class illegal_access: public exception
{
    public:
        illegal_access(int i):i(i){};
        virtual char const * what() const throw ()
        {
            if(i>0)
            {
                return "Exception illegal access: Trying to access index greater than the size of the vector.";
            }
            else
            {
                return "Exception illegal access: Trying to access negative index of a vector.";
            }
        }
        virtual ~illegal_access() throw(){};
    private:
        int i;
};

class length_mismatch: public exception
{
private:
    std::string error_msg;
public:
    length_mismatch(const std::string& message):error_msg(message){};
    virtual char const * what() const throw (){ return error_msg.c_str();}
    virtual ~length_mismatch() throw(){};
};






#endif // MY_EXCEPTIONS
