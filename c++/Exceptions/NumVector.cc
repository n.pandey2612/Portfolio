#include<iostream>
#include<cmath>
#include "NumVector.h"
#include "my_exceptions.h"
using namespace std;

NumVector::NumVector(): vector<double>(){}
NumVector::NumVector(int n): vector<double>(n){}
int NumVector::Size() const
{
    return vector<double>::size();
}
double& NumVector::operator[](unsigned int i)
{
    if((this->size()<=i) || (i<0))
    {
        throw illegal_access(i); //throw an exception when index is out of bounds or negative
    }
    else
    {
        return vector<double>::operator[](i);
    }
}
const double& NumVector::operator[](unsigned int i) const
{
    if(this->size()<=i || i < 0)
    {
        throw illegal_access(i);
    }
    else
    {
        return vector<double>::operator[](i);
    }
}
double NumVector::norm() const
{
    double norm_sq=0;
    for(unsigned int i=0; i<size(); i++)
    {
        norm_sq=norm_sq+pow(at(i), 2);
    }
    double norm_val=sqrt(norm_sq);
    return norm_val;
}
double NumVector::operator*(const NumVector& rhs) const
{
    if(Size()!=rhs.Size())
    {
        throw length_mismatch("Exception length mismatch: Multiplication of vectors of uneven lengths.");
    }
    else if(Size()==0 && rhs.Size()==0)
    {
        throw length_mismatch("Exception length mismatch: Multiplication of empty vectors.");
    }
    else
    {
        double scalar_product=0;
        for(unsigned int i=0; i<size(); i++)
        {
            scalar_product=scalar_product+(at(i)*rhs[i]);
        }
        return scalar_product;
    }
}

