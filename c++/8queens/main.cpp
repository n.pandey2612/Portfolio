#include <iostream>
#include <conio.h>
using namespace std;

class board
{
private:
    int row;
    int column;
    int board1[8][8];
    bool ok;
    bool columns[8];
    bool leftdiagonals[15];
    bool rightdiagonals[15];
public:
    board();
    void initializeboard();
    void setboard();
    void boardplace(int r,int c);
    void boardremove(int r,int c);
    void show();
    bool putboard(int r);
};
board::board()
{
  initializeboard();
}
void board::initializeboard()
{
  for(int i=0;i<8;i++)
  {
      for(int j=0;j<8;j++)
      {
         board1[i][j]=0;
      }
  }
  for(int i=0;i<8;i++)
  {
      columns[i]=true;
  }
  for(int i=0;i<14;i++)
  {
      leftdiagonals[i]=true;
  }
  for(int i=0;i<14;i++)
  {
      rightdiagonals[i]=true;
  }

}
void board::boardplace(int r,int c)
{
    board1[r][c]=1;
    columns[c]=false;
    leftdiagonals[r-c+7]=false;
    rightdiagonals[r+c]=false;
}
void board::boardremove(int r,int c)
{
    board1[r][c]=0;
    columns[c]=true;
    leftdiagonals[r-c+7]=true;
    rightdiagonals[r+c]=true;
}
bool board::putboard(int r)
    {
        for(int c=0;c<8;c++)
        {
            if(columns[c]==true && leftdiagonals[r-c+7]==true && rightdiagonals[r+c]==true)
            {
                boardplace(r,c);
                if(r<7)
                {
                    ok=putboard(r+1);
                    if(ok==true)
                    {
                        return true;
                    }
                    else
                    {
                        boardremove(r,c);
                    }
                }
                else
                {
                    return true;
                }
             }
         }
       return false;
    }
void board::show()
{
    for(int i=0;i<8;i++)
  {
      for(int j=0;j<8;j++)
      {
         cout<<board1[i][j]<<" ";
      }
      cout<<"\n";
  }
}
void board::setboard()
{
    row=0;
    bool success=putboard(row);
    if(success==true)
    {
      show();
    }
    else
    {
        cout<<"\nNo success in placing the queens.\n";
    }

}
int main()
{
    board b1;
    b1.setboard();
    return 0;
}
