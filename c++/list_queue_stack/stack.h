#include "list.h"
#ifndef STACK
#define STACK

//struct node;
template <class T> class stack: public list<T>
{
  protected:
     int n;
     struct node *head, *tail ;
  public:
      stack();
      void clear();
      int isEmpty();
      void push(T el);
      void pop();
      T TopEl();
      void show();
      int size();
      void clearstack();
};
#endif

