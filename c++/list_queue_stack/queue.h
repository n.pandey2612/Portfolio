#include<iostream>
#include<conio.h>
#include "list.h"

#ifndef QUEUE
#define QUEUE

using namespace std;

template <class T> class queue: public list<T>
{
      private:
      int n;
      struct node *head, *tail;

  public:
      queue();
      void isEmpty();
      void enqueue(T el);
      void dequeue();
      T firstEl();
      void show();
      int size();
};
#endif // QUEUE
