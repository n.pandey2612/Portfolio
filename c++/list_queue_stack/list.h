#ifndef LIST
#define LIST

//using namespace std;
//template <class T> struct node;
template <class T> class list
{
   protected:
   struct node
    {
      T info;
      node *next;
      node()
      {
       //info=0;
       next=0;
      }
      node(T in)
      {
       info=in;
       next=0;
      }
      //friend class sllist;
    };
   node *head, *tail;
   public:
      list();
      void addtohead(T in1);
      void addtotail(T in1);
      void removefromhead();
      void removefromtail();
      int find(T in1);
      int ifempty();
      void removenode(T in2);
      void insert(node* key, T info_inser);
      void show();
      int sizeoflist();
      T returnheadel();
      //node* gethead();
      //node* gettail();
};
#endif

