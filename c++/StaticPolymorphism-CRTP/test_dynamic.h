# ifndef TESTDCLASS_H
# define TESTDCLASS_H
#include "quadratures_dynamic.h"
#include "functions_dynamic.h"
# include <iostream>
# include <cmath>


class test_dynamic_inline {
private:
	double  a, b;
public:
	test_dynamic_inline (double  a_, double b_) : a(a_),b(b_) {};

	double test_result(DI::Functor &f, DI::QuadRule q)
	{
		return q(f, a, b);
	};
};

class test_dynamic {
private:
	double  a, b;
public:
	test_dynamic (double  a_, double b_) : a(a_),b(b_) {};

	double test_result(D::Functor &f, D::QuadRule q)
	{
		return q(f, a, b);
	};
};

#endif
