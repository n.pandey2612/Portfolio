# ifndef QUADSCLASS_H
# define QUADSCLASS_H
# include <cmath>
#include "functions_static.h"

namespace SI
{
template<typename T>
class QuadRule
{

public:
    template<typename T1>
	double operator()(Functor<T1>& f, double a, double b)
	{
	    return static_cast<T*>(this)-> operator ()(f, a, b);
	}

};

//implementation of the Trapeziodal Rule
class Trapezoidal : public QuadRule<Trapezoidal>
{
    private:
    size_t n=100;
public:
    template<typename T1>
	inline double operator()(Functor<T1>& f, double a, double b)
	{
		double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+f(b))/2;
			}
        return h* result;
	}
};

//implementation of the Simpson Rule
class Simpson : public QuadRule<Simpson>
{
    private:
    size_t n=100;
public:
    template<typename T1>
	inline double operator()(Functor<T1>& f, double a, double b)
	{
	    double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+4*f((a+b)/2)+f(b))/6;
			}
			return h* result;

	}
};
}
namespace S
{
template<typename T>
class QuadRule
{
public:
    template<typename T1>
	double operator()(Functor<T1>& f, double a, double b)
	{
	    return static_cast<T*>(this)-> operator ()(f, a, b);
	}
};

//implementation of the Trapeziodal Rule
class Trapezoidal : public QuadRule<Trapezoidal>
{
    private:
    size_t n=100;
public:
    template<typename T1>
    double operator()(Functor<T1>& f, double a, double b)
	{
	    double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+f(b))/2;
			}
        return h* result;

	}
};

//implementation of the Simpson Rule
class Simpson : public QuadRule<Simpson>
{
    private:
    size_t n=100;
public:
    template<typename T1>
    double operator()(Functor<T1>& f, double a, double b)
	{
	    double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+4*f((a+b)/2)+f(b))/6;
			}
			return h* result;

	}
};

}

#endif
