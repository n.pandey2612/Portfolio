# ifndef QUADDCLASS_H
# define QUADDCLASS_H
# include <cmath>
#include "functions_dynamic.h"

namespace DI
{
class QuadRule
{
public:
	virtual inline double operator()(Functor& f, double a, double b){};
	virtual ~ QuadRule () = default;
};

//implementation of the Trapeziodal Rule
class Trapezoidal : public QuadRule
{
    private:
    size_t n=100;
public:
	inline double operator()(Functor& f, double a, double b) override
	{
		double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+f(b))/2;
			}
        return h* result;
	}
};

//implementation of the Simpson Rule
class Simpson : public QuadRule
{
    private:
    size_t n=100;
public:
	inline double operator()(Functor& f, double a, double b) override
	{
		double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+4*f((a+b)/2)+f(b))/6;
			}
			return h* result;

	}
};
}
namespace D
{
class QuadRule
{
public:
	virtual double operator()(Functor& f, double a, double b){};
	virtual ~ QuadRule () = default;
};

//implementation of the Trapeziodal Rule
class Trapezoidal : public QuadRule
{
    private:
    size_t n=100;
public:
	double operator()(Functor& f, double a, double b) override
	{
		double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+4*f((a+b)/2)+f(b))/6;
			}
			return h* result;

	}
};

//implementation of the Simpson Rule
class Simpson : public QuadRule
{
    private:
    size_t n=100;
public:
	double operator()(Functor& f, double a, double b) override
	{
		double h = (b - a) / n; // length of a single interval
									// compute the integral boxes and sum them
			double result = 0.;
			for (size_t i = 0; i < n; ++i)
			{
				// evaluate function at midpoint and sum integral value
				result += (f(a)+4*f((a+b)/2)+f(b))/6;
			}
			return h* result;
	}
};

}
#endif
