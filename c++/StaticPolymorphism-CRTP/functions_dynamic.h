# ifndef FUNCTORDCLASS_H
# define FUNCTORDCLASS_H
# include <cmath>
# include <vector>


namespace DI{
class Functor
{
public:
	virtual double operator ()(double x) const = 0;
};
class Polynomial :public Functor {
private:
	std::vector<double> a;
public:
	Polynomial (std::vector<double> a_) : a(a_) {}//Passing coefficients as vector.
	inline double operator ()(double x) const override
	{
		double sum=0;
    for(size_t i=0; i < a.size(); i++)
    {
        sum=sum+a[i]*pow(x, i);
    }
    return sum;
	}
};

}

namespace D {
    class Functor
{
public:
	virtual double operator ()(double x) const = 0;
};
	class Polynomial :public Functor {
    private:
	std::vector<double> a;
    public:
	Polynomial (std::vector<double> a_) : a(a_) {}//Passing coefficients as vector.
	double operator ()(double x) const override
	{
		double sum=0;
    for(size_t i=0; i < a.size(); i++)
    {
        sum=sum+a[i]*pow(x, i);
    }
    return sum;
	}

};
}
#endif
