# ifndef TESTSCLASS_H
# define TESTSCLASS_H
#include "quadratures_static.h"
#include "functions_static.h"
# include <iostream>
# include <cmath>


class test_static_inline
{
private:
	double  a, b;
public:
	test_static_inline (double  a_, double b_) : a(a_),b(b_){};
    template <typename T, typename T1>
	double test_result(SI::Functor<T1> &f, SI::QuadRule<T> q)
	{
		return q(f, a, b);
	};
};

class test_static {
private:
	double  a, b;
public:
	test_static (double  a_, double b_) : a(a_),b(b_) {};
    template <typename T, typename T1>
	double test_result(S::Functor<T1> &f, S::QuadRule<T> q)
	{
		return q(f, a, b);
	};
};

#endif
