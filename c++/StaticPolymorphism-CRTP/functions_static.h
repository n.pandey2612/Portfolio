# ifndef FUNCTORSCLASS_H
# define FUNCTORSCLASS_H
# include <cmath>
# include <vector>
# include <iostream>


namespace SI
   {
       template <class T>
	class Functor
	{
	    public:
	    // Curiously_recurring_template_pattern
		double operator ()(double x)  {
			return static_cast<T*>(this)-> operator ()(x);
		}

	};

	class Polynomial :public Functor<Polynomial> {
	private:
		std::vector<double> a;
	public:
		Polynomial (std::vector<double> a_) : a(a_) {}//Passing coefficients as vector.
		inline double operator ()(double x) const
	     {
		  double sum=0;
          for(size_t i=0; i < a.size(); i++)
          {
           sum=sum+a[i]*pow(x, i);
          }
          return sum;
         }
	};
}

namespace S{
    template <class T>
	class Functor
	{
	    public:
	    // Curiously_recurring_template_pattern
		double operator ()(double x)  {
			return static_cast<T*>(this)-> operator ()(x);
		}

	};
	class Polynomial :public Functor<Polynomial> {
	private:
		std::vector<double> a;
	public:
		Polynomial (std::vector<double> a_) : a(a_) {}//Passing coefficients as vector.
		double operator ()(double x) const
	     {
		  double sum=0;
          for(size_t i=0; i < a.size(); i++)
          {
           sum=sum+a[i]*pow(x, i);
          }
          return sum;
         }
	};


}
#endif
