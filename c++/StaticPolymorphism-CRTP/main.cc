#include <iostream>
#include "quadratures_dynamic.h"
#include "functions_dynamic.h"
#include "quadratures_static.h"
#include "functions_static.h"
#include "test_dynamic.h"
#include "test_static.h"
#include <chrono>

int main()
{
    double result;
	double l = -3., r = 13.;
	std::vector<double> a= {5, 0, 2};
	DI::Polynomial polydi(a);
	D::Polynomial polyd(a);
	SI::Polynomial polysi(a);
	S::Polynomial polys(a);

	DI::Trapezoidal trapdi;
	D::Trapezoidal trapd;
	DI::Simpson simpdi;
    D::Simpson simpd;

	SI::Trapezoidal trapsi;
	S::Trapezoidal traps;
	SI::Simpson simpsi;
	S::Simpson simps;


	test_dynamic_inline testdi(l, r);
	test_dynamic testd(l, r);
	test_static_inline testsi(l, r);
	test_static tests(l, r);

	const auto startDIt = std::chrono::high_resolution_clock::now();
	result=testdi.test_result(polydi, trapdi);
	const auto endDIt = std::chrono::high_resolution_clock::now();
	const auto msDIt = std::chrono::duration_cast<std::chrono::nanoseconds>(endDIt - startDIt).count() / 1000.;
	std::cout << "Dynamic inline Trapezoidal Integral for Dynamic inline Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msDIt << " ms" << std::endl;

	const auto startDIs = std::chrono::high_resolution_clock::now();
	result=testdi.test_result(polydi, simpdi);
	const auto endDIs = std::chrono::high_resolution_clock::now();
	const auto msDIs = std::chrono::duration_cast<std::chrono::nanoseconds>(endDIs - startDIs).count() / 1000.;
	std::cout << "Dynamic inline Simpson Integral for Dynamic inline Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msDIs << " ms" << std::endl;

	const auto startDt = std::chrono::high_resolution_clock::now();
	result=testd.test_result(polyd, trapd);
	const auto endDt = std::chrono::high_resolution_clock::now();
	const auto msDt = std::chrono::duration_cast<std::chrono::nanoseconds>(endDt - startDt).count() / 1000.;
	std::cout << "Dynamic Trapezoidal Integral for Dynamic Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msDIt << " ms" << std::endl;

	const auto startDs = std::chrono::high_resolution_clock::now();
	result=testd.test_result(polyd, simpd);
	const auto endDs = std::chrono::high_resolution_clock::now();
	const auto msDs = std::chrono::duration_cast<std::chrono::nanoseconds>(endDs - startDs).count() / 1000.;
	std::cout << "Dynamic Simpson Integral for Dynamic Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msDIs << " ms" << std::endl;


	const auto startSIt = std::chrono::high_resolution_clock::now();
	result=testsi.test_result(polysi, trapsi);
	const auto endSIt = std::chrono::high_resolution_clock::now();
	const auto msSIt = std::chrono::duration_cast<std::chrono::nanoseconds>(endSIt - startSIt).count() / 1000.;
	std::cout << "Static inline Trapezoidal Integral for Static inline Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msSIt << " ms" << std::endl;

	const auto startSIs = std::chrono::high_resolution_clock::now();
	result=testsi.test_result(polysi, simpsi);
    const auto endSIs = std::chrono::high_resolution_clock::now();
	const auto msSIs = std::chrono::duration_cast<std::chrono::nanoseconds>(endSIs - startSIs).count() / 1000.;
	std::cout << "Static inline Simpson Integral for Static inline Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msSIs << " ms" << std::endl;

	const auto startSt = std::chrono::high_resolution_clock::now();
	result=tests.test_result(polys, traps);
    const auto endSt = std::chrono::high_resolution_clock::now();
	const auto msSt = std::chrono::duration_cast<std::chrono::nanoseconds>(endSt - startSt).count() / 1000.;
	std::cout << "Static Trapezoidal Integral for Static Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msSt << " ms" << std::endl;

	const auto startSs = std::chrono::high_resolution_clock::now();
	result=tests.test_result(polys, simps);
    const auto endSs = std::chrono::high_resolution_clock::now();
	const auto msSs = std::chrono::duration_cast<std::chrono::nanoseconds>(endSs - startSs).count() / 1000.;
	std::cout << "Static Simpson Integral for Static Polynomial : " << result << std::endl;
	std::cout << "Time consumed: " << msSs << " ms" << std::endl;

}











