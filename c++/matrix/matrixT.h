#include<vector>
#include<iomanip>
#include<iostream>
#include<cstdlib>
#include "Rational.h"

template <typename T>
class MatrixT
{
public:
    void resize(int numRows_, int numCols_)
    {
    entries.resize(numRows_);
    for (size_t i = 0; i < entries.size(); ++i)
        entries[i].resize(numCols_);
    numRows = numRows_;
    numCols = numCols_;
    }
    void resize(int numRows_, int numCols_, T value)
    {
    entries.resize(numRows_);
    for (size_t i = 0; i < entries.size(); ++i) {
        entries[i].resize(numCols_);
        for (size_t j = 0; j < entries[i].size(); ++j)
            entries[i][j] = value;
    }
    numRows = numRows_;
    numCols = numCols_;
    }

    // access elements
    T& operator()(int i, int j)
    {
    if (i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if (j < 0 || j >= numCols)
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is [0:" << numCols-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return entries[i][j];
    }

    T  operator()(int i, int j) const
    {
    if ( i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if (j < 0 || j >= numCols)
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is [0:" << numCols-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return entries[i][j];
    }

    std::vector<T>& operator[](int i)
    {
    if (i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return entries[i];
    }

    const std::vector<T>& operator[](int i) const
    {
    if (i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return entries[i];
    }

    // arithmetic functions
    template<typename T_>MatrixT<T>& operator*=(T_ x)
    {
    for (int i = 0; i < numRows; ++i)
        for (int j = 0; j < numCols; ++j)
            entries[i][j] *= x;
    return *this;
    }

    MatrixT<T>& operator+=(const MatrixT<T>& x)
    {
    if (x.numRows != numRows || x.numCols != numCols)
    {
        std::cerr << "Dimensions of matrix a (" << numRows
                  << "x" << numCols << ") and matrix x ("
                  << x.numRows << "x" << x.numCols << ") do not match!";
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < numRows; ++i)
        for (int j = 0;j < numCols; ++j)
            entries[i][j] += x[i][j];
    return *this;
    }

    std::vector<T> solve(std::vector<T> b) const
    {
    std::vector<std::vector<T> > a(entries);
    for (int m = 0; m < numRows-1; ++m)
        for (int i = m+1; i < numRows; ++i)
        {
            T q = a[i][m]/a[m][m];
            a[i][m] = 0.;
            for (int j= m+1; j < numRows; ++j)
                a[i][j] = a[i][j] - q * a[m][j];
            b[i] -= q*b[m];
        }
    std::vector<T> x(b);
    x.back() /= a[numRows-1][numRows-1];
    for (int i = numRows-2; i >= 0; --i)
    {
        for (int j = i+1; j < numRows; ++j)
            x[i] -= a[i][j] * x[j];
        x[i] /= a[i][i];
    }
    return(x);
    }

    // output
    void print() const
    {
    std::cout << "(" << numRows << "x";
    std::cout << numCols << ") matrix:" << std::endl;
    for (int i = 0; i < numRows; ++i)
    {
        std::cout << std::setprecision(3);
        for (int j = 0; j < numCols; ++j)
            std::cout << std::setw(5) << entries[i][j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
    }
    int rows() const
    {
        return numRows;
    }
    int cols() const
    {
        return numCols;
    }

    MatrixT(int numRows_, int numCols_) :
            entries(numRows_), numRows(numRows_), numCols(numCols_)
    {
        for (int i = 0; i < numRows_; ++i)
            entries[i].resize(numCols_);
    };

    MatrixT(int dim) : MatrixT(dim,dim)
    {};

    MatrixT(int numRows_, int numCols_, T value)
    {
        resize(numRows_,numCols_,value);
    };

    MatrixT(std::vector<std::vector<T> > a)
    {
        entries = a;
        numRows = a.size();
        if (numRows > 0)
            numCols = a[0].size();
        else
            numCols = 0;
    }

    MatrixT(const MatrixT<T>& b)
    {
        entries = b.entries;
        numRows = b.numRows;
        numCols = b.numCols;
    }


private:
    std::vector<std::vector<T> > entries;
    int numRows = 0;
    int numCols = 0;
};

template <typename T>
std::vector<T> operator*(const MatrixT<T>& a,
                              const std::vector<T>& x){
    if (x.size() != a.cols())
    {
        std::cerr << "Dimensions of vector " << x.size();
        std::cerr << " and matrix " << a.cols() << " do not match!";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    std::vector<T> y(a.rows());
    for (int i = 0; i < a.rows(); ++i)
    {
        y[i] = 0.;
        for (int j = 0; j < a.cols(); ++j)
            y[i] += a[i][j] * x[j];
    }
    return y;
}

 //Multiplication of Matrix with T data type with a variable of T_data type
template <typename T, typename T_>
MatrixT<T> operator*(const MatrixT<T>& a, T_ x){
    MatrixT<T> output(a);
    output *= x;
    return output;
}

template <typename T, typename T_>
MatrixT<T> operator*(T_ x, const MatrixT<T>& a){
    MatrixT<T> output(a);
    output *= x;
    return output;
}

template <typename T>
MatrixT<T> operator+(const MatrixT<T>& a, const MatrixT<T>& b){
    MatrixT<T> output(a);
    output += b;
    return output;
}

std::ostream& operator<< (std::ostream &str, const RationalNum &num){
    str << num.get_numerator() << "/" << num.get_denominator();
}
