#include "Rational.h"
#include <iostream>



using namespace std;

RationalNum::RationalNum()
{
    numerator=1;
    denominator=1;
}
RationalNum::RationalNum(int n)
{
    numerator=n;
    denominator=1;
}
RationalNum::RationalNum(int n, int d)
{
    numerator=n;
    denominator=d;
}
int RationalNum::get_numerator() const
{
    return numerator;
}
int RationalNum::get_denominator() const
{
    return denominator;
}

int RationalNum::gcd(int n, int d)//Using Euclidean's algorithm
{
    if(d==0)
    {
        return n;
    }
    else
    {
        return gcd(d, n%d);
    }
}
void RationalNum::reduce()
{
    int g;
    g=gcd(this->numerator, this->denominator);
    numerator=(this->numerator)/g;
    denominator=(this->denominator)/g;
    if (denominator<0)
    {
        numerator=-numerator;
        denominator=-denominator;
    }
}
void RationalNum::output()
{
    cout<<numerator<<"/"<<denominator;
}
void RationalNum::operator+=(const RationalNum& rhs)
{
    numerator=(this->numerator)*rhs.denominator + rhs.numerator*(this->denominator);
    denominator=(this->denominator)*rhs.denominator;
    reduce();
}
void RationalNum::operator-=(const RationalNum& rhs)
{
    numerator=(this->numerator)*rhs.denominator - rhs.numerator*(this->denominator);
    denominator=(this->denominator)*rhs.denominator;
    reduce();
}
void RationalNum::operator*=(const RationalNum& rhs)
{
    numerator=(this->numerator)*rhs.numerator;
    denominator=(this->denominator)*rhs.denominator;
    reduce();
}
void RationalNum::operator/=(const RationalNum& rhs)
{
    numerator=(this->numerator)*rhs.denominator;
    denominator=(this->denominator)*rhs.numerator;
    reduce();
}
void RationalNum::operator=(const RationalNum& rhs)
{
    numerator=rhs.numerator;
    denominator=rhs.denominator;
}
RationalNum operator+(RationalNum lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=lhs;
    temp+=rhs;
    return temp;
}
RationalNum operator+(int lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=RationalNum(lhs);
    temp+=rhs;
    return temp;
}
RationalNum operator+(RationalNum lhs, int rhs)
{
    return rhs+lhs;
}

RationalNum operator-(RationalNum lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=lhs;
    temp-=rhs;
    return temp;
}
RationalNum operator-(int lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=RationalNum(lhs);
    temp-=rhs;
    return temp;
}
RationalNum operator-(RationalNum lhs, int rhs)
{
    RationalNum temp;
    temp=lhs;
    temp-=RationalNum(rhs);
    return temp;
}
RationalNum operator*(RationalNum lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=lhs;
    temp*=rhs;
    return temp;
}
RationalNum operator*(int lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=RationalNum(lhs);
    temp*=rhs;
    return temp;
}
RationalNum operator*(RationalNum lhs, int rhs)
{
    return rhs*lhs;
}
RationalNum operator/(RationalNum lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=lhs;
    temp/=rhs;
    return temp;
}
RationalNum operator/(int lhs, RationalNum rhs)
{
    RationalNum temp;
    temp=RationalNum(lhs);
    temp/=rhs;
    return temp;
}
RationalNum operator/(RationalNum lhs, int rhs)
{
    RationalNum temp;
    temp=lhs;
    temp/=RationalNum(rhs);
    return temp;
}