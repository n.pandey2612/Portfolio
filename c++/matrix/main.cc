#include <iostream>
#include <complex>
#include "matrixT.h"
#include "Rational.cc"

template <typename T>
void test() {
    MatrixT<T> A(4, 6, 0.);
    for (int i = 0; i < A.rows(); ++i)
        A[i][i] = 2.;
    for (int i = 0; i < A.rows() - 1; ++i)
        A[i + 1][i] = A[i][i + 1] = -1.;
    MatrixT<T> B(6, 4, 0.);
    for (int i = 0; i < B.cols(); ++i)
        B[i][i] = 2.;
    for (int i = 0; i < B.cols() - 1; ++i)
        B[i + 1][i] = B[i][i + 1] = -1.;
// print matrix
    A.print();
    B.print();
    MatrixT<T> C(A);

    A.print();
    A = C + A;
    A.print();
    const MatrixT<T> D(A);
    std::cout << "Element 1,1 of D is " << D(1, 1) << std::endl;
    std::cout << std::endl;
    A.resize(5, 5, 0);
    for (int i = 0; i < A.rows(); ++i)
        A(i, i) = 2;
    for (int i = 0; i < A.rows() - 1; ++i)
        A(i + 1, i) = A(i, i + 1) = -1;
// define vector b
    std::vector<T> b(5);
    b[0] = b[4] = 5;
    b[1] = b[3] = -4;
    b[2] = 4;
    std::vector<T> x = A * b;
    std::cout << "A*b = ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    std::cout << std::endl;
// solve
    x = A.solve(b);
    A.print();
    std::cout << "The solution with the ordinary Gauss Elimination is: ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    C=4.3*A;
    std::cout<<"Multiplication of above 5x5 matrix with float 4.3: "<<std::endl;
    C.print();
    C=2*A;
    std::cout<<"Multiplication of above 5x5 matrix with int 2: "<<std::endl;
    C.print();
    /*
    C=RationalNum(2, 3)*A;
    std::cout<<"Multiplication of above 5x5 matrix with rational 2/3: "<<std::endl;
    A.print();
    C=complex<double>(2, 3)*A;
    std::cout<<"Multiplication of above 5x5 matrix with complex 2+3i: "<<std::endl;
    A.print();
    */

}

void testRational() {
    MatrixT<RationalNum> A(4, 6, 0.);
    for (int i = 0; i < A.rows(); ++i)
        A[i][i] = RationalNum(2,3);
    for (int i = 0; i < A.rows() - 1; ++i){
        A[i + 1][i] = RationalNum(-1.);
        A[i][i + 1] = RationalNum(-1.);}

    MatrixT<RationalNum> B(6, 4, 0.);
    for (int i = 0; i < B.cols(); ++i)
        B[i][i] = RationalNum(2,3);
    for (int i = 0; i < B.cols() - 1; ++i){
        B[i + 1][i] = RationalNum(-1.);
        B[i][i + 1] = RationalNum(-1.);
    }

// print matrix
    A.print();
    B.print();
    MatrixT<RationalNum> C(A);

    A.print();
    A = C + A;
    A.print();
    const MatrixT<RationalNum> D(A);
    std::cout << "Element 1,1 of D is " << D(1, 1) << std::endl;
    std::cout << std::endl;
    A.resize(5, 5, 0);
    for (int i = 0; i < A.rows(); ++i)
        A(i, i) = RationalNum(2);
    for (int i = 0; i < A.rows() - 1; ++i) {
        A(i + 1, i) = RationalNum(-1.);
        A(i, i + 1) = RationalNum(-1.);
    }
// define vector b
    std::vector<RationalNum> b(5);
    b[0] = RationalNum(5);
    b[4] = RationalNum(5);
    b[1] = RationalNum(-4.);
    b[3] = RationalNum(-4.);
    b[2] = RationalNum(4.);
    std::vector<RationalNum> x = A * b;
    std::cout << "A*b = ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    std::cout << std::endl;
// solve
    x = A.solve(b);
    A.print();
    std::cout << "The solution with the ordinary Gauss Elimination is: ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    C=4.3*A;
    std::cout<<"Multiplication of above 5x5 matrix with float 4.3: "<<std::endl;
    C.print();
    C=2*A;
    std::cout<<"Multiplication of above 5x5 matrix with int 2: "<<std::endl;
    C.print();
}

void testComplex(){
    MatrixT<complex<double>> A(4, 6, 0.);
    for (int i = 0; i < A.rows(); ++i)
        A[i][i] = complex<double>(2.,3.);
    for (int i = 0; i < A.rows() - 1; ++i){
        A[i + 1][i] = complex<double>(-1.,1.);
        A[i][i + 1] = complex<double>(-1.,1.);}

    MatrixT<complex<double>> B(6, 4, 0.);
    for (int i = 0; i < B.cols(); ++i)
        B[i][i] = complex<double>(2.,3.);
    for (int i = 0; i < B.cols() - 1; ++i){
        B[i + 1][i] = complex<double>(-1.,1.);
        B[i][i + 1] = complex<double>(-1.,1.);
    }

// print matrix
    A.print();
    B.print();
    MatrixT<complex<double>> C(A);

    A.print();
    A = C + A;
    A.print();
    const MatrixT<complex<double>> D(A);
    std::cout << "Element 1,1 of D is " << D(1, 1) << std::endl;
    std::cout << std::endl;
    A.resize(5, 5, 0.);
    for (int i = 0; i < A.rows(); ++i)
        A(i, i) = complex<double>(2.,1.);
    for (int i = 0; i < A.rows() - 1; ++i) {
        A(i + 1, i) = complex<double>(-1.,1.);
        A(i, i + 1) = complex<double>(-1.,1.);
    }
// define vector b
    std::vector<complex<double>> b(5);
    b[0] = complex<double>(5.,1.);
    b[4] = complex<double>(5.,1.);
    b[1] = complex<double>(-4.,1.);
    b[3] = complex<double>(-4.,1.);
    b[2] = complex<double>(4.,1.);
    std::vector<complex<double>> x = A * b;
    std::cout << "A*b = ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    std::cout << std::endl;
// solve
    x = A.solve(b);
    A.print();
    std::cout << "The solution with the ordinary Gauss Elimination is: ( ";
    for (size_t i = 0; i < x.size(); ++i)
        std::cout << x[i] << "  ";
    std::cout << ")" << std::endl;
    C=4.3*A;
    std::cout<<"Multiplication of above 5x5 matrix with float 4.3: "<<std::endl;
    C.print();
    C=2*A;
    std::cout<<"Multiplication of above 5x5 matrix with int 2: "<<std::endl;
    C.print();
}
int main()
{
    int dataType;
    label:
    std::cout << "Please enter the number of data type you want to test: \n"
            "1: integer \n"
            "2: rational \n"
            "3: float \n"
            "4: complex numbers \n";
    std::cin >> dataType;
    switch (dataType){
        case 1:
            test<int>();
            break;
        case 2:
            testRational();
            break;
        case 3:
            test<float>();
            break;
        case 4:
            testComplex();
            break;
        default:
        std::cout << "The number you entered is invalid" << std::endl;
    }
    char choice;
    std::cout << "Do you wish to continue? y/n" <<std::endl;
    std::cin>>choice;
    if(choice=='y')
    {
        goto label;
    }
    else
    {
        goto endloop;
    }
    endloop:
    return 0;
}
