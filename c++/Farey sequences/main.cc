#include "Rational_num.cc"
#include <iostream>
=======
>>>>>>> 5d391b2fad7f2e9480a901646eb41635be402753
#include <list>

using namespace std;

void print_list(list<RationalNum> myList)
{
    cout<<"(";
        for(auto itp=myList.begin(); itp!=myList.end(); itp++)//iterating through the linked list
        {
            RationalNum R3;
            R3=*itp;
            R3.output();
            auto itp1=itp;
            itp1++;
            if(itp1!=myList.end())
            {
                cout<<", ";
            }
        }
        cout<<")"<<"\n";
}
list<RationalNum> farey_list;
void Farey(int N)
{
    if(N==1)
    {
        RationalNum R1(0, 1), R2(1, 1);
        farey_list.push_front(R1);
        farey_list.push_back(R2);
    }
    else
    {
        Farey(N-1);
        for(auto it=next(farey_list.begin()); it!=farey_list.end(); it++)//iterating list starting from second element
        {

            RationalNum prev1, current;
            current= *it;
            auto it1= prev(it);
            prev1= *it1;
            if(prev1.get_denominator() + current.get_denominator()== N)
            {
                RationalNum R(prev1.get_numerator()+current.get_numerator(), prev1.get_denominator()+current.get_denominator());
                farey_list.insert(it, R);
            }
         }
    }
    print_list(farey_list);
}

int main()
{
    int N=6;
    cout<<"Solution 2\n";
    cout<<"The Farey sequences upto order 6:\n";
    Farey(N);
    return 0;
}
