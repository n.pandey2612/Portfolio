#ifndef RATIONAL_NUM_H
#define RATIONAL_NUM_H

using namespace std;

class RationalNum
    {
    private:
        int numerator;
        int denominator;
    public:
        RationalNum();
        RationalNum(int n);
        RationalNum(int n, int d);
        int get_numerator();
        int get_denominator();
        int gcd(int n, int d);
        void reduce();// Function to reduce fractions using gcd
        void output();
        void operator+=(const RationalNum& rhs);
        void operator-=(const RationalNum& rhs);
        void operator*=(const RationalNum& rhs);
        void operator/=(const RationalNum& rhs);
        void operator=(const RationalNum& rhs);
    };
//Supplement functions for various operations on Rational Numbers
RationalNum operator+(RationalNum lhs, RationalNum rhs);
RationalNum operator+(int lhs, RationalNum rhs);
RationalNum operator+(RationalNum lhs, int rhs);

RationalNum operator-(RationalNum lhs, RationalNum rhs);
RationalNum operator-(int lhs, RationalNum rhs);
RationalNum operator-(RationalNum lhs, int rhs);

RationalNum operator*(RationalNum lhs, RationalNum rhs);
RationalNum operator*(int lhs, RationalNum rhs);
RationalNum operator*(RationalNum lhs, int rhs);

RationalNum operator/(RationalNum lhs, RationalNum rhs);
RationalNum operator/(int lhs, RationalNum rhs);
RationalNum operator/(RationalNum lhs, int rhs);
#endif
