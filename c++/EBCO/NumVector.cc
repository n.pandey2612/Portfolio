#include<iostream>
#include<cmath>
#include "NumVector.h"
using namespace std;

NumVector::NumVector(): vector<double>(){}
NumVector::NumVector(int n): vector<double>(n){}
double& NumVector::operator[](unsigned int i)
{
    if(this->size()<=i || i < 0)
    {
        throw string("Illegal access!"); //throw an exception when index is out of bounds
    }
    else
    {
        return vector<double>::operator[](i);
    }
}
const double& NumVector::operator[](unsigned int i) const
{
    if(this->size()<=i || i < 0)
    {
        throw string("Illegal access!");
    }
    else
    {
        return vector<double>::operator[](i);
    }
}
double NumVector::norm() const
{
    double norm_sq=0;
    for(unsigned int i=0; i<size(); i++)
    {
        norm_sq=norm_sq+pow(at(i), 2);
    }
    double norm_val=sqrt(norm_sq);
    return norm_val;
}
