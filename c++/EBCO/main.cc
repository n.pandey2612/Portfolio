//#include "NumVector.cc" //NumVector class inherited from vector class
#include "NumVector1.cc" //Numvector class having vector as component
#include<iostream>

int main()
{
//NumVector v(3);
NumVector1 v(3);
v[0]=1; v[1]=3, v[2]=4;
//const NumVector& w=v;
const NumVector1& w=v;
std::cout<<"norm is "<<w.norm()<<std::endl;
std::cout<<"vector is ["<<w[0]<<", "<<w[1]<<", "<<w[2]<<"]"<<std::endl;
return 0;
}


