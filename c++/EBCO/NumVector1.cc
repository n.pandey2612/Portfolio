#include<iostream>
#include<cmath>
#include "NumVector1.h"
using namespace std;

NumVector1::NumVector1()
{

}
NumVector1::NumVector1(int n)
{
    v.resize(n);
}
double& NumVector1::operator[](unsigned int i)
{
    if(v.size()<=i || i < 0)
    {
        throw string("Illegal access!"); //throw an exception when index is out of bounds
    }
    else
    {
        return v[i];
    }
}
const double& NumVector1::operator[](unsigned int i) const
{
    if(v.size()<=i || i < 0)
    {
        throw string("Illegal access!");
    }
    else
    {
        return v[i];
    }
}
double NumVector1::norm() const
{
    double norm_sq=0;
    for(unsigned int i=0; i<v.size(); i++)
    {
        norm_sq=norm_sq+pow(v[i], 2);
    }
    double norm_val=sqrt(norm_sq);
    return norm_val;
}

