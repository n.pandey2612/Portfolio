#ifndef NUMVECTOR1
#define NUMVECTOR1
#include<vector>
using namespace std;

class NumVector1
{
private:
    vector<double> v;
public:
    NumVector1();
    NumVector1(int n);
    double& operator[](unsigned int i);
    const double& operator[](unsigned int i) const;
    double norm() const;
};


#endif
