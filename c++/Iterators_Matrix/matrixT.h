#ifndef MATRIXT
#define MATRIXT

#include<vector>
#include<iomanip>
#include<iostream>
#include<cstdlib>

template <typename T>
class MatrixT
{
private:
    unsigned int numRows=0;
    unsigned int numCols=0;
    class Row;
    class RowIterator;
    class ColIterator;
    std::vector<Row> mat;
    RowIterator myRowIt{mat};
    class Row
    {
        private:
            std::vector<T> row_vec;
            size_t cols;
            ColIterator col_it{row_vec};
        public:
            void resize(size_t c)
            {
                cols = c;
                row_vec.resize(c);
                for(size_t j = 0; j < cols; j++)
                {
                    row_vec[j]=0.;
                }
            }
            void resize(size_t c, T value)
            {
                cols = c;
                row_vec.resize(c);
                for(size_t j = 0; j < cols; j++)
                {
                    row_vec[j]=value;
                }
            }
            T& operator[](size_t i)
            {
               if (i < 0 || i >= cols)
            {
                std::cerr << "Illegal col index " << i;
                std::cerr << " valid range is [0:" << cols-1 << "]";
                std::cerr << std::endl;
                exit(EXIT_FAILURE);
            }
            return row_vec[i];
            }

            T& operator[](size_t i) const
            {
               if (i < 0 || i >= cols)
            {
                std::cerr << "Illegal col index " << i;
                std::cerr << " valid range is [0:" << cols-1 << "]";
                std::cerr << std::endl;
                exit(EXIT_FAILURE);
            }
            return row_vec[i];
            }

            ColIterator begin()
            {
               col_it.reset();
               return col_it;
            }

            ColIterator end()
            {
               col_it.reset();
               for(size_t i=0; i<cols; i++)
               {
                  ++col_it;
               }
               return col_it;
            }

      };
     class ColIterator
     {
      public:
      ColIterator(std::vector<T>& r_): r(r_){};
      ColIterator& operator++() // move to next entry
      {
          curr_col=curr_col+1;
          return *this;
      }
      bool operator==(ColIterator rhs) const // comparison of iterators
      {
          if(col()==rhs.col())
          {
              return true;
          }
          else
            return false;
      }
      T& operator*() // access to current entry
      {
          return r[curr_col];
      }
      const T& operator*() const // as above, but const
      {
          return r[curr_col];
      }
      size_t col() const // number of current entry
      {
          return curr_col;
      }
      ColIterator& operator=(ColIterator other)
      {
        std::swap(this->curr_col, other.curr_col);
        return *this;
      }
      void reset()
      {
          curr_col=0;
      }
      private:
        size_t curr_col=0;
        std::vector<T>& r;
    };

    class RowIterator
    {
     public:
      RowIterator(std::vector<Row>& m_): m(m_){};
      RowIterator& operator++() // move to next row
      {
          curr_row=curr_row+1;
          return *this;
      }
      bool operator==(RowIterator rhs) const // comparison of iterators
      {
          if(row()==rhs.row())
          {
              return true;
          }
          else
            return false;
      }
      Row& operator*() // access to current row
      {
          return m[curr_row];
      }
      const Row& operator*() const // as above, but const
      {
          return m[curr_row];
      }
      size_t row() const // number of current row
      {
          return curr_row;
      }
      RowIterator& operator=(RowIterator other)
      {
        std::swap(this->curr_row, other.curr_row);
        return *this;
      }
      void reset()
      {
          curr_row=0;
      }
     private:
        size_t curr_row=0;
        std::vector<Row>& m;
    } ;

public:
    RowIterator& begin()
    {
        myRowIt.reset();
        return myRowIt;
    }

    RowIterator& end()
    {
        myRowIt.reset();
        for(size_t i=0; i<numRows; i++)
        {
            ++myRowIt;
        }
        return myRowIt;
    }
    // access elements
    T& operator()(size_t i, size_t j)
    {
    if (i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if (j < 0 || j >= numCols)
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is [0:" << numCols-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return mat[i][j];
    }

    T&  operator()(size_t i, size_t j) const
    {
    if ( i < 0 || i >= numRows)
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is [0:" << numRows-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if (j < 0 || j >= numCols)
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is [0:" << numCols-1 << "]";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return mat[i][j];
    }

    MatrixT(size_t numRows_, size_t numCols_) : numRows(numRows_), numCols(numCols_)
    {
        mat.resize(numRows);
        for(size_t i=0; i<numRows; i++)
        {
            mat[i].resize(numCols);
        }
    }
    MatrixT(size_t numRows_, size_t numCols_, T value) : numRows(numRows_), numCols(numCols_)
    {
        mat.resize(numRows);
        for(size_t i=0; i<numRows; i++)
        {
            mat[i].resize(numCols, value);
        }
    }
    size_t rows() const
    {
        return numRows;
    }
    size_t cols() const
    {
        return numCols;
    }
    // arithmetic functions
    template<typename T_>MatrixT<T>& operator*=(T_ x)
    {
    for (RowIterator itr = this->begin(); !(itr==this->end()); ++itr)
    {
        Row row=*itr;
        for (ColIterator itc = row.begin(); !(itc==row.end()); ++itc)
        {
             (*itc)=(*itc) * x;
        }
    }
    return *this;
    }

    MatrixT<T>& operator+=(const MatrixT<T>& x)
    {
    if (x.numRows != numRows || x.numCols != numCols)
    {
        std::cerr << "Dimensions of matrix a (" << numRows
                  << "x" << numCols << ") and matrix x ("
                  << x.numRows << "x" << x.numCols << ") do not match!";
        exit(EXIT_FAILURE);
    }
    for (RowIterator itr = this->begin(); !(itr==this->end()); ++itr)
    {
        Row row=*itr;
        for (ColIterator itc = row.begin(); !(itc==row.end()); ++itc)
        {
           (*itc)=(*itc) + x(itc.row(), itc.col());
        }
    }
    return *this;
    }
    // output
    void print()
    {
    std::cout << "(" << numRows << "x";
    std::cout << numCols << ") matrix:" << std::endl;
    for (RowIterator itr = this->begin(); !(itr==this->end()); ++itr)
    {
        Row row=*itr;
        std::cout << std::setprecision(3);
        for (ColIterator itc = row.begin(); !(itc==row.end()); ++itc)
            std::cout << std::setw(5) << *itc << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
    }
};

 //Multiplication of Matrix with T data type with a variable of T_data type
template <typename T, typename T_>
MatrixT<T> operator*(const MatrixT<T>& a, T_ x){
    MatrixT<T> output(a);
    output *= x;
    return output;
}

template <typename T, typename T_>
MatrixT<T> operator*(T_ x, const MatrixT<T>& a){
    MatrixT<T> output(a);
    output *= x;
    return output;
}

template <typename T>
MatrixT<T> operator+(const MatrixT<T>& a, const MatrixT<T>& b){
    MatrixT<T> output(a);
    output += b;
    return output;
}

#endif
