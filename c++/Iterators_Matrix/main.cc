#include <iostream>
#include "matrixT.h"

template <typename T>
void test() {
    MatrixT<T> A(4, 6, 0.);
    MatrixT<T> C(4, 6, 0.);
    for (size_t i = 0; i < A.rows(); ++i)
        A(i, i) = 2.;
    for (size_t i = 0; i < A.rows() - 1; ++i)
        A(i + 1, i) = A(i, i + 1) = -1.;
    MatrixT<T> B(6, 4, 0.);
    for (size_t i = 0; i < B.cols(); ++i)
        B(i, i) = 2.;
    for (size_t i = 0; i < B.cols() - 1; ++i)
        B(i + 1, i) = B(i, i + 1) = -1.;
// print matrix
    A.print();
    B.print();
    //MatrixT<T> C(A);
    C=4.3*A;
    std::cout<<"Multiplication of above matrix with double 4.3: "<<std::endl;
    C.print();
}


int main()
{
    test<double>();
}
