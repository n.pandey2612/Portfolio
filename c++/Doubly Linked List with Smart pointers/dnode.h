#ifndef DNODE
#define DNODE
#include<memory>

class dNode
{
private:
    std::weak_ptr<dNode> prev;
    std::shared_ptr<dNode> next;
public:
    int value;
    dNode();
    dNode(int i);
    friend class dList;
    ~dNode();
};

#endif
