#include<iostream>
#include<memory>
#include "dlist.cc"

using namespace std;

void printList (const dList& dlist)
{
    for(shared_ptr<const dNode> n = dlist.first(); n != 0; n = dlist.Next(n))
    {
        cout << n->value << endl;
    }
}
void append (dList& dlist1, const dList& dlist2)
{
    for(shared_ptr<const dNode> n = dlist2.first(); n != 0; n = dlist2.Next(n))
    {
        dlist1.append(n->value);
    }
}

int main ()
{
dList dlist1,dlist2;
dlist1.append(2);
dlist1.append(13);
dlist1.append(20);
dlist2.append(-2);
dlist2.append(-3);
dlist2.append(50);
append(dlist1, dlist2);
int m=dlist1.Max();
dlist1.insert(dlist1.first(), 1);
printList(dlist1);
cout<<"Maximum value in the list is: "<<m<<endl;
return 0;
}
