#ifndef DLIST
#define DLIST
#include "dnode.cc"
#include<memory>

class dList
{
private:
    std::shared_ptr<dNode> head, next;
    mutable int maxVal;
    mutable bool isMaxVal;
public:
    dList ();
   ~dList ();
    std::shared_ptr<dNode> first() const;
    std::shared_ptr<dNode> Next(std::shared_ptr<const dNode> n) const;
    void append (int i);
    void insert (std::shared_ptr<dNode> n, int i);
    void erase (std::shared_ptr<dNode> n);
    int Max() const;
};

#endif
