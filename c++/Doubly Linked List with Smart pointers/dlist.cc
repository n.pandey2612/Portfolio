#include<iostream>
#include<limits>
#include<memory>
#include "dlist.h"

dList::dList()
{
    head=0;
    maxVal=std::numeric_limits<int>::min();
    isMaxVal=false;
}
std::shared_ptr<dNode> dList::first() const
{
    return head;
}
std::shared_ptr<dNode> dList::Next(std::shared_ptr<const dNode> n) const
{
    return n->next;
}
void dList::append (int i)
{
    std::shared_ptr<dNode> temp(new dNode(i));
    if(head==0)
    {
        head=temp;
    }
    else
    {
        std::shared_ptr<dNode> iter;
        for(iter=head; iter->next!=0; iter=iter->next); //setting iter to the last node
        iter->next=temp;
        temp->prev=iter;
    }
    if(i>maxVal)
    {
        maxVal=i;
    }
}
void dList::insert (std::shared_ptr<dNode> n, int i)
{
    std::shared_ptr<dNode> temp(new dNode(i));
    if (n==head)
    {
        std::shared_ptr<dNode> temp1=head;
        head=temp;
        temp->next=temp1;
        temp1->prev=temp;
    }
    else
    {
        std::shared_ptr<dNode> iter;
        for(iter=head; iter->next!=n; iter=iter->next); //setting iter to the node before n
        iter->next=temp;
        temp->next=n;
        n->prev=temp;
        temp->prev=iter;
    }
    if(i>maxVal)
    {
        maxVal=i; //updating cached maxVal if the appended node is maximum 
    }
}
void dList::erase (std::shared_ptr<dNode> n)
{
    std::shared_ptr<dNode> iter;
    if(n==head)
    {
        std::shared_ptr<dNode> temp;
        head=head->next;
    }
    else
    {
        std::shared_ptr<dNode> iter;
        for(iter=head; iter->next!=n; iter=iter->next);
        iter->next=n->next;
        n->next->prev=iter;
    }
    if(n->value==maxVal)
    {
        maxVal=std::numeric_limits<int>::min(); //if the erased node is maximum then setting maxVal again to minus infinity
        isMaxVal=false;
    }

}
int dList::Max() const
{
    if(head!=0 && !isMaxVal) // finding max value while the list is non empty and a request has been made by setting isMaxVal=false
    {
        for(std::shared_ptr<dNode> iter=head; iter!=0; iter=iter->next)
        {
            if(iter->value > maxVal)
            {
                maxVal=iter->value;
            }
        }
        isMaxVal=true;
    }
    return maxVal;
}
dList::~dList(){std::cout<<"destructor is called"<<std::endl;}
