# ifndef INTEGRATOR_H
# define INTEGRATOR_H
# include "quadrature.h"
# include "test_functor.h"
class Integrator
{
public :
virtual double operator () (testFunctor & f, QuadratureRule & q) = 0;
virtual double get_error () =0;
virtual void reset(size_t n_)=0;
virtual ~ Integrator()
{}
};

# endif
