# ifndef SIMPSON_H
# define SIMPSON_H
# include "quadrature.h"
# include "test_functor.h"

class SimpsonRule : public QuadratureRule
{
double q;
public :
SimpsonRule ()
{}
double operator() (testFunctor& f, double l, double r) override
{
    q = f(l) + f(r) + 4*f(0.5*(l+r));
    return (1.0/6.0)*q;
}
};
#endif // SIMPSON_H
