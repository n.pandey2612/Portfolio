# ifndef POLYNOMIAL_H
# define POLYNOMIAL_H
# include <cmath>
# include <vector>
# include "functor.h"
// realization of a polynomial function
class Polynomial : public Functor
{
public :
Polynomial (std::vector<double> a_) : a(a_) //Passing coefficients as vector.
{}
double operator () (double x) override
{
    double sum=0;
    for(size_t i=0; i < a.size(); i++)
    {
        sum=sum+a[i]*pow(x, i);
    }
    return sum;
}
private :
std::vector<double> a;
};
# endif
