# ifndef FUNCTORCLASS_H
# define FUNCTORCLASS_H
// Base class for arbitrary functions with one double parameter
class Functor
{
public :
virtual double operator () ( double x) = 0;
virtual ~ Functor ()
{}
};
# endif
