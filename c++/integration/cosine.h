# ifndef COSINECLASS_H
# define COSINECLASS_H
# include <cmath>
# include "functor.h"
// realization of a function cos (a*x + b)
class Cosine : public Functor
{
public :
Cosine ( double a_ = 1. , double b_ = 0.) : a ( a_ ) , b ( b_ )
{}
double operator () ( double x) override
{
return cos ( a*x + b);
}
private :
double a , b;
};
# endif
