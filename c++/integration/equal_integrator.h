# ifndef EQUALINTEGRATOR_H
# define EQUALINTEGRATOR_H
# include <cmath>
# include <deque>
# include <numeric>
# include "quadrature.h"
# include "test_functor.h"
class equalIntegrator : public Integrator
{
private:
    std::deque<double> approx, error, exact;
    double a, b, l, r, q_result, e_result;
    size_t n;
public:
    equalIntegrator(size_t n_): n(n_)
    {};
    double operator () ( testFunctor & f, QuadratureRule & q)
    {
        f.reset_limits();
        a=f.get_lower();
        b=f.get_upper();
        e_result=f.exactIntegral(); //exact result
        double h=(b-a)/n;
        for ( size_t i = 0; i < n-1; i++)
        {
            l = a+(i)*h;
            r = a+(i+1)*h;
            double a1=q(f, l, r);
            approx.push_back(a1);
            f.integrationInterval(l, r);
            double a2=f.exactIntegral();// exact integration in the subinterval
            exact.push_back(a2);
            error.push_back(fabs(a2-h*a1));
        }
    q_result=h*(std::accumulate(approx.begin(), approx.end(), 0));
    return q_result;
    }
    double get_error()
    {
        return fabs(e_result-q_result); //absolute value of difference between exact and quadrature
    }
    void reset(size_t n_)
    {
        n=n_;
        approx.clear();
        error.clear();
    }
};

# endif
