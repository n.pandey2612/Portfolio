#include <iostream>
#include <string>
#include <cmath>
#include "test_functor.h"
#include "test_func1.h"
#include "test_func2.h"
#include "quadrature.h"
#include "trapezoidalRule.h"
#include "simpsonRule.h"
#include "integrator.h"
#include "equal_integrator.h"
#include <iomanip>

using namespace std;
double EOC(Integrator& i, testFunctor& f, QuadratureRule& q, size_t n)
{
    double e_n, e_2n;
    e_n=i.get_error();//absolute error for n subintervals
    i.reset(2*n); //reset integrator for 2*n subintervals
    i(f, q);
    e_2n=i.get_error();//absolute error for 2*n subintervals
    return (log(e_n/e_2n)/log(2));
}

int main()
{
    size_t n=100;//number of subintervals
    equalIntegrator eq(n);
    TrapezoidalRule t;
    SimpsonRule s;
    test_func1 test1; //f(t)=2t^2 + 5
    test_func2 test2; //f(t)=(t/pi)*sin(t)

    cout<<"correct solution of f(t)=2t^2 + 5: "<<test1.exactIntegral()<<endl;
    cout<<"correct solution of f(t)=(t/pi)*sin(t): "<<test2.exactIntegral()<<"\n"<<endl;

    cout<<"Integration of f(t)=2t^2 + 5 by trapezoidal rule: "<< eq(test1, t)<<endl;
    cout<<"Order of convergence of the error: "<<EOC(eq, test1, t, n)<<"\n"<<endl;
    eq.reset(n);

    cout<<"Integration of f(t)=2t^2 + 5 by simpson's rule: "<< eq(test1, s)<<endl;
    cout<<"Order of convergence of the error: "<<EOC(eq, test1, s, n)<<"\n"<<endl;
    eq.reset(n);

    cout<<"Integration of f(t)=(t/pi)*sin(t) by trapezoidal rule: "<< eq(test2, t)<<endl;
    cout<<"Order of convergence of the error: "<<EOC(eq, test2, t, n)<<"\n"<<endl;
    eq.reset(n);

    cout<<"Integration of f(t)=(t/pi)*sin(t) by simpson's rule: "<< eq(test2, s)<<endl;
    cout<<"Order of convergence of the error: "<<EOC(eq, test2, s, n)<<"\n"<<endl;
    eq.reset(n);


}
