# ifndef QUADRATURERULE_H
# define QUADRATURERULE_H
# include "test_functor.h"

class QuadratureRule
{
public :
virtual double operator () (testFunctor& f, double l, double r) = 0;
virtual ~ QuadratureRule()
{}
};
# endif
