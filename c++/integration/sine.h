# ifndef SINECLASS_H
# define SINECLASS_H
# include <cmath>
# include "functor.h"
// realization of a function sin (a*x + b)
class Sine : public Functor
{
public :
Sine ( double a_ = 1. , double b_ = 0.) : a ( a_ ) , b ( b_ )
{}
double operator () ( double x) override
{
return sin ( a*x + b);
}
private :
double a , b;
};
# endif
