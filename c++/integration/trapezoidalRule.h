# ifndef TRAPEZOIDAL_H
# define TRAPEZOIDAL_H
# include "quadrature.h"
# include "test_functor.h"

class TrapezoidalRule : public QuadratureRule
{
double q;
public :
TrapezoidalRule ()
{}
double operator() (testFunctor& f, double l, double r) override
{
    q = f(l) + f(r);
    return 0.5*q;
}
};
#endif // TRAPEZOIDAL_H
