# ifndef TESTFUNCTORCLASS_H
# define TESTFUNCTORCLASS_H
# include "functor.h"
// Extension of abstract class Functor to use for integration.
class testFunctor: public Functor
{
public :
virtual void integrationInterval(double& l, double& r) const =0;
virtual double exactIntegral() const=0;
virtual double get_lower() const=0;
virtual double get_upper() const=0;
virtual void reset_limits() const=0;
virtual ~ testFunctor ()
{}
};
#endif
