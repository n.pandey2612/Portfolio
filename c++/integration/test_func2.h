# ifndef TESTFUNC2_H
# define TESTFUNC2_H
# include <cmath>
# include "cosine.h"
# include "sine.h"

//realization of f(t)=(t/pi)*sin(t)
const double pi = 3.1416;
class test_func2 : public testFunctor
{
public :
test_func2 ()
{
    a=1.0;
    lower=0.0;
    upper=2*pi;
}
double operator () (double x) override
{
    Sine sine(a);
    return (1/pi)*x*sine(x);
}
void integrationInterval(double& l, double& r) const override
{
    lower=l;
    upper=r;
}
double exactIntegral() const override
{
    Sine sine(a);
    Cosine cosine(a);
    double upper_value=((1.0/pi)*sine(upper)-upper*(1.0/pi)*cosine(upper));
    double lower_value=((1.0/pi)*sine(lower)-lower*(1.0/pi)*cosine(lower));
    return (upper_value-lower_value);
}
double get_lower() const override
{
    return lower;
}
double get_upper() const override
{
    return upper;
}
void reset_limits() const override
{
    lower=0.0;
    upper=2*pi;
}
private :
double a; //coefficients for sine and cosine part
mutable double lower; //lower limit for definite integral
mutable double upper; //upper limit for definite integral
};
# endif
