# ifndef TESTFUNC1_H
# define TESTFUNC1_H
# include <cmath>
# include <vector>
# include "polynomial.h"

//realization of f(t)=2t^2 + 5
class test_func1 : public testFunctor
{
public :
test_func1()
{
    lower=-3.0;
    upper=13.0;
    a = {5, 0, 2};
    b= {0, 5, 0, (2.0/3.0)};
}
double operator () (double x) override
{
    Polynomial poly_func(a);
    return poly_func(x);
}
void integrationInterval(double& l, double& r) const override
{
    lower=l;
    upper=r;
}
double exactIntegral() const override
{
    Polynomial poly_integral(b);
    double upper_value=poly_integral(upper);
    double lower_value=poly_integral(lower);
    return (upper_value-lower_value);
}
double get_lower() const override
{
    return lower;
}
double get_upper() const override
{
    return upper;
}
void reset_limits() const override
{
    lower=-3.0;
    upper=13.0;
}
private :
std::vector<double> a; //coefficients of function
std::vector<double> b; //coefficients of integral
mutable double lower; //lower limit for definite integral
mutable double upper; //upper limit for definite integral
};
# endif
