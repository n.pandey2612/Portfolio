# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 17:14:37 2016

@author: Neha Pandey
"""
import matplotlib.pyplot as plt
import numpy as np
from skimage.feature import hog
from skimage import color
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import time
from prepare_data_XOR import prepare_data_XOR

def unit_step(x):
    if x<0:
        return 0
    else:
        return 1
        
def perceptron(data,weight,b):
    yPred=unit_step(np.dot(weight,data)+b)
    return yPred
    
def update_parameters(yPred,yTrue,weight_prev,data,b_prev):
    updated_weight=weight_prev+(yTrue-yPred)*data
    updated_b=b_prev+yTrue-yPred
    return updated_weight, updated_b
    
def neural_net(data_set,yTrue,weight_start,b_start):
    weight=weight_start
    b=b_start
    for i in range(0,len(data_set)):
        yPred=perceptron(data_set[i,:],weight,b)
        updated_weight,updated_b=update_parameters(yPred,yTrue[i],weight,data_set[i,:],b)
        weight=updated_weight
        b=updated_b
    return weight,b


def delta(data,weight,b):
    yPred=np.dot(weight,data)+b
    return yPred

def delta_of_error (data_set,weight,b,yTrue):
    error=[]
    for i in range(0,len(data_set)):
        yPred=delta(data_set[i,:],weight,b)
        error_i=yTrue[i]-yPred
        error.append(error_i)
    error=np.array(error)
    loss=(np.sum(error**2))/(2*len(data_set))
    gradient=(np.dot(np.transpose(error),data_set))/len(data_set)
    return loss,gradient
              
def update_parameters_delta(weight_prev,gradient,learning_rate):
    weight=weight_prev+learning_rate*gradient
    return weight
    
def gradient_neural_net(data_set,yTrue,epochs,learning_rate,weight_start,b):
    weight=weight_start
    loss_epoch=[]
    for i in range(0,epochs):
        loss,gradient=delta_of_error (data_set,weight,b,yTrue)
        weight=update_parameters_delta(weight,gradient,learning_rate)
        loss_epoch.append(loss)
    return weight,loss_epoch
    

    
def stochastic_gradient_neural_net(data_set,yTrue,epochs,learning_rate,weight_start,b,batch_size):
    batches_no=int(len(data_set)/batch_size)
    loss_epoch_avg=[]
    weight_batch_start=weight_start
    for i in range(0,batches_no):
        start=i*batch_size
        stop=(i+1)*batch_size
        weight_batch,loss_epoch_batch=gradient_neural_net(data_set[start:stop,:],yTrue[start:stop,:],epochs,learning_rate,weight_batch_start,b)
        loss_epoch_avg.append(loss_epoch_batch)
        weight_batch_start=weight_batch
    return weight_batch,[sum(elem)/len(elem) for elem in zip(*loss_epoch_avg)]
                         
def draw_decision_boundary(data_set,data1,data2,weight,b,fig_name):
    h = .02  # step size in the mesh
    # create a mesh to plot in
    x_min, x_max = data_set[:, 0].min() - 1, data_set[:, 0].max() + 1
    y_min, y_max = data_set[:, 1].min() - 1, data_set[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),np.arange(y_min, y_max, h))
    z = weight[0,0]*xx+weight[0,1]*yy+b
    plt.contour(xx,yy,z,levels=[0])
    plt.scatter(data1[:,0],data1[:,1], marker='+')
    plt.scatter(data2[:,0],data2[:,1], c= 'green', marker='o')
    plt.suptitle(fig_name)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.savefig(fig_name)
    plt.show()
    
def learning_curve(epochs,loss,alpha,flag,accuracy1,accuracy2,fig_title):
    plt.plot(np.arange(0,epochs), loss)
    plt.suptitle(fig_title)
    plt.xlabel("Epoch #")
    plt.ylabel("Loss")
    plt.annotate('learning rate={1}'.format(alpha,alpha), xy=(0.5, 0.85), xycoords='axes fraction')
    plt.annotate('training accuracy={1:0.2f}'.format(alpha,accuracy1), xy=(0.5, 0.80), xycoords='axes fraction')
    if flag==True:
        plt.annotate('test accuracy={1:0.2f}'.format(alpha,accuracy2), xy=(0.5, 0.75), xycoords='axes fraction')
    
    
    plt.savefig(fig_title)
    plt.show()    

"""
#Working on Gaussian data

#generating dataset for training
mean1 = [0,0]
cov1 = [[2, 0],[0, 1]]
data1 = np.random.multivariate_normal(mean1, cov1, 100)
yTrue1=np.full((len(data1),1),1)

mean2 = [20,10]
cov2 = [[1, 0],[0, 2]]
data2 = np.random.multivariate_normal(mean2, cov2, 100)
yTrue2=np.full((len(data2),1),-1)

data_set=list()
data_set.extend(data1)
data_set.extend(data2)
data_set=np.array(data_set)

yTrue=np.vstack((yTrue1,yTrue2))

#fitting in perceptron model
weight_start=np.random.randn(1,2)
b_start=np.random.randn()
weight_perceptron,b_perceptron=neural_net(data_set,yTrue,weight_start,b_start)
draw_decision_boundary(data_set,data1,data2,weight_perceptron,b_perceptron,'Decision boundary for Perceptron')

#fitting in gradient descent update model
epochs_gauss=100
learning_rate_gda_gauss=0.001
start_time=time.time()
weight_gda,loss_epoch_gda=gradient_neural_net(data_set,yTrue,epochs_gauss,learning_rate_gda_gauss,weight_start,b_start)
stop_time=time.time()
time_gda=stop_time-start_time
draw_decision_boundary(data_set,data1,data2,weight_gda,b_start,'Decision boundary for Gradient Descent Model')
learning_curve(epochs_gauss,loss_epoch_gda,learning_rate_gda_gauss,flag=False,accuracy=0,fig_title='Learning Curve for Gradient Descent Model')

#fitting in stochastic descent update model
epochs=100
learning_rate_sgda_gauss=0.001
batch_size_gauss=25
start_time=time.time()
weight_sgda,loss_epoch_sgda=stochastic_gradient_neural_net(data_set,yTrue,epochs,learning_rate_sgda_gauss,weight_start,b_start,batch_size_gauss)
stop_time=time.time()
time_sgda=stop_time-start_time
draw_decision_boundary(data_set,data1,data2,weight_sgda,b_start,'Decision boundary for Stochastic Gradient Descent Model')
learning_curve(epochs,loss_epoch_sgda,learning_rate_sgda_gauss,flag=False,accuracy=0,fig_title='Learning Curve for Stochastic Gradient Descent Model')       

#Working on CIFAR-10 Dataset 
     
def unpickle(file):
    import pickle
    fo = open(file, 'rb')
    dict = pickle.load(fo,encoding='latin1')
    fo.close()
    return dict

batch=unpickle('data_batch_1')
batch['data']=batch ['data'].reshape([-1 , 3 , 32 , 32])
batch ['data'] = batch ['data'].transpose ( ( 0 , 2 , 3 , 1 ) )

airplane_data=[]
dogs_data=[]

for i in range(0,10000):
    if(batch['labels'][i]==0):
        airplane_data.append(batch['data'][i])
    elif(batch['labels'][i]==5):
        dogs_data.append(batch['data'][i])
    else:
        pass


def extract_hog_features(image_set):
    hog_features=[]
    for i in range(0,len(image_set)):
        gray_image=color.rgb2gray(image_set[i])
        fd= hog(gray_image, orientations=8, pixels_per_cell=(4, 4),cells_per_block=(1, 1))
        
        hog_features.append(fd)
    return hog_features
    
hog_airplane_data=extract_hog_features(airplane_data)
hog_airplane_data=np.array(hog_airplane_data)
hog_dogs_data=extract_hog_features(dogs_data)
hog_dogs_data=np.array(hog_dogs_data)
label_airplane=np.full((1,len(hog_airplane_data)),-1)
label_dogs=np.full((1,len(hog_dogs_data)),1)

Airplane_train, Airplane_test, airplane_train, airplane_test = train_test_split(hog_airplane_data,np.transpose(label_airplane),test_size=0.2)
Dogs_train, Dogs_test, dogs_train, dogs_test = train_test_split(hog_dogs_data,np.transpose(label_dogs),test_size=0.2)

train=np.vstack((Airplane_train,Dogs_train))
test=np.vstack((Airplane_test,Dogs_test))
train_label=np.vstack((airplane_train,dogs_train))
test_label=np.vstack((airplane_test,dogs_test))


alpha_n=[0.1,0.01,0.001]
for alpha in alpha_n:
    epochs_CIFAR=1000
    weight_start=np.random.randn(1,512)
    b_CIFAR=np.random.rand()
    batch_size_CIFAR=100
    weight_sgda_CIFAR,epoch_loss_sgda_CIFAR=stochastic_gradient_neural_net(train,train_label,epochs_CIFAR,alpha,weight_start,b_CIFAR,batch_size_CIFAR)
    
    y_predicted_CIFAR=[]
    for i in range(0,len(test)):
        y=perceptron(test[i,:],weight_sgda_CIFAR,b_CIFAR)
        y_predicted_CIFAR.append(y)
        
    y_predicted_CIFAR=np.array(y_predicted_CIFAR)
    accuracy_CIFAR=accuracy_score(test_label,y_predicted_CIFAR)
    
    y_predicted_CIFAR_tr=[]
    for i in range(0,len(train)):
        y=perceptron(train[i,:],weight_sgda_CIFAR,b_CIFAR)
        y_predicted_CIFAR_tr.append(y)
        
    y_predicted_CIFAR_tr=np.array(y_predicted_CIFAR_tr)
    accuracy_CIFAR_tr=accuracy_score(train_label,y_predicted_CIFAR_tr)
    
    title2=('Learning curve for alpha={0}'.format(alpha*10)).replace('.',',')
    learning_curve(epochs_CIFAR,epoch_loss_sgda_CIFAR,alpha*10,flag=True,accuracy1=accuracy_CIFAR_tr,accuracy2=accuracy_CIFAR,fig_title=title2)

"""
weight_start=np.random.randn(1,2)
b=np.random.randn()
alpha_n=[0.1,0.01,0.001]
for alpha in alpha_n:
    #working with gaussian XOR data
    
    N_samples=50
    sigma_n=[1,3,5]
    for sigma in sigma_n:
        X,y=prepare_data_XOR(N_samples, sigma)
        
        weight,loss=stochastic_gradient_neural_net(X,y,1000,alpha,weight_start,b,50)
        
        y_predicted_tr=[]
        for i in range(0,len(X)):
            yr=perceptron(X[i,:],weight,b)
            y_predicted_tr.append(yr)
        
        y_predicted_tr=np.array(y_predicted_tr)
        accuracy=accuracy_score(y,y_predicted_tr)
        
        data1=X[np.any(y==0, axis=1),:]
        data2=X[np.any(y==1, axis=1),:]
        
        name1=('decision boundary for sigma={0}, alpha={1}'.format(sigma,alpha*10)).replace('.',',')
        name2='learning curve for sigma={0}, alpha={1}'.format(sigma,alpha*10).replace('.',',')
        draw_decision_boundary(X,data1,data2,weight,b,name1)
        learning_curve(epochs=1000,loss=loss,alpha=alpha*10,flag=False,accuracy1=accuracy,accuracy2=0,fig_title=name2)
        
    


        
        

    

    

    






        
    
        
    
    

    
        
        
        
        


