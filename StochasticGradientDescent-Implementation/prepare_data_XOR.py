import numpy as np

def prepare_data_XOR(N_samples, sigma):
    # CREATE DATA (XOR gate)
    means_gaussians = np.asarray([[3, 3], [-3, 3], [-3, -3], [3, -3]])
    labels_gaussians = [1, 0, 1, 0]
    var_gaussians = sigma * np.eye(2)

    # sample points per gaussian
    X = []
    labels = []
    for i in range(len(means_gaussians)):
        pts_tmp = np.random.multivariate_normal(means_gaussians[i], var_gaussians, N_samples)

        if i == 0:
            X = pts_tmp
            labels = labels_gaussians[i] * np.ones([N_samples, 1])
        else:
            X = np.vstack((X, pts_tmp))
            labels = np.vstack((labels, labels_gaussians[i] * np.ones([N_samples, 1])))

    return X, labels